//
//  SMyProfileViewController.swift
//  Splooze
//
//  Created by ETHANEMAC on 10/05/19.
//  Copyright © 2019 Deepak Kumar. All rights reserved.
//

import UIKit

class SMyProfileViewController: UIViewController {
    
    let Window_Width = UIScreen.main.bounds.size.width
    let Window_height = UIScreen.main.bounds.size.height
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var dobLabel: UILabel!
    
    
    //MARK:- UIView life cycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        navigationController?.isNavigationBarHidden = true
    }
    
    //MARK:- UIButton Action Methods
    
    @IBAction func likedButtonAction(_ sender: UIButton) {
        let controller = HomeStoryboard.instantiateViewController(withIdentifier: "SUsersListViewController") as! SUsersListViewController
        controller.userType = .likedList
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func myTicketsButtonAction(_ sender: UIButton) {
        let controller = HomeStoryboard.instantiateViewController(withIdentifier: "STicketListViewController") as! STicketListViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func myHangoutsButtonAction(_ sender: UIButton) {
        //move on calendar screen
        let controller = HomeStoryboard.instantiateViewController(withIdentifier: "SMyHangoutViewController") as! SMyHangoutViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func myPostButtonAction(_ sender: UIButton) {
        let controller = HomeStoryboard.instantiateViewController(withIdentifier: "SEventsListViewController") as! SEventsListViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func followingButtonAction(_ sender: UIButton) {
        let controller = HomeStoryboard.instantiateViewController(withIdentifier: "SUsersListViewController") as! SUsersListViewController
        controller.userType = .followingList
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func followersButtonAction(_ sender: UIButton) {
        let controller = HomeStoryboard.instantiateViewController(withIdentifier: "SUsersListViewController") as! SUsersListViewController
        controller.userType = .followerList
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func editProfileButtonAction(_ sender: UIButton) {
        let controller = RegistrationStoryboard.instantiateViewController(withIdentifier: "SEditProfileViewController") as! SEditProfileViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func logoutButtonAction(_ sender: Any) {
        _ = AlertController.alert("Alert!", message: "Are you sure you want to logout?", controller: self, buttons: ["No", "Yes"], tapBlock: { (action, index) in
            if index == 1 {
                kAppDelegate.gotoLogin()
            }
        })
    }
    
    //MARK:- Helper Methods
    
    func initialSetup() {
        // FOR tabBar Constraints
        
    }
    
}
