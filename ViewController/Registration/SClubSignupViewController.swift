//
//  SClubSignupViewController.swift
//  Splooze
//
//  Created by ETHANEMAC on 07/05/19.
//  Copyright © 2019 Deepak Kumar. All rights reserved.
//

import UIKit
import UnderLineTextField

class SClubSignupViewController: UIViewController {
    
    var userInfo = UserInfo()
    var errorArray = [false, false, false, false, false, true, true]
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var firstNameTextField: UnderLineTextField!
    @IBOutlet weak var lastNameTextField: UnderLineTextField!
    @IBOutlet weak var emailTextField: UnderLineTextField!
    @IBOutlet weak var contactTextField: UnderLineTextField!
    @IBOutlet weak var restro_clubNameTextField: UnderLineTextField!
    @IBOutlet weak var restro_clubAddressTextField: UnderLineTextField!
    @IBOutlet weak var additionalCommentTextField: UnderLineTextField!
    
    @IBOutlet weak var addressButton: UIButton!
    
    @IBOutlet weak var signupViewLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var signupViewRightConstraint: NSLayoutConstraint!
    
    //MARK:- UIView life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    //MARK:- Helper Methods
    func initialSetup() {
        
        signupViewLeftConstraint.constant = Window_Width == 320 ? 30 : Window_Width == 375 ? 40 : 50
        signupViewRightConstraint.constant = Window_Width == 320 ? 30 : Window_Width == 375 ? 40 : 50
        
    }
    
    //MARK:- UIButton Action Methods
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addressButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        //open google place picker
        
    }
    
    @IBAction func submitButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        let filtered = errorArray.filter { $0 == false }
        
        if filtered.count <= 0 {
            APIManager.apiForClubRegistration(userInfo: userInfo) { (status) in
                _ = AlertController.alert("Thanks!", message: "Your are successfully register with Splooze. Please wait for admin approval.", acceptMessage: "OK", acceptBlock: {
                    self.navigationController?.popToRootViewController(animated: true)
                })
            }
        } else if !errorArray[0] {
            do {
                try (self.view.viewWithTag(1000) as! UnderLineTextField).validate()
            } catch {
                // handle error
            }
        } else if !errorArray[1] {
            do {
                try (self.view.viewWithTag(1001) as! UnderLineTextField).validate()
            } catch {
                // handle error
            }
        } else if !errorArray[2] {
            do {
                try (self.view.viewWithTag(1002) as! UnderLineTextField).validate()
            } catch {
                // handle error
            }
        } else if !errorArray[3] {
            do {
                try (self.view.viewWithTag(1003) as! UnderLineTextField).validate()
            } catch {
                // handle error
            }
        } else if !errorArray[4] {
            do {
                try (self.view.viewWithTag(1004) as! UnderLineTextField).validate()
            } catch {
                // handle error
            }
        } else if !errorArray[5] {
            do {
                try (self.view.viewWithTag(1005) as! UnderLineTextField).validate()
            } catch {
                // handle error
            }
        }
    }
    
}

extension SClubSignupViewController: UnderLineTextFieldDelegate, UITextFieldDelegate {
    
    //==================================
    // MARK: - UnderLineTextField Delegate
    //==================================
    func textFieldValidate(underLineTextField: UnderLineTextField) throws {
        switch underLineTextField.tag {
        case 1000:
            let verifyObj = ValidationClass.verifyFirstname(text: underLineTextField.text!)
            if !verifyObj.1 {
                throw UnderLineTextFieldErrors
                    .error(message: verifyObj.0)
            } else {
                errorArray[0] = true
            }
        case 1001:
            let verifyObj = ValidationClass.verifyLastname(text: underLineTextField.text!)
            if !verifyObj.1 {
                throw UnderLineTextFieldErrors
                    .error(message: verifyObj.0)
            } else {
                errorArray[1] = true
            }
        case 1002:
            let verifyObj = ValidationClass.verifyEmail(text: underLineTextField.text!)
            if !verifyObj.1 {
                throw UnderLineTextFieldErrors
                    .error(message: verifyObj.0)
            } else {
                errorArray[2] = true
            }
        case 1003:
            let verifyObj = ValidationClass.verifyPhoneNumber(text: underLineTextField.text!)
            if !verifyObj.1 {
                throw UnderLineTextFieldErrors
                    .error(message: verifyObj.0)
            } else {
                errorArray[3] = true
            }
        case 1004:
            let verifyObj = ValidationClass.verifyRestro_ClubName(text: underLineTextField.text!)
            if !verifyObj.1 {
                throw UnderLineTextFieldErrors
                    .error(message: verifyObj.0)
            } else {
                errorArray[4] = true
            }
        case 1005:
            let verifyObj = ValidationClass.verifyRestro_ClubAddress(text: underLineTextField.text!)
            if !verifyObj.1 {
                throw UnderLineTextFieldErrors
                    .error(message: verifyObj.0)
            } else {
                errorArray[5] = true
            }
        default:
            break
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let nextTextField = view.viewWithTag(textField.tag + 1) else {
            view.endEditing(true)
            return false
        }
        _ = nextTextField.becomeFirstResponder()
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.textInputMode?.primaryLanguage == "emoji") || !((textField.textInputMode?.primaryLanguage) != nil) {
            return false
        }
        
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        switch textField.tag {
        case 1000:
            if str.length > 32 || string == " " {
                return false
            }
            break
        case 1001:
            if str.length > 32 || string == " " {
                return false
            }
            break
        case 1002:
            if str.length > 64 || string == " " {
                return false
            }
            break
        case 1003:
            if str.length > 15 || string == " " {
                return false
            }
            break
        case 1004:
            if str.length > 80  {
                return false
            }
            break
        case 1006:
            if str.length > 150  {
                return false
            }
            break
        default:
            break
        }
        return true
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 1000:
            userInfo.firstName = textField.text!.trimWhiteSpace
            break
        case 1001:
            userInfo.lastName = textField.text!.trimWhiteSpace
            break
        case 1002:
            userInfo.email = textField.text!.trimWhiteSpace
            break
        case 1003:
            userInfo.contact = textField.text!.trimWhiteSpace
            break
        case 1004:
            userInfo.restro_clubName = textField.text!.trimWhiteSpace
            break
        case 1005:
            userInfo.restro_clubAddress = textField.text!.trimWhiteSpace
            break
        case 1006:
            userInfo.additionalComment = textField.text!.trimWhiteSpace
            break
        default:
            break
        }
    }
    
    func textFieldTextChanged(underLineTextField: UnderLineTextField) {
        print("im empty \(underLineTextField)")
    }
    
}
