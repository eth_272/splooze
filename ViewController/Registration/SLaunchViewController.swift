//
//  SLaunchViewController.swift
//  Splooze
//
//  Created by ETHANEMAC on 26/03/19.
//  Copyright © 2019 Deepak Kumar. All rights reserved.
//

import UIKit

class SLaunchViewController: UIViewController {

    //MARK:- UIComponent Outlets
    
    @IBOutlet weak var endUserButton: UIButton!
    @IBOutlet weak var clubUserButton: UIButton!
    
    
    //MARK:- UIView Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.initialSetup()
    }
    
    //MARK:- Helper Methods
    func initialSetup() {
        self.clubUserButton.titleLabel?.numberOfLines = 0
        self.clubUserButton.titleLabel?.lineBreakMode = .byWordWrapping
        self.clubUserButton.titleLabel?.textAlignment = .center
        self.clubUserButton.setTitle("A Club, bar\nor\nevent representative".uppercased(), for: .normal)
        
    }
    
    //MARK:- UIButton Action Methods
 
    @IBAction func endUserButtonAction(_ sender: UIButton) {
        
        let controller = RegistrationStoryboard.instantiateViewController(withIdentifier: "SSignupOneViewController") as! SSignupOneViewController
        controller.userInfo.userType = "0"
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clubUserButtonAction(_ sender: UIButton) {
        let controller = RegistrationStoryboard.instantiateViewController(withIdentifier: "SClubSignupViewController") as! SClubSignupViewController
        controller.userInfo.userType = "1"
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}
