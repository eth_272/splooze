//
//  SForgetPassOTPVerificationViewController.swift
//  Splooze
//
//  Created by ETHANEMAC on 09/05/19.
//  Copyright © 2019 Deepak Kumar. All rights reserved.
//

import UIKit
import UnderLineTextField


class SForgetPassOTPVerificationViewController: UIViewController {
    
    var isOTPVerified = false
    var userInfo = UserInfo()
    
    //MARK:- IBOutlets
    @IBOutlet weak var otpTextField: UnderLineTextField!
    @IBOutlet weak var otpViewLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var otpViewRightConstraint: NSLayoutConstraint!
    
    //MARK:- UIView life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initialSetup()
    }
    
    //MARK:- Helper Methods
    func initialSetup() {
        
        otpViewLeftConstraint.constant = Window_Width == 320 ? 30 : Window_Width == 375 ? 40 : 50
        otpViewRightConstraint.constant = Window_Width == 320 ? 30 : Window_Width == 375 ? 40 : 50
        
    }
    
    //MARK:- UIButton Action Methods
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func resendButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        _ = AlertController.alert("", message: WIP)
    }
    
    @IBAction func nextButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if isOTPVerified {
            let controller = RegistrationStoryboard.instantiateViewController(withIdentifier: "SResetPasswordViewController") as! SResetPasswordViewController
            self.navigationController?.pushViewController(controller, animated: true)
        } else {
            do {
                try otpTextField.validate()
            } catch {
                // handle error
            }
        }
    }
    
}

extension SForgetPassOTPVerificationViewController: UnderLineTextFieldDelegate, UITextFieldDelegate {
    
    //==================================
    // MARK: - UnderLineTextField Delegate
    //==================================
    func textFieldValidate(underLineTextField: UnderLineTextField) throws {
        switch underLineTextField {
        case otpTextField:
            if underLineTextField.text!.length != 6 {
                throw UnderLineTextFieldErrors
                    .error(message: "Please enter 6 digit OTP.")
            } else {
                isOTPVerified = true
            }
        default:
            break
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let nextTextField = view.viewWithTag(textField.tag + 1) else {
            view.endEditing(true)
            return false
        }
        _ = nextTextField.becomeFirstResponder()
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.textInputMode?.primaryLanguage == "emoji") || !((textField.textInputMode?.primaryLanguage) != nil) {
            return false
        }
        
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        if str.length > 6 || string == " " {
            return false
        }
        return true
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        userInfo.OTP = textField.text!.trimWhiteSpace
    }
    
    func textFieldTextChanged(underLineTextField: UnderLineTextField) {
        print("im empty \(underLineTextField)")
    }
    
}
