//
//  SSignupTwoViewController.swift
//  Splooze
//
//  Created by ETHANEMAC on 10/04/19.
//  Copyright © 2019 Deepak Kumar. All rights reserved.
//

import UIKit
import UnderLineTextField

class SSignupTwoViewController: UIViewController {
    
    var isPasswordVerified = false
    var isConfirmPasswordVerified = false
    var userInfo = UserInfo()
    
    //MARK:- IBOutlets
    @IBOutlet weak var passwordTextField: UnderLineTextField! {
        didSet {
            passwordTextField.tag = 1000
        }
    }
    @IBOutlet weak var confirmPasswordTextField: UnderLineTextField! {
        didSet {
            confirmPasswordTextField.tag = 1001
        }
    }
    @IBOutlet weak var dobTextField: UnderLineTextField! {
        didSet {
            dobTextField.tag = 1002
        }
    }
    @IBOutlet weak var genderTextField: UnderLineTextField! {
        didSet {
            genderTextField.tag = 1003
        }
    }
    @IBOutlet weak var signupViewLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var signupViewRightConstraint: NSLayoutConstraint!
    
    //MARK:- UIView life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initialSetup()
    }
    
    //MARK:- Helper Methods
    func initialSetup() {
        
        signupViewLeftConstraint.constant = Window_Width == 320 ? 30 : Window_Width == 375 ? 40 : 50
        signupViewRightConstraint.constant = Window_Width == 320 ? 30 : Window_Width == 375 ? 40 : 50
        
    }
    
    //MARK:- UIButton Action Methods
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func dobButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        RPicker.selectDate(datePickerMode: .date, selectedDate: Date(), minDate: nil, maxDate: Date()) { (selectedDate) in
            self.userInfo.DOB = convertToDate(date: selectedDate, format: "dd/MM/yyyy")
            self.dobTextField.text = self.userInfo.DOB
        }
    }
    
    @IBAction func genderButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        RPicker.sharedInstance.selectOption(dataArray: ["Male","Female","Other"], selectedIndex: 0) { (selectedValue, selectedIndex) in
            self.userInfo.gender = selectedValue
            self.genderTextField.text = self.userInfo.gender
        }
    }
    
    @IBAction func nextButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if isPasswordVerified && isConfirmPasswordVerified && userInfo.DOB.length > 0 && userInfo.gender.length > 0 {
            let controller = RegistrationStoryboard.instantiateViewController(withIdentifier: "SSignupThreeViewController") as! SSignupThreeViewController
            controller.userInfo = self.userInfo
            self.navigationController?.pushViewController(controller, animated: true)
        } else if !isPasswordVerified {
            do {
                try passwordTextField.validate()
            } catch {
                // handle error
            }
        } else if !isConfirmPasswordVerified {
            do {
                try confirmPasswordTextField.validate()
            } catch {
                // handle error
            }
        } else if userInfo.DOB.length == 0 {
            _ = AlertController.alert("", message: "Please choose your date of birth.")
        } else if userInfo.gender.length == 0 {
            _ = AlertController.alert("", message: "Please select gender.")
        }
    }
    
}

extension SSignupTwoViewController: UnderLineTextFieldDelegate, UITextFieldDelegate {
    
    //==================================
    // MARK: - UnderLineTextField Delegate
    //==================================
    func textFieldValidate(underLineTextField: UnderLineTextField) throws {
        switch underLineTextField {
        case passwordTextField:
            let verifyObj = ValidationClass.verifyPassword(password: underLineTextField.text!)
            if !verifyObj.1 {
                throw UnderLineTextFieldErrors
                    .error(message: verifyObj.0)
            } else {
                isPasswordVerified = true
            }
        case confirmPasswordTextField:
            let verifyObj = ValidationClass.verifyPasswordAndConfirmPassword(password: userInfo.password, confirmPassword: underLineTextField.text!)
            if !verifyObj.1 {
                throw UnderLineTextFieldErrors
                    .error(message: verifyObj.0)
            } else {
                isConfirmPasswordVerified = true
            }
        default:
            break
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let nextTextField = view.viewWithTag(textField.tag + 1) else {
            view.endEditing(true)
            return false
        }
        _ = nextTextField.becomeFirstResponder()
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.textInputMode?.primaryLanguage == "emoji") || !((textField.textInputMode?.primaryLanguage) != nil) {
            return false
        }
        
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        switch textField.tag {
        case 1000:
            if str.length > 16 || string == " " {
                return false
            }
            break
        case 1001:
            if str.length > 16 || string == " " {
                return false
            }
            break
        default:
            break
        }
        return true
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 1000:
            userInfo.password = textField.text!.trimWhiteSpace
            break
        case 1001:
            userInfo.confirmPassword = textField.text!.trimWhiteSpace
            break
        default:
            break
        }
    }
    
    func textFieldTextChanged(underLineTextField: UnderLineTextField) {
        print("im empty \(underLineTextField)")
    }
    
}



