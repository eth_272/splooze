//
//  SResetPasswordViewController.swift
//  Splooze
//
//  Created by ETHANEMAC on 09/05/19.
//  Copyright © 2019 Deepak Kumar. All rights reserved.
//

import UIKit
import UnderLineTextField

class SResetPasswordViewController: UIViewController {
    
    var isNewPasswordVerified = false
    var isConfirmedNewPasswordVerified = false
    
    //MARK:- IBOutlets
    @IBOutlet weak var loginViewLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var loginViewRightConstraint: NSLayoutConstraint!
    @IBOutlet weak var newPassword: UnderLineTextField!{
        didSet {
            newPassword.tag = 1000
        }
    }
    
    @IBOutlet weak var confirmNewPassword: UnderLineTextField!{
        didSet {
            confirmNewPassword.tag = 1001
        }
    }
    
    //MARK:- UIView life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isNewPasswordVerified = false
        isConfirmedNewPasswordVerified = false
    }
    
    //MARK:- Helper Methods
    func initialSetup() {
        loginViewLeftConstraint.constant = Window_Width == 320 ? 30 : Window_Width == 375 ? 40 : 50
        loginViewRightConstraint.constant = Window_Width == 320 ? 30 : Window_Width == 375 ? 40 : 50
    }
    
    //MARK:- UIButton Action Methods
    @IBAction func submitButton(_ sender: UIButton) {
        self.view.endEditing(true)
        if isNewPasswordVerified && isConfirmedNewPasswordVerified {
            _ = AlertController.alert("Thanks", message: "Your password reset successfully.", acceptMessage: "OK", acceptBlock: {
                self.navigationController?.popToRootViewController(animated: true)
            })
        }else if !isNewPasswordVerified {
            do {
                try newPassword.validate()
            } catch {
                // handle error
            }
        } else if !isConfirmedNewPasswordVerified {
            do {
                try confirmNewPassword.validate()
            } catch {
                // handle error
            }
        }
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
}

extension SResetPasswordViewController: UnderLineTextFieldDelegate, UITextFieldDelegate {
    
    //==================================
    // MARK: - UnderLineTextField Delegate
    //==================================
    func textFieldValidate(underLineTextField: UnderLineTextField) throws {
        switch underLineTextField {
        case newPassword:
            let verifyObj = ValidationClass.verifyNewPassword(password: underLineTextField.text!)
            if !verifyObj.1 {
                throw UnderLineTextFieldErrors
                    .error(message: verifyObj.0)
            } else {
                isNewPasswordVerified = true
            }
        case confirmNewPassword:
            let verifyObj = ValidationClass.verifyNewPasswordAndConfirmNewPassword(password: newPassword.text!, confirmPassword: underLineTextField.text!)
            if !verifyObj.1 {
                throw UnderLineTextFieldErrors
                    .error(message: verifyObj.0)
            } else {
                isConfirmedNewPasswordVerified = true
            }
        default:
            break
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let nextTextField = view.viewWithTag(textField.tag + 1) else {
            view.endEditing(true)
            return false
        }
        _ = nextTextField.becomeFirstResponder()
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.textInputMode?.primaryLanguage == "emoji") || !((textField.textInputMode?.primaryLanguage) != nil) {
            return false
        }
        
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        if str.length > 16 && string == " "{
            return false
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 1000:
            newPassword.text = textField.text!.trimWhiteSpace
            break
        case 1001:
            confirmNewPassword.text = textField.text!.trimWhiteSpace
        default:
            break
        }
    }
    
    func textFieldTextChanged(underLineTextField: UnderLineTextField) {
        print("im empty \(underLineTextField)")
    }
}
