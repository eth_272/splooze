//
//  SSignupThreeViewController.swift
//  Splooze
//
//  Created by ETHANEMAC on 10/04/19.
//  Copyright © 2019 Deepak Kumar. All rights reserved.
//

import UIKit
import UnderLineTextField

class SSignupThreeViewController: UIViewController {
    
    var picker = UIImagePickerController()
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var cameraImage: UIImageView!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var t_cBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var signupViewLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var signupViewRightConstraint: NSLayoutConstraint!
    var userInfo = UserInfo()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initialSetup()
    }
    
    //MARK:- Helper Methods
    func initialSetup() {
        signupViewLeftConstraint.constant = Window_Width == 320 ? 30 : Window_Width == 375 ? 40 : 50
        signupViewRightConstraint.constant = Window_Width == 320 ? 30 : Window_Width == 375 ? 40 : 50
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
        userImage.addGestureRecognizer(tap)
        cameraImage.addGestureRecognizer(tap1)
    }
    
    //MARK:- Gesture recognizer method action
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        // handling code
        self.optionForImageUpload()
    }
    
    //MARK:- UI Button Action
    
    
    @IBAction func commonBtnAction(_ sender: UIButton) {
        
        switch sender.tag{
        //Back
        case 100:
            self.navigationController?.popViewController(animated: true)
            break
            
        //Terms check
        case 101:
            sender.isSelected = !sender.isSelected
            break
            
        //Allow google
        case 102:
            sender.isSelected = !sender.isSelected
            break
            
        //Submit Btn
        case 103:
            if !t_cBtn.isSelected {
                _ = AlertController.alert("Please accept terms and condition.")
            } else {
                APIManager.apiForRegistration(userInfo: userInfo) { (status) in
                    if status {
                        let controller = RegistrationStoryboard.instantiateViewController(withIdentifier: "SSignupFourViewController") as! SSignupFourViewController
                        controller.userInfo = UserInfo.sharedInstance
                        self.navigationController?.pushViewController(controller, animated: true)
                    }
                }
            }
            break
            
        case 104:
            let controller = RegistrationStoryboard.instantiateViewController(withIdentifier: "SStaticDataViewController") as! SStaticDataViewController
            self.navigationController?.pushViewController(controller, animated: true)
            break
        default:
            break
        }
    }
    
}

// MARK: Image Picker Handler
extension SSignupThreeViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func optionForImageUpload() {
        self.view.endEditing(true)
        
        _ = AlertController.actionSheet("Choose image", message: "", sourceView: self.view, buttons: ["Camera","Gallery","Cancel"], tapBlock: { (action, index) in
            if index == 0 {
                self.openCamera()
            } else if index == 1 {
                self.openGallery()
            }
        })
    }
    
    func openCamera() {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {
            picker.delegate = self
            picker.isEditing = true
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.navigationBar.tintColor = UIColor.black
            self .present(picker, animated: true, completion: nil)
        } else {
            openGallery()
        }
    }
    
    func openGallery() {
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        picker.delegate = self
        if UIDevice.current.userInterfaceIdiom == .phone {
            self.present(picker, animated: true, completion: nil)
            picker.navigationBar.tintColor = UIColor.black
        } else {
            
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        picker .dismiss(animated: true, completion: nil)
        //        var imageData = Data()
        //        imageData = ((info[UIImagePickerController.InfoKey.originalImage] as? UIImage)?.jpegData(compressionQuality: 0.2))!
        //        self.userInfo.imageData = imageData
        
        if let url =  info[.mediaURL] as? URL {
            Debug.log("\(url)")
        }
        
        if let images = info[.originalImage] as? UIImage {
            self.userImage.image = images
            self.userInfo.userImageIcon = images
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker .dismiss(animated: true, completion: nil)
    }
    
}
