//
//  SChangePasswordViewController.swift
//  Splooze
//
//  Created by ETHANEMAC on 09/05/19.
//  Copyright © 2019 Deepak Kumar. All rights reserved.
//

import UIKit
import UnderLineTextField

class SChangePasswordViewController: UIViewController {
    
    var isOldPasswordVerified = false
    var isNewPasswordVerified = false
    var isConfirmedNewPasswordVerified = false
    var userInfo = UserInfo()
    
    
    //MARK:- IBOutlets
    @IBOutlet weak var loginViewLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var loginViewRightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var oldPassword: UnderLineTextField!{
        didSet {
            oldPassword.tag = 1000
        }
    }
    
    @IBOutlet weak var newPassword: UnderLineTextField!{
        didSet {
            newPassword.tag = 1001
        }
    }
    
    
    @IBOutlet weak var confirmNewPassword: UnderLineTextField!{
        didSet {
            confirmNewPassword.tag = 1002
        }
    }
    
    
    //MARK:- UIView life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
    }
    
    
    //MARK:- Helper Methods
    func initialSetup() {
        
        loginViewLeftConstraint.constant = Window_Width == 320 ? 30 : Window_Width == 375 ? 40 : 50
        loginViewRightConstraint.constant = Window_Width == 320 ? 30 : Window_Width == 375 ? 40 : 50
    }
    
    //MARK:- UIButton Action Methods
    @IBAction func backButton(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func submitButton(_ sender: UIButton) {
        self.view.endEditing(true)
        if isOldPasswordVerified && isNewPasswordVerified && isConfirmedNewPasswordVerified {
            let controller = RegistrationStoryboard.instantiateViewController(withIdentifier: "SSignupFourViewController") as! SSignupFourViewController
            self.navigationController?.pushViewController(controller, animated: true)
        } else if !isOldPasswordVerified {
            do {
                try oldPassword.validate()
            } catch {
                // handle error
            }
        } else if !isNewPasswordVerified {
            do {
                try newPassword.validate()
            } catch {
                // handle error
            }
        } else if !isConfirmedNewPasswordVerified {
            do {
                try confirmNewPassword.validate()
            } catch {
                // handle error
            }
        }
        
        print("hellllllllllo")
    }
    
}
extension SChangePasswordViewController: UnderLineTextFieldDelegate, UITextFieldDelegate {
    
    //==================================
    // MARK: - UnderLineTextField Delegate
    //==================================
    func textFieldValidate(underLineTextField: UnderLineTextField) throws {
        switch underLineTextField {
        case oldPassword:
            let verifyObj = ValidationClass.verifyPasswordOld(password:
                underLineTextField.text!)
            if !verifyObj.1 {
                throw UnderLineTextFieldErrors
                    .error(message: verifyObj.0)
            } else {
                isOldPasswordVerified = true
            }
        case newPassword:
            let verifyObj = ValidationClass.verifyNewPassword(password: underLineTextField.text!)
            if !verifyObj.1 {
                throw UnderLineTextFieldErrors
                    .error(message: verifyObj.0)
            } else {
                isNewPasswordVerified = true
            }
        case confirmNewPassword:
            let verifyObj = ValidationClass.verifyNewPasswordAndConfirmNewPassword(password: newPassword.text!, confirmPassword: underLineTextField.text!)
            if !verifyObj.1 {
                throw UnderLineTextFieldErrors
                    .error(message: verifyObj.0)
            } else {
                isConfirmedNewPasswordVerified = true
            }
        default:
            break
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let nextTextField = view.viewWithTag(textField.tag + 1) else {
            view.endEditing(true)
            return false
        }
        _ = nextTextField.becomeFirstResponder()
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.textInputMode?.primaryLanguage == "emoji") || !((textField.textInputMode?.primaryLanguage) != nil) {
            return false
        }
        
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        if str.length > 16 || string == " " {
            return false
        }
        return true
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 1000:
            oldPassword.text = textField.text!.trimWhiteSpace
            break
        case 1001:
            newPassword.text = textField.text!.trimWhiteSpace
            break
        case 1002:
            confirmNewPassword.text = textField.text!.trimWhiteSpace
        default:
            break
        }
    }
    
    func textFieldTextChanged(underLineTextField: UnderLineTextField) {
        print("im empty \(underLineTextField)")
    }
    
}


