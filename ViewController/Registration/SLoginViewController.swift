//
//  SLoginViewController.swift
//  Splooze
//
//  Created by ETHANEMAC on 05/04/19.
//  Copyright © 2019 Deepak Kumar. All rights reserved.
//

import UIKit
import UnderLineTextField

class SLoginViewController: UIViewController {

    var isEmailVerified = false
    var isPasswordVerified = false
    
    //MARK:- IBOutlets
    @IBOutlet weak var emailTextField: UnderLineTextField! {
        didSet {
            emailTextField.validationType = .afterEdit
            emailTextField.tag = 1000
        }
    }
    @IBOutlet weak var passwordTextField: UnderLineTextField! {
        didSet {
            passwordTextField.validationType = .afterEdit
            passwordTextField.tag = 1001
        }
    }
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var rememberMeButton: UIButton!
    @IBOutlet weak var loginViewLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var loginViewRightConstraint: NSLayoutConstraint!
    
    //MARK:- UIView life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initialSetup()
    }
    
    //MARK:- Helper Methods
    func initialSetup() {
        
        loginViewLeftConstraint.constant = Window_Width == 320 ? 30 : Window_Width == 375 ? 40 : 50
        loginViewRightConstraint.constant = Window_Width == 320 ? 30 : Window_Width == 375 ? 40 : 50
        showRememberMe()
        
        //start for testing purpose
        emailTextField.text = "deepak131@mailinator.com"
        passwordTextField.text = "123456789"
        isEmailVerified = true
        isPasswordVerified = true
        self.rememberMeButton.isSelected = true
        //end for testing purpose
    }
    
    /// Remember me feature implementation
    func  doRememberMe() {
        DispatchQueue.main.async(execute: {
            if self.rememberMeButton.isSelected {
                UserDefaults.standard.set(self.emailTextField.text?.trimWhiteSpace, forKey: "email")
                UserDefaults.standard.set(self.passwordTextField.text?.trimWhiteSpace, forKey: "password")
            } else {
                UserDefaults.standard.removeObject(forKey: "email")
                UserDefaults.standard.removeObject(forKey: "password")
            }
        })
    }
    
    func showRememberMe() {
        
        if  (((UserDefaults.standard.value(forKey: "email") as! String?) != nil) && ((UserDefaults.standard.value(forKey: "password") as! String?) != nil)) {
            self.emailTextField.text = (UserDefaults.standard.value(forKey: "email") as! String?)!
            self.passwordTextField.text = (UserDefaults.standard.value(forKey: "password") as! String?)!
            self.rememberMeButton.isSelected = true
        } else {
            self.emailTextField.text = ""
            self.passwordTextField.text = ""
            self.rememberMeButton.isSelected = false
        }
    }
    
    //MARK:- UIButton Action Methods

    @IBAction func rememberMeButtonAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    @IBAction func forgotPasswordButtonAction(_ sender: UIButton) {
        _ = AlertController.alert("", message: WIP)
    }
    @IBAction func loginButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if isEmailVerified && isPasswordVerified {
            self.loginButton.isUserInteractionEnabled = false
            self.doRememberMe()
            
            APIManager.apiForLogin(email: self.emailTextField.text!.trimWhiteSpace, password: self.passwordTextField.text!.trimWhiteSpace) { (status) in
                if status {
                    if !UserInfo.sharedInstance.isOTPVerified && UserInfo.sharedInstance.userType == "UR" {
                        let controller = RegistrationStoryboard.instantiateViewController(withIdentifier: "SSignupFourViewController") as! SSignupFourViewController
                        controller.userInfo = UserInfo.sharedInstance
                        self.navigationController?.pushViewController(controller, animated: true)
                    } else {
                        DispatchQueue.main.async {
                            let controller = HomeStoryboard.instantiateViewController(withIdentifier: "STabBarBaseViewController") as! STabBarBaseViewController
                            self.navigationController?.pushViewController(controller, animated: true)
                        }
                    }
                }
            }
        } else if !isEmailVerified {
            do {
                try emailTextField.validate()
            } catch {
                // handle error
            }
        } else if !isPasswordVerified {
            do {
                try passwordTextField.validate()
            } catch {
                // handle error
            }
        }
        
    }
    
    @IBAction func facebookButtonAction(_ sender: UIButton) {
        _ = AlertController.alert("", message: WIP)
    }
    @IBAction func twitterButtonAction(_ sender: UIButton) {
        _ = AlertController.alert("", message: WIP)
    }
    @IBAction func instagramButtonAction(_ sender: UIButton) {
        _ = AlertController.alert("", message: WIP)
    }
    @IBAction func signupButtonAction(_ sender: UIButton) {
        let controller = RegistrationStoryboard.instantiateViewController(withIdentifier: "SLaunchViewController") as! SLaunchViewController
        self.navigationController?.pushViewController(controller, animated: true)

//        let controller = HomeStoryboard.instantiateViewController(withIdentifier: "SEventsListViewController") as! SEventsListViewController
//        self.navigationController?.pushViewController(controller, animated: true)
    }
        
}

extension SLoginViewController: UnderLineTextFieldDelegate, UITextFieldDelegate {
    
    //==================================
    // MARK: - UnderLineTextField Delegate
    //==================================
    func textFieldValidate(underLineTextField: UnderLineTextField) throws {
        switch underLineTextField {
        case emailTextField:
            if underLineTextField.text!.isEmpty {
                throw UnderLineTextFieldErrors
                    .error(message: "Please enter email or contact number.")
            } else if !underLineTextField.text!.contains("@") && underLineTextField.text!.containsNumberOnly()  {
                let verifyObj = ValidationClass.verifyPhoneNumber(text: underLineTextField.text!)
                
                if !verifyObj.1 {
                    throw UnderLineTextFieldErrors
                        .error(message: verifyObj.0)
                } else {
                    isEmailVerified = true
                }
            } else if !underLineTextField.text!.isEmail {
                isEmailVerified = false
                throw UnderLineTextFieldErrors
                    .error(message: "Please enter valid email.")
            } else {
                isEmailVerified = true
            }
        case passwordTextField:
            if underLineTextField.text!.isEmpty {
                throw UnderLineTextFieldErrors
                    .error(message: "Please enter password.")
            } else if underLineTextField.text!.length < 6 {
                throw UnderLineTextFieldErrors
                    .error(message: "Please enter at least 6 characters.")
            } else {
                isPasswordVerified = true
            }
        default:
            break
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let nextTextField = view.viewWithTag(textField.tag + 1) else {
            view.endEditing(true)
            return false
        }
        _ = nextTextField.becomeFirstResponder()
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.textInputMode?.primaryLanguage == "emoji") || !((textField.textInputMode?.primaryLanguage) != nil) {
            return false
        }
        
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        switch textField.tag {
        case 1000: // email
            if str.length > 64 || string == " " {
                return false
            }
            break
        case 1001: // password
            if str.length > 16 || string == " " {
                return false
            }
            break
            
        default:
            break
        }
        return true
        
    }
    
    func textFieldTextChanged(underLineTextField: UnderLineTextField) {
        print("im empty \(underLineTextField)")
    }
    
}
