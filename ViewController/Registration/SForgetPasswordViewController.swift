//
//  SForgetPasswordViewController.swift
//  Splooze
//
//  Created by ETHANEMAC on 18/04/19.
//  Copyright © 2019 Deepak Kumar. All rights reserved.
//

import UIKit
import UnderLineTextField

class SForgetPasswordViewController: UIViewController {
    
    var isEmailVerified = false
    
    //MARK:- IBOutlets
    @IBOutlet weak var emailTextField: UnderLineTextField! {
        didSet {
            emailTextField.tag = 1000
        }
    }
    @IBOutlet weak var forgetPassViewLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var forgetPassViewRightConstraint: NSLayoutConstraint!
    
    //MARK:- UIView life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initialSetup()
    }
    
    //MARK:- Helper Methods
    func initialSetup() {
        
        forgetPassViewLeftConstraint.constant = Window_Width == 320 ? 30 : Window_Width == 375 ? 40 : 50
        forgetPassViewRightConstraint.constant = Window_Width == 320 ? 30 : Window_Width == 375 ? 40 : 50
        
    }
    
    //MARK:- UIButton Action Methods
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func sendOTPButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if isEmailVerified{
            let controller = RegistrationStoryboard.instantiateViewController(withIdentifier: "SForgetPassOTPVerificationViewController") as! SForgetPassOTPVerificationViewController
            self.navigationController?.pushViewController(controller, animated: true)
        } else if !isEmailVerified {
            do {
                try emailTextField.validate()
            } catch {
                // handle error
            }
        }
        
    }
}

extension SForgetPasswordViewController: UnderLineTextFieldDelegate, UITextFieldDelegate {
    
    //==================================
    // MARK: - UnderLineTextField Delegate
    //==================================
    func textFieldValidate(underLineTextField: UnderLineTextField) throws {
        switch underLineTextField {
        case emailTextField:
            if underLineTextField.text!.isEmpty {
                throw UnderLineTextFieldErrors
                    .error(message: "Please enter email or contact number.")
            } else if !underLineTextField.text!.contains("@") && underLineTextField.text!.containsNumberOnly()  {
                let verifyObj = ValidationClass.verifyPhoneNumber(text: underLineTextField.text!)
                
                if !verifyObj.1 {
                    throw UnderLineTextFieldErrors
                        .error(message: verifyObj.0)
                } else {
                    isEmailVerified = true
                }
            } else if !underLineTextField.text!.isEmail {
                isEmailVerified = false
                throw UnderLineTextFieldErrors
                    .error(message: "Please enter valid email.")
            } else {
                isEmailVerified = true
            }
        default:
            break
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let nextTextField = view.viewWithTag(textField.tag + 1) else {
            view.endEditing(true)
            return false
        }
        _ = nextTextField.becomeFirstResponder()
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.textInputMode?.primaryLanguage == "emoji") || !((textField.textInputMode?.primaryLanguage) != nil) {
            return false
        }
        
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        switch textField.tag {
        case 1000: // email
            if str.length > 64 || string == " " {
                return false
            }
            break
        default:
            break
        }
        return true
        
    }
    
    func textFieldTextChanged(underLineTextField: UnderLineTextField) {
    }
    
}
