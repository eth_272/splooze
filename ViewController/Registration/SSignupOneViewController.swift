//
//  SSignupOneViewController.swift
//  Splooze
//
//  Created by ETHANEMAC on 09/04/19.
//  Copyright © 2019 Deepak Kumar. All rights reserved.
//

import UIKit
import UnderLineTextField

class SSignupOneViewController: UIViewController {
    
    var isFirstNameVerified = false
    var isLastNameVerified = false
    var isEmailVerified = false
    var isContactVerified = false
    var userInfo = UserInfo()
    
    //MARK:- IBOutlets
    @IBOutlet weak var firstNameTextField: UnderLineTextField! {
        didSet {
            firstNameTextField.tag = 1000
        }
    }
    @IBOutlet weak var lastNameTextField: UnderLineTextField! {
        didSet {
            lastNameTextField.tag = 1001
        }
    }
    @IBOutlet weak var emailTextField: UnderLineTextField! {
        didSet {
            emailTextField.tag = 1002
        }
    }
    @IBOutlet weak var contactTextField: UnderLineTextField! {
        didSet {
            contactTextField.tag = 1003
        }
    }
    @IBOutlet weak var signupViewLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var signupViewRightConstraint: NSLayoutConstraint!
    
    //MARK:- UIView life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initialSetup()
    }
    
    //MARK:- Helper Methods
    func initialSetup() {
        
        signupViewLeftConstraint.constant = Window_Width == 320 ? 30 : Window_Width == 375 ? 40 : 50
        signupViewRightConstraint.constant = Window_Width == 320 ? 30 : Window_Width == 375 ? 40 : 50
        
    }
    
    //MARK:- UIButton Action Methods
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if isEmailVerified && isFirstNameVerified && isLastNameVerified && isContactVerified {
            let controller = RegistrationStoryboard.instantiateViewController(withIdentifier: "SSignupTwoViewController") as! SSignupTwoViewController
            controller.userInfo = userInfo
            self.navigationController?.pushViewController(controller, animated: true)
        } else if !isFirstNameVerified {
            do {
                try firstNameTextField.validate()
            } catch {
                // handle error
            }
        }else if !isLastNameVerified {
            do {
                try lastNameTextField.validate()
            } catch {
                // handle error
            }
        }else if !isEmailVerified {
            do {
                try emailTextField.validate()
            } catch {
                // handle error
            }
        } else if !isContactVerified {
            do {
                try contactTextField.validate()
            } catch {
                // handle error
            }
        }
    }

}

extension SSignupOneViewController: UnderLineTextFieldDelegate, UITextFieldDelegate {
    
    //==================================
    // MARK: - UnderLineTextField Delegate
    //==================================
    func textFieldValidate(underLineTextField: UnderLineTextField) throws {
        switch underLineTextField {
        case firstNameTextField:
            let verifyObj = ValidationClass.verifyFirstname(text: underLineTextField.text!)
            if !verifyObj.1 {
                throw UnderLineTextFieldErrors
                    .error(message: verifyObj.0)
            } else {
                isFirstNameVerified = true
            }
        case lastNameTextField:
            let verifyObj = ValidationClass.verifyLastname(text: underLineTextField.text!)
            if !verifyObj.1 {
                throw UnderLineTextFieldErrors
                    .error(message: verifyObj.0)
            } else {
                isLastNameVerified = true
            }
        case emailTextField:
            let verifyObj = ValidationClass.verifyEmail(text: underLineTextField.text!)
            if !verifyObj.1 {
                throw UnderLineTextFieldErrors
                    .error(message: verifyObj.0)
            } else {
                isEmailVerified = true
            }
        case contactTextField:
            let verifyObj = ValidationClass.verifyPhoneNumber(text: underLineTextField.text!)
            if !verifyObj.1 {
                throw UnderLineTextFieldErrors
                    .error(message: verifyObj.0)
            } else {
                isContactVerified = true
            }
        default:
            break
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let nextTextField = view.viewWithTag(textField.tag + 1) else {
            view.endEditing(true)
            return false
        }
        _ = nextTextField.becomeFirstResponder()
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.textInputMode?.primaryLanguage == "emoji") || !((textField.textInputMode?.primaryLanguage) != nil) {
            return false
        }
        
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        switch textField.tag {
        case 1000:
            if str.length > 32 || string == " " {
                return false
            }
            break
        case 1001:
            if str.length > 32 || string == " " {
                return false
            }
            break
        case 1002:
            if str.length > 64 || string == " " {
                return false
            }
            break
        case 1003:
            if str.length > 15 || string == " " {
                return false
            }
            break
        default:
            break
        }
        return true
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 1000:
            userInfo.firstName = textField.text!.trimWhiteSpace
            break
        case 1001:
            userInfo.lastName = textField.text!.trimWhiteSpace
            break
        case 1002:
            userInfo.email = textField.text!.trimWhiteSpace
            break
        case 1003:
            userInfo.contact = textField.text!.trimWhiteSpace
            break
        default:
            break
        }
    }
    
    func textFieldTextChanged(underLineTextField: UnderLineTextField) {
        print("im empty \(underLineTextField)")
    }
    
}
