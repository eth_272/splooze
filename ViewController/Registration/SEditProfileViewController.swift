//
//  SEditProfileViewController.swift
//  Splooze
//
//  Created by ETHANEMAC on 16/05/19.
//  Copyright © 2019 Deepak Kumar. All rights reserved.
//

import UIKit
import UnderLineTextField

class SEditProfileViewController: UIViewController {
    
    var userInfo = UserInfo()
    
    var isFirstNameVerified = false
    var isLastNameVerified = false
    var isContactVerified = false
    
    
    @IBOutlet weak var dobButton: UIButton!
    @IBOutlet weak var genderButton: UIButton!
    
    var picker = UIImagePickerController()
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var cameraImage: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var firstNameTextField: UnderLineTextField!
    @IBOutlet weak var lastNameTextField: UnderLineTextField!
    @IBOutlet weak var contactTextField: UnderLineTextField!
    @IBOutlet weak var dobTextField: UnderLineTextField!
    @IBOutlet weak var genderTextField: UnderLineTextField!
    
    @IBOutlet weak var signupViewLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var signupViewRightConstraint: NSLayoutConstraint!
    
    //MARK:- UIView life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //MARK:- Helper Methods
    func initialSetup() {
//        dobTextField.bringSubviewToFront(self.dobButton)
//        genderTextField.bringSubviewToFront(self.genderButton)
        signupViewLeftConstraint.constant = Window_Width == 320 ? 30 : Window_Width == 375 ? 40 : 50
        signupViewRightConstraint.constant = Window_Width == 320 ? 30 : Window_Width == 375 ? 40 : 50
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
        userImage.addGestureRecognizer(tap)
        cameraImage.addGestureRecognizer(tap1)
    }
    
    //MARK:- Gesture recognizer method action
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        // handling code
        self.optionForImageUpload()
    }
    
    //MARK:- UIButton Action Methods
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func changePasswordButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        let controller = RegistrationStoryboard.instantiateViewController(withIdentifier: "SChangePasswordViewController") as! SChangePasswordViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func dobButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        RPicker.selectDate(datePickerMode: .date, selectedDate: Date(), minDate: nil, maxDate: Date()) { (selectedDate) in
            self.userInfo.DOB = convertToDate(date: selectedDate, format: "dd-MM-yyyy")
            self.dobTextField.text = self.userInfo.DOB
        }
    }
    
    @IBAction func genderButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        RPicker.sharedInstance.selectOption(dataArray: ["Male","Female","Other"], selectedIndex: 0) { (selectedValue, selectedIndex) in
            self.userInfo.gender = selectedValue
            self.genderTextField.text = self.userInfo.gender
        }
    }
    @IBAction func editBeverageButtonAction(_ sender: UIButton) {
        let controller = HomeStoryboard.instantiateViewController(withIdentifier: "SBeveragesViewController") as! SBeveragesViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func submitButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if isFirstNameVerified && isLastNameVerified && isContactVerified && userInfo.DOB.length != 0 && userInfo.gender.length != 0 {
            _ = AlertController.alert("Thanks!", message: "Your profile updated successfully.", acceptMessage: "OK", acceptBlock: {
                self.navigationController?.popViewController(animated: true)
            })
            
        } else if !isFirstNameVerified {
            do {
                try (self.view.viewWithTag(1000) as! UnderLineTextField).validate()
            } catch {
                // handle error
            }
        } else if !isLastNameVerified {
            do {
                try (self.view.viewWithTag(1001) as! UnderLineTextField).validate()
            } catch {
                // handle error
            }
        } else if !isContactVerified {
            do {
                try (self.view.viewWithTag(1002) as! UnderLineTextField).validate()
            } catch {
                // handle error
            }
        } else if userInfo.DOB.length == 0 {
            _ = AlertController.alert("", message: "Please choose your date of birth.")
        } else if userInfo.gender.length == 0 {
            _ = AlertController.alert("", message: "Please select gender.")
        }
    }
    
}

extension SEditProfileViewController: UnderLineTextFieldDelegate, UITextFieldDelegate {
    
    //==================================
    // MARK: - UnderLineTextField Delegate
    //==================================
    func textFieldValidate(underLineTextField: UnderLineTextField) throws {
        switch underLineTextField.tag {
        case 1000:
            let verifyObj = ValidationClass.verifyFirstname(text: underLineTextField.text!)
            if !verifyObj.1 {
                throw UnderLineTextFieldErrors
                    .error(message: verifyObj.0)
            } else {
                isFirstNameVerified = true
            }
        case 1001:
            let verifyObj = ValidationClass.verifyLastname(text: underLineTextField.text!)
            if !verifyObj.1 {
                throw UnderLineTextFieldErrors
                    .error(message: verifyObj.0)
            } else {
                isLastNameVerified = true
            }
        case 1002:
            let verifyObj = ValidationClass.verifyPhoneNumber(text: underLineTextField.text!)
            if !verifyObj.1 {
                throw UnderLineTextFieldErrors
                    .error(message: verifyObj.0)
            } else {
                isContactVerified = true
            }
        default:
            break
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let nextTextField = view.viewWithTag(textField.tag + 1) else {
            view.endEditing(true)
            return false
        }
        _ = nextTextField.becomeFirstResponder()
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.textInputMode?.primaryLanguage == "emoji") || !((textField.textInputMode?.primaryLanguage) != nil) {
            return false
        }
        
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        switch textField.tag {
        case 1000:
            if str.length > 32 || string == " " {
                return false
            }
            break
        case 1001:
            if str.length > 32 || string == " " {
                return false
            }
            break
        case 1002:
            if str.length > 15 || string == " " {
                return false
            }
            break
        default:
            break
        }
        return true
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 1000:
            userInfo.firstName = textField.text!.trimWhiteSpace
            break
        case 1001:
            userInfo.lastName = textField.text!.trimWhiteSpace
            break
        case 1002:
            userInfo.contact = textField.text!.trimWhiteSpace
            break
        default:
            break
        }
    }
    
    func textFieldTextChanged(underLineTextField: UnderLineTextField) {
        print("im empty \(underLineTextField)")
    }
    
}

// MARK: Image Picker Handler
extension SEditProfileViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func optionForImageUpload() {
        self.view.endEditing(true)
        
        _ = AlertController.actionSheet("Choose image", message: "", sourceView: self.view, buttons: ["Camera","Gallery","Cancel"], tapBlock: { (action, index) in
            if index == 0 {
                self.openCamera()
            } else if index == 1 {
                self.openGallery()
            }
        })
    }
    
    func openCamera() {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {
            picker.delegate = self
            picker.isEditing = true
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.navigationBar.tintColor = UIColor.black
            self .present(picker, animated: true, completion: nil)
        } else {
            openGallery()
        }
    }
    
    func openGallery() {
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        picker.delegate = self
        if UIDevice.current.userInterfaceIdiom == .phone {
            self.present(picker, animated: true, completion: nil)
            picker.navigationBar.tintColor = UIColor.black
        } else {
            
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        picker .dismiss(animated: true, completion: nil)
        //        var imageData = Data()
        //        imageData = ((info[UIImagePickerController.InfoKey.originalImage] as? UIImage)?.jpegData(compressionQuality: 0.2))!
        //        self.userInfo.imageData = imageData
        
        if let url =  info[.mediaURL] as? URL {
            Debug.log("\(url)")
        }
        
        if let images = info[.originalImage] as? UIImage {
            self.userImage.image = images
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker .dismiss(animated: true, completion: nil)
    }
    
}
