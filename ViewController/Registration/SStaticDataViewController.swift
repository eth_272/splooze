//
//  SStaticDataViewController.swift
//  Splooze
//
//  Created by ETHANEMAC on 27/05/19.
//  Copyright © 2019 Deepak Kumar. All rights reserved.
//

import UIKit

enum StaticControllerType {
    case p_p, t_c, a_u
}

class SStaticDataViewController: UIViewController {
    var staticControllerType : StaticControllerType = .p_p
    
    //MARK:- IBOutlets
    @IBOutlet weak var navigationLabel: UILabel!
    @IBOutlet weak var dataTextView: UITextView!
    
    //MARK:- UIView life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationLabel.text = staticControllerType == .p_p ? "PRIVACY POLICY" : staticControllerType == .t_c ? "TERMS & CONDITIONS" : "ABOUT US"
        self.dataTextView.text = staticControllerType == .p_p ? "PRIVACY POLICY" : staticControllerType == .t_c ? "TERMS & CONDITIONS" : "ABOUT US"
    }
    
    //Mark:- IBAction buttons
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
