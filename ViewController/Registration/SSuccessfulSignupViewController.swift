//
//  SSuccessfulSignupViewController.swift
//  Splooze
//
//  Created by DK Varshney on 13/04/19.
//  Copyright © 2019 Deepak Kumar. All rights reserved.
//

import UIKit

class SSuccessfulSignupViewController: UIViewController {

    @IBOutlet weak var xplooreNowBtn: UIButton!
    @IBOutlet weak var viewMyProfileBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var signupViewLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var signupViewRightConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initialSetup()
    }
    
    //MARK:- Helper Methods
    func initialSetup() {
        
        signupViewLeftConstraint.constant = Window_Width == 320 ? 30 : Window_Width == 375 ? 40 : 50
        signupViewRightConstraint.constant = Window_Width == 320 ? 30 : Window_Width == 375 ? 40 : 50
        
    }

    @IBAction func commonBtnAction(_ sender: UIButton) {
        
        switch sender.tag{
            
        //Back
        case 100:
        self.navigationController?.popViewController(animated: true)
            break
        case 101:_ = AlertController.alert(WIP)
            break
        case 102:
            let controller = HomeStoryboard.instantiateViewController(withIdentifier: "STabBarBaseViewController") as! STabBarBaseViewController
            self.navigationController?.pushViewController(controller, animated: true)
            break
        default:
            break
        }
    }
}
