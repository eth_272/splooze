//
//  STicketListViewController.swift
//  Splooze
//
//  Created by ETHANEMAC on 17/05/19.
//  Copyright © 2019 Deepak Kumar. All rights reserved.
//

import UIKit

class STicketListViewController: UIViewController {
    
    var dataArray = [EventDetail]()
    
    //MARK:- UIView life cycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataArray = EventDetailInfo().detailArray
    }
    
    //MARK:- UIButton Action Methods
    
    @IBAction func backButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension STicketListViewController : UITableViewDataSource , UITableViewDelegate {
    //MARK:- tableView delegate and datasource Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SUserTableViewCell", for: indexPath) as! SUserTableViewCell
        let object = dataArray[indexPath.row]
        cell.backGroundImage?.image = UIImage(named:object.bgImage)
        cell.ticketEventName.text = (object.ticketEventNameString)
        cell.Address.text = (object.address)
        cell.ticketImage?.image = UIImage(named:object.image)
        cell.ticketPriceLabel.text = (object.pricelabel)
        cell.ticketCostLabel.text = (object.Costlabel)
        cell.timeLabel.text = "\(object.dayString) \(object.dateString), \(object.timeString)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 115
    }
    
}
