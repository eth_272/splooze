//
//  SBeveragesListViewController.swift
//  Splooze
//
//  Created by ETHANEMAC on 28/05/19.
//  Copyright © 2019 Deepak Kumar. All rights reserved.
//

import UIKit

class SBeveragesListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var dataArray = [BeverageDetail]()
    
    //MARK:- UIView life cycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        self.dataArray = BeverageList().beveragesArray
        
    }
    
    //Mark:- IBAction buttons
    @IBAction func doneButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func checkBoxButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        var object = dataArray[sender.tag - 30000000]
        object.b_edit_status = !object.b_edit_status
        dataArray[sender.tag - 30000000] = object
        self.tableView.reloadData()
    }
    
    
}

extension SBeveragesListViewController : UITableViewDelegate, UITableViewDataSource {
    //MARK:- TableView Delegate and datasource Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SBeverageTableViewCell", for: indexPath) as! SBeverageTableViewCell
        
        let object = dataArray[indexPath.row]
        cell.checkBoxButton.tag = indexPath.row + 30000000
        cell.beverageNameTextField.text = object.b_name
        cell.beveragePriceTextField.text = object.b_price
        cell.beverageImageView.image = object.b_image
        cell.checkBoxButton.isSelected = object.b_edit_status
        cell.checkBoxButton.addTarget(self, action: #selector(checkBoxButtonAction(_:)), for: .touchUpInside)
        
        return cell
    }
    
    //MARK:- TableView Method For Managing Height Of Row
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
}

