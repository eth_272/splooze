//
//  SOtherUserProfileViewController.swift
//  Splooze
//
//  Created by ETHANEMAC on 20/05/19.
//  Copyright © 2019 Deepak Kumar. All rights reserved.
//

import UIKit

class SOtherUserProfileViewController: UIViewController {
    
    let Window_Width = UIScreen.main.bounds.size.width
    let Window_height = UIScreen.main.bounds.size.height
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var dobLabel: UILabel!
    
    //MARK:- UIView life cycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        navigationController?.isNavigationBarHidden = true
    }
    
    //MARK:- UIButton Action Methods
    
    @IBAction func likedButtonAction(_ sender: UIButton) {
        let controller = HomeStoryboard.instantiateViewController(withIdentifier: "SUsersListViewController") as! SUsersListViewController
        controller.userType = .likedList
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func followingButtonAction(_ sender: UIButton) {
        let controller = HomeStoryboard.instantiateViewController(withIdentifier: "SUsersListViewController") as! SUsersListViewController
        controller.userType = .followingList
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func followersButtonAction(_ sender: UIButton) {
        let controller = HomeStoryboard.instantiateViewController(withIdentifier: "SUsersListViewController") as! SUsersListViewController
        controller.userType = .followerList
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func likeProfileButtonAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func followButtonAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Helper Methods
    
    func initialSetup() {
        
    }
    
}
