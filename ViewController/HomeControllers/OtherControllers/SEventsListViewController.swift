//
//  SEventsListViewController.swift
//  Splooze
//
//  Created by ETHANEMAC on 21/05/19.
//  Copyright © 2019 Deepak Kumar. All rights reserved.
//

import UIKit
//import CVCalendar

class SEventsListViewController: UIViewController {
    
    // MARK: - Properties
    @IBOutlet weak var calendarView: CVCalendarView!
    @IBOutlet weak var menuView: CVCalendarMenuView!
    @IBOutlet weak var monthLabel: UILabel!
    
    private var animationFinished = true
    private var selectedDay: DayView!
    private var currentCalendar: Calendar?
    var dataArray = [EventDetail]()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var yourEventButton: UIButton!
    @IBOutlet weak var allEventButton: UIButton!
    @IBOutlet weak var yourEventArrowImage: UIImageView!
    @IBOutlet weak var allEventArrowImage: UIImageView!
    
    override func awakeFromNib() {
    }
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        calendarView.commitCalendarViewUpdate()
        menuView.commitMenuViewUpdate()
    }
    
    //MARK:- Helper method
    func initialSetup() {
        self.dataArray = EventDetailInfo().detailArray
        calendarView.changeDaysOutShowingState(shouldShow: true)
        monthLabel.text = CVDate(date: Date(), calendar: Calendar.current).globalDescription
        self.eventButtonSetting(isAllEvent: false)
    }
    
    func eventButtonSetting(isAllEvent : Bool) {
        yourEventArrowImage.isHidden = isAllEvent
        allEventArrowImage.isHidden = !isAllEvent
        yourEventButton.isSelected = !isAllEvent
        allEventButton.isSelected = isAllEvent
    }
    
}

// MARK: - CVCalendarViewDelegate & CVCalendarMenuViewDelegate

extension SEventsListViewController: CVCalendarViewDelegate, CVCalendarMenuViewDelegate {
    
    // MARK: Required methods
    
    func presentationMode() -> CalendarMode { return .weekView }
    
    func firstWeekday() -> Weekday { return .monday }
    
    // Defaults to true
    
    func shouldAutoSelectDayOnMonthChange() -> Bool { return false }
    
    func didSelectDayView(_ dayView: CVCalendarDayView, animationDidFinish: Bool) {
        selectedDay = dayView
        print("Selected Date \(selectedDay.date.day)")
    }
    
    func shouldSelectRange() -> Bool { return true }
    
    func didSelectRange(from startDayView: DayView, to endDayView: DayView) {
        print("RANGE SELECTED: \(startDayView.date.commonDescription) to \(endDayView.date.commonDescription)")
    }
    
    func presentedDateUpdated(_ date: CVDate) {
        if monthLabel.text != date.globalDescription && self.animationFinished {
            let updatedMonthLabel = UILabel()
            updatedMonthLabel.textColor = monthLabel.textColor
            updatedMonthLabel.font = monthLabel.font
            updatedMonthLabel.textAlignment = .center
            updatedMonthLabel.text = date.globalDescription
            updatedMonthLabel.sizeToFit()
            updatedMonthLabel.alpha = 0
            updatedMonthLabel.center = self.monthLabel.center
            
            let offset = CGFloat(48)
            updatedMonthLabel.transform = CGAffineTransform(translationX: 0, y: offset)
            updatedMonthLabel.transform = CGAffineTransform(scaleX: 1, y: 0.1)
            
            UIView.animate(withDuration: 0.35, delay: 0, options: UIView.AnimationOptions.curveEaseIn, animations: {
                self.animationFinished = false
                self.monthLabel.transform = CGAffineTransform(translationX: 0, y: -offset)
                self.monthLabel.transform = CGAffineTransform(scaleX: 1, y: 0.1)
                self.monthLabel.alpha = 0
                
                updatedMonthLabel.alpha = 1
                updatedMonthLabel.transform = CGAffineTransform.identity
                
            }) { _ in
                
                self.animationFinished = true
                self.monthLabel.frame = updatedMonthLabel.frame
                self.monthLabel.text = updatedMonthLabel.text
                self.monthLabel.transform = CGAffineTransform.identity
                self.monthLabel.alpha = 1
                updatedMonthLabel.removeFromSuperview()
            }
            
            self.view.insertSubview(updatedMonthLabel, aboveSubview: self.monthLabel)
        }
    }
    
    func topMarker(shouldDisplayOnDayView dayView: CVCalendarDayView) -> Bool { return true }
    
    func selectionViewPath() -> ((CGRect) -> (UIBezierPath)) {
        return { UIBezierPath(rect: CGRect(x: 0, y: 0, width: $0.width, height: $0.height)) }
    }
    
    func shouldShowCustomSingleSelection() -> Bool { return false }
    
    func preliminaryView(viewOnDayView dayView: DayView) -> UIView {
        let circleView = CVAuxiliaryView(dayView: dayView, rect: dayView.frame, shape: CVShape.circle)
        circleView.fillColor = .colorFromCode(0xCCCCCC)
        return circleView
    }
    
    func preliminaryView(shouldDisplayOnDayView dayView: DayView) -> Bool {
        if (dayView.isCurrentDay) {
            return true
        }
        return false
    }
    
    func supplementaryView(viewOnDayView dayView: DayView) -> UIView {
        
        dayView.setNeedsLayout()
        dayView.layoutIfNeeded()
        
        let π = Double.pi
        let ringLayer = CAShapeLayer()
        let ringLineWidth: CGFloat = 4.0
        let ringLineColour = UIColor.blue
        let newView = UIView(frame: dayView.frame)
        let diameter = (min(newView.bounds.width, newView.bounds.height))
        let radius = diameter / 2.0 - ringLineWidth
        
        newView.layer.addSublayer(ringLayer)
        
        ringLayer.fillColor = nil
        ringLayer.lineWidth = ringLineWidth
        ringLayer.strokeColor = ringLineColour.cgColor
        
        let centrePoint = CGPoint(x: newView.bounds.width/2.0, y: newView.bounds.height/2.0)
        let startAngle = CGFloat(-π/2.0)
        let endAngle = CGFloat(π * 2.0) + startAngle
        let ringPath = UIBezierPath(arcCenter: centrePoint,
                                    radius: radius,
                                    startAngle: startAngle,
                                    endAngle: endAngle,
                                    clockwise: true)
        
        ringLayer.path = ringPath.cgPath
        ringLayer.frame = newView.layer.bounds
        
        return newView
    }
    
    func dayOfWeekTextColor() -> UIColor { return .black }
    func dayOfWeekBackGroundColor() -> UIColor { return .white }
    func disableScrollingBeforeDate() -> Date { return Date() }
    func maxSelectableRange() -> Int { return 1 }
    func earliestSelectableDate() -> Date { return Date() }
    
}

// MARK: - CVCalendarViewAppearanceDelegate

extension SEventsListViewController: CVCalendarViewAppearanceDelegate {
    
    func dayLabelWeekdayDisabledColor() -> UIColor { return .lightGray }
    
    func dayLabelPresentWeekdayInitallyBold() -> Bool { return false }
    
    func spaceBetweenDayViews() -> CGFloat { return 0 }
    
    func dayLabelFont(by weekDay: Weekday, status: CVStatus, present: CVPresent) -> UIFont { return UIFont.systemFont(ofSize: 14) }
    
    func dayLabelColor(by weekDay: Weekday, status: CVStatus, present: CVPresent) -> UIColor? {
        switch (weekDay, status, present) {
        case (_, .selected, _), (_, .highlighted, _): return ColorsConfig.selectedText
        case (.sunday, .in, _): return ColorsConfig.sundayText
        case (.sunday, _, _): return ColorsConfig.sundayTextDisabled
        case (_, .in, _): return ColorsConfig.text
        default: return ColorsConfig.textDisabled
        }
    }
    
    func dayLabelBackgroundColor(by weekDay: Weekday, status: CVStatus, present: CVPresent) -> UIColor? {
        switch (weekDay, status, present) {
        case (.sunday, .selected, _), (.sunday, .highlighted, _): return ColorsConfig.sundaySelectionBackground
        case (_, .selected, _), (_, .highlighted, _): return ColorsConfig.selectionBackground
        default: return nil
        }
    }
}

// MARK: - IB Actions

extension SEventsListViewController {
    
    /// Switch to WeekView mode.
    @IBAction func toWeekView(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            calendarView.changeMode(.monthView)
        } else {
            calendarView.changeMode(.weekView)
        }
    }
    
    @IBAction func backDaysButtonAction(_ sender: AnyObject) {
        calendarView.loadPreviousView()
    }
    
    @IBAction func nextDaysButtonAction(_ sender: AnyObject) {
        calendarView.loadNextView()
    }
    
    @IBAction func backButtonAction(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func allEventButtonAction(_ sender: UIButton) {
        self.eventButtonSetting(isAllEvent: true)
    }
    @IBAction func yourEventButtonAction(_ sender: UIButton) {
        self.eventButtonSetting(isAllEvent: false)
    }
    
    @objc func editEventButtonAction(_ sender: UIButton) {
        _ = AlertController.alert("", message: WIP)
    }
    @objc func deleteEventButtonAction(_ sender: UIButton) {
        _ = AlertController.alert("", message: WIP)
    }
    @objc func shareEventButtonAction(_ sender: UIButton) {
        _ = AlertController.alert("", message: WIP)
    }
    
}

// MARK: - Convenience API Demo

extension SEventsListViewController {
    func toggleMonthViewWithMonthOffset(offset: Int) {
        guard let currentCalendar = currentCalendar else { return }
        var components = Manager.componentsForDate(Date(), calendar: currentCalendar) // from today
        components.month! += offset
        let resultDate = currentCalendar.date(from: components)!
        self.calendarView.toggleViewWithDate(resultDate)
    }
    
    func didShowNextMonthView(_ date: Date) {
        guard let currentCalendar = currentCalendar else { return }
        let components = Manager.componentsForDate(date, calendar: currentCalendar) // from today
        Debug.log("Showing Month: \(components.month!)")
    }
    
    func didShowPreviousMonthView(_ date: Date) {
        guard let currentCalendar = currentCalendar else { return }
        let components = Manager.componentsForDate(date, calendar: currentCalendar) // from today
        Debug.log("Showing Month: \(components.month!)")
    }
    
    func didShowNextWeekView(from startDayView: DayView, to endDayView: DayView) {
        Debug.log("Showing Week: from \(startDayView.date.day) to \(endDayView.date.day)")
    }
    
    func didShowPreviousWeekView(from startDayView: DayView, to endDayView: DayView) {
        Debug.log("Showing Week: from \(startDayView.date.day) to \(endDayView.date.day)")
    }
    
}

extension SEventsListViewController : UITableViewDelegate, UITableViewDataSource {
    //MARK:- TableView Delegate and datasource Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SMyHangoutsTableViewCell", for: indexPath) as! SMyHangoutsTableViewCell
        let object = dataArray[indexPath.row]
        cell.EventNameLabel.text = (object.ticketEventNameString)
        cell.addressLabel.text = (object.address)
        cell.eventImage?.image = UIImage(named:object.image)
        cell.dateAndTimeLabel.text = "\(object.dayString) \(object.dateString), \(object.timeString)"
        cell.deleteButton.tag = indexPath.row + 10000000
        cell.editButton.tag = indexPath.row + 20000000
        cell.shareButton.tag = indexPath.row + 30000000
        cell.deleteButton.addTarget(self, action: #selector(deleteEventButtonAction(_:)), for: .touchUpInside)
        cell.editButton.addTarget(self, action: #selector(editEventButtonAction(_:)), for: .touchUpInside)
        cell.shareButton.addTarget(self, action: #selector(shareEventButtonAction(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 240
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = HomeStoryboard.instantiateViewController(withIdentifier: "SEventDetailViewController") as! SEventDetailViewController
        //        controller.modalTransitionStyle = .flipHorizontal
        controller.modalTransitionStyle = .coverVertical
        let navNew = UINavigationController.init(rootViewController: controller)
        self.present(navNew, animated: true, completion: nil)
    }
}
