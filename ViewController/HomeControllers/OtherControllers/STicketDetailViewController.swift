//
//  STicketDetailViewController.swift
//  Splooze
//
//  Created by ETHANEMAC on 17/05/19.
//  Copyright © 2019 Deepak Kumar. All rights reserved.
//

import UIKit

class STicketDetailViewController: UIViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var eventImageView: UIImageView!
    @IBOutlet weak var eventNameLabel: UILabel!
    @IBOutlet weak var eventAddressLabel: UILabel!
    @IBOutlet weak var eventDateAndTimeLabel: UILabel!
    @IBOutlet weak var eventTransactionIdLabel: UILabel!
    @IBOutlet weak var qrCodeImageView: UIImageView!
    @IBOutlet weak var eventQrCodeIdLabel: UILabel!
    
    //MARK:- UIView Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    //MARK:- UIButton Action
    @IBAction func closeButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
