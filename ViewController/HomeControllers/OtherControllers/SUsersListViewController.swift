//
//  SUsersListViewController.swift
//  Splooze
//
//  Created by ETHANEMAC on 10/05/19.
//  Copyright © 2019 Deepak Kumar. All rights reserved.
//

import UIKit

enum UsersType {
    case likedList, followerList, followingList, participantList
}

class SUsersListViewController: UIViewController {
    
    var userType : UsersType = .participantList
    
    //MARK:- IBOutlets
    @IBOutlet weak var navigationLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK:- UIView life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationLabel.text = userType == .likedList ? "LIKED" : userType == .followingList ? "FOLLOWING" : userType == .followerList ? "FOLLOWER" : "PARTICIPANTS"
    }
    
    //Mark:- IBAction buttons
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension SUsersListViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    //MARK:- Collection View Methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SUsersCollectionViewCell", for: indexPath) as! SUsersCollectionViewCell
        
        cell.userName?.text = indexPath.row % 2 == 0 ? "Naddia Osminy" : "Naddia Osminy"
        cell.userImageView?.image = UIImage(named: indexPath.row % 2 == 0 ? "img_1" : "img_2")
        
        return cell
    }
    
    //MARK:- Collection View Methods For Managing FlowLayOut
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = Window_Width == 320 ? collectionView.frame.size.width / 3 - 9 : Window_Width == 375 ? collectionView.frame.size.width / 3 - 12 : collectionView.frame.size.width / 3 - 13
        return CGSize(width:width ,height: (collectionView.frame.size.width / 3 ) + 40 )
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        _ = AlertController.alert("", message: WIP)
        let controller = HomeStoryboard.instantiateViewController(withIdentifier: "SOtherUserProfileViewController") as! SOtherUserProfileViewController
        self.navigationController?.pushViewController(controller, animated: true)
        /*switch userType {
        case .followerList://move on follower list
            break
        case .likedList://move on liked list
            break
        case .followingList://move on following list
            break
        default://move on participant list
            break
        }*/
    }
}
