//
//  SBeveragesViewController.swift
//  Splooze
//
//  Created by ETHANEMAC on 16/05/19.
//  Copyright © 2019 Deepak Kumar. All rights reserved.
//

import UIKit

class SBeveragesViewController: UIViewController {
    
    @IBOutlet weak var beverageImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var beverageNameTextField: UITextField!
    @IBOutlet weak var beveragePriceTextField: UITextField!
    var picker = UIImagePickerController()
    var dataArray = [BeverageDetail]()
    var editableIndex = -1
    
    //MARK:- UIView life cycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
        beverageImageView.addGestureRecognizer(tap)
        beverageImageView?.isUserInteractionEnabled = true
        self.dataArray = BeverageList().beverageArray
        
    }
    
    //Mark:- IBAction buttons
    @IBAction func doneButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addMoreButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if beverageImageView != nil && beverageNameTextField.text?.count != 0 && beveragePriceTextField.text?.count != 0 {
            let object = BeverageDetail.init(b_id: "\(self.dataArray.count + 1)", b_image: beverageImageView.image!, b_name: beverageNameTextField.text!, b_price: beveragePriceTextField.text!, b_edit_status: false)
            
            APIManager.apiForAddBeverage(beverageInfo: <#T##BeverageDetail#>, clubId: <#T##String#>, completion: <#T##((Bool) -> Void)##((Bool) -> Void)##(Bool) -> Void#>)
            
            
            self.dataArray.append(object)
            beverageNameTextField.text = ""
            beveragePriceTextField.text = ""
            beverageImageView.image = nil
            self.tableView.reloadData()
        } else if beverageNameTextField.text?.count == 0 {
            _ = AlertController.alert("", message: "Please enter name.")
        } else if beveragePriceTextField.text?.count == 0 {
            _ = AlertController.alert("", message: "Please enter price.")
        } else {
            _ = AlertController.alert("", message: "Please select beverage image.")
        }
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func editButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        for index in 0..<self.dataArray.count {
            var object = self.dataArray[index]
            object.b_edit_status = false
            self.dataArray[index] = object
        }
        
        var object = self.dataArray[sender.tag - 20000000]
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            object.b_edit_status = true
            editableIndex = sender.tag - 20000000
        } else {
            editableIndex = -1
            object.b_edit_status = false
        }
        self.dataArray[sender.tag - 20000000] = object
        self.tableView.reloadData()
    }
    
    @objc func deleteButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.dataArray.remove(at: sender.tag - 30000000)
        self.tableView.reloadData()
    }
    
    //MARK:- Gesture recognizer method action
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        // handling code
        editableIndex = -1
        self.optionForImageUpload()
    }
    
    @objc func handleTapOnCell(sender: UITapGestureRecognizer? = nil) {
        let imageView = sender?.view as! UIImageView
        editableIndex = imageView.tag - 10000000
        // handling code
        self.optionForImageUpload()
    }
    
}

extension SBeveragesViewController : UITableViewDelegate, UITableViewDataSource {
    //MARK:- TableView Delegate and datasource Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SBeverageTableViewCell", for: indexPath) as! SBeverageTableViewCell
        
        let object = dataArray[indexPath.row]
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTapOnCell(sender:)))
        cell.beverageImageView.tag = indexPath.row + 10000000
        cell.editButton.tag = indexPath.row + 20000000
        cell.deleteButton.tag = indexPath.row + 30000000
        cell.beverageNameTextField.tag = indexPath.row + 40000000
        cell.beveragePriceTextField.tag = indexPath.row + 50000000
        cell.beverageNameTextField.delegate = self
        cell.beveragePriceTextField.delegate = self
        cell.beverageImageView?.addGestureRecognizer(tap)
        cell.beverageNameTextField.text = object.b_name
        cell.beveragePriceTextField.text = object.b_price
        cell.beverageImageView.image = object.b_image
        cell.editButton.isSelected = object.b_edit_status
        cell.beverageImageView?.isUserInteractionEnabled = object.b_edit_status
        cell.beverageNameTextField?.isUserInteractionEnabled = object.b_edit_status
        cell.beveragePriceTextField?.isUserInteractionEnabled = object.b_edit_status
        cell.editButton.addTarget(self, action: #selector(editButtonAction(_:)), for: .touchUpInside)
        cell.deleteButton.addTarget(self, action: #selector(deleteButtonAction(_:)), for: .touchUpInside)
        
        return cell
    }
    
    //MARK:- TableView Method For Managing Height Of Row
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
}

    // MARK: Image Picker Handler
extension SBeveragesViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
        
        func optionForImageUpload() {
            self.view.endEditing(true)
            
            _ = AlertController.actionSheet("Choose image", message: "", sourceView: self.view, buttons: ["Camera","Gallery","Cancel"], tapBlock: { (action, index) in
                if index == 0 {
                    self.openCamera()
                } else if index == 1 {
                    self.openGallery()
                }
            })
        }
        
        func openCamera() {
            if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {
                picker.delegate = self
                picker.isEditing = true
                picker.sourceType = UIImagePickerController.SourceType.camera
                picker.navigationBar.tintColor = UIColor.black
                self .present(picker, animated: true, completion: nil)
            } else {
                openGallery()
            }
        }
        
        func openGallery() {
            picker.sourceType = UIImagePickerController.SourceType.photoLibrary
            picker.delegate = self
            if UIDevice.current.userInterfaceIdiom == .phone {
                self.present(picker, animated: true, completion: nil)
                picker.navigationBar.tintColor = UIColor.black
            } else {
                
            }
        }
        
        func imagePickerController(_ picker: UIImagePickerController,
                                   didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            
            picker .dismiss(animated: true, completion: nil)
            //        var imageData = Data()
            //        imageData = ((info[UIImagePickerController.InfoKey.originalImage] as? UIImage)?.jpegData(compressionQuality: 0.2))!
            //        self.userInfo.imageData = imageData
            
//            if let url =  info[.mediaURL] as? URL {
//                Debug.log("\(url)")
//            }
            
            if let images = info[.originalImage] as? UIImage {
                if editableIndex == -1 {
                    beverageImageView.image = images
                } else {
                    var object = self.dataArray[editableIndex]
                    object.b_image = images
                    self.dataArray[editableIndex] = object
                }
                self.tableView.reloadData()
            }
        }
        
        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            picker .dismiss(animated: true, completion: nil)
        }
    
}

extension SBeveragesViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.textInputMode?.primaryLanguage == "emoji") || !((textField.textInputMode?.primaryLanguage) != nil) {
            return false
        }
        
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        
        if textField == beverageNameTextField {
            if str.length > 32 {
                return false
            }
        } else if textField == beveragePriceTextField {
            if str.length > 6 || string == " " {
                return false
            }
        } else {
            if textField.tag >= 40000000 && textField.tag >= 50000000 {
                if str.length > 32 {
                    return false
                }
            } else {
                if str.length > 6 || string == " " {
                    return false
                }
            }
        }
        
        return true
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField != beverageNameTextField && textField != beveragePriceTextField {
            if textField.tag >= 40000000 && textField.tag >= 50000000 {
                var object = self.dataArray[textField.tag - 40000000]
                object.b_name = textField.text!.trimWhiteSpace
                self.dataArray[textField.tag - 40000000] = object
            } else {
                var object = self.dataArray[textField.tag - 50000000]
                object.b_price = textField.text!.trimWhiteSpace
                self.dataArray[textField.tag - 50000000] = object
            }
            self.tableView.reloadData()
        }
    }
    
}

