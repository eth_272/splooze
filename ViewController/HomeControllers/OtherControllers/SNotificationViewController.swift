//
//  SNotificationViewController.swift
//  Splooze
//
//  Created by ETHANEMAC on 22/05/19.
//  Copyright © 2019 Deepak Kumar. All rights reserved.
//

import UIKit

class SNotificationViewController: UIViewController {
    
    var dataArray = [NotificationInfo]()
    
    //MARK:- UIView life cycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        self.dataArray = NotificationListInfo().NotificationArray
    }
    
    //Mark:- IBAction buttons
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func followButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension SNotificationViewController : UITableViewDelegate, UITableViewDataSource {
    //MARK:- TableView Delegate and datasource Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SUserTableViewCell", for: indexPath) as! SUserTableViewCell
        
        let object = dataArray[indexPath.row]
        cell.notificationButton.isHidden = object.n_type.lowercased() != "follow" && object.n_type.lowercased() != "follower"
        cell.notificationButton.addTarget(self, action: #selector(followButtonAction(_:)), for: .touchUpInside)
        cell.notificationButton.tag = indexPath.row
        cell.notificationButton.setTitle(object.n_type.lowercased() == "follow" ? "Following" : "Follow", for: .normal)
        cell.notificationImageView.image = UIImage.init(named: object.n_image)
        cell.notificationTitleLabel.text = object.n_name
        cell.notificationTextLabel.text = object.n_text
        cell.notificationTimeLabel.text = object.n_date
        cell.notificationTypeLabel.text = object.n_type
        return cell
    }
    
    //MARK:- TableView Method For Managing Height Of Row
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
