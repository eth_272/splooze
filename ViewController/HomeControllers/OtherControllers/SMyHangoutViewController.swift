//
//  SMyHangoutViewController.swift
//  Splooze
//
//  Created by ETHANEMAC on 16/05/19.
//  Copyright © 2019 Deepak Kumar. All rights reserved.
//

import UIKit

class SMyHangoutViewController: UIViewController {
    
    var dataArray = [HistoryEventDetail]()
    
    //MARK:- UIView life cycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataArray = HistoryEventDetailInfo().HistorydetailArray
    }
    
    //Mark:- IBAction buttons
    @IBAction func BACKBUTTON(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension SMyHangoutViewController : UITableViewDelegate, UITableViewDataSource {
    //MARK:- TableView Delegate and datasource Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SMyHangoutsTableViewCell", for: indexPath) as! SMyHangoutsTableViewCell
        
        let object = dataArray[indexPath.row]
        cell.EventNameLabel.text = (object.ticketEventNameString)
        cell.addressLabel.text = (object.address)
        cell.eventImage?.image = UIImage(named:object.image)
        cell.locationIcon?.image = UIImage(named: object.locationicon)
        return cell
    }
    
    //MARK:- TableView Method For Managing Height Of Row
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = HomeStoryboard.instantiateViewController(withIdentifier: "SEventDetailViewController") as! SEventDetailViewController
        //        controller.modalTransitionStyle = .flipHorizontal
        controller.modalTransitionStyle = .coverVertical
        let navNew = UINavigationController.init(rootViewController: controller)
        self.present(navNew, animated: true, completion: nil)
    }
}
