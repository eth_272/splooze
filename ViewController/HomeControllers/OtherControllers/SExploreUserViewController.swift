//
//  SExploreUserViewController.swift
//  Splooze
//
//  Created by ETHANEMAC on 17/05/19.
//  Copyright © 2019 Deepak Kumar. All rights reserved.
//

import UIKit
import UnderLineTextField

class SExploreUserViewController: UIViewController {
        
    //MARK:- IBOutlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchTextField: UnderLineTextField!
    
    //MARK:- UIView life cycle methods
    override func viewDidLoad() {
        self.navigationController?.isNavigationBarHidden = true
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }
}

extension SExploreUserViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    //MARK:- Collection View Methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SUsersCollectionViewCell", for: indexPath) as! SUsersCollectionViewCell
        
        cell.userName?.text = indexPath.row % 2 == 0 ? "Naddia Osminy" : "Naddia"
        cell.userImageView?.image = UIImage(named: indexPath.row % 2 == 0 ? "img_1" : "img_2")
        
        return cell
    }
    
    //MARK:- Collection View Methods For Managing FlowLayOut
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = Window_Width == 320 ? collectionView.frame.size.width / 3 - 9 : Window_Width == 375 ? collectionView.frame.size.width / 3 - 12 : collectionView.frame.size.width / 3 - 13
        return CGSize(width:width ,height: width * 1.157 )
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let controller = HomeStoryboard.instantiateViewController(withIdentifier: "SOtherUserProfileViewController") as! SOtherUserProfileViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

extension SExploreUserViewController: UnderLineTextFieldDelegate, UITextFieldDelegate {
    
    //==================================
    // MARK: - UnderLineTextField Delegate
    //==================================
    func textFieldValidate(underLineTextField: UnderLineTextField) throws {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.textInputMode?.primaryLanguage == "emoji") || !((textField.textInputMode?.primaryLanguage) != nil) {
            return false
        }
        
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        if str.length > 50 || string == " " {
            return false
        }

        return true
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textFieldTextChanged(underLineTextField: UnderLineTextField) {
        print("im empty \(underLineTextField)")
    }
    
}
