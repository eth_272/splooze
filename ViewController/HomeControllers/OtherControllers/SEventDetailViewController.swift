//
//  SEventDetailViewController.swift
//  Splooze
//
//  Created by ETHANEMAC on 17/04/19.
//  Copyright © 2019 Deepak Kumar. All rights reserved.
//


import UIKit
import MapKit


class SEventDetailViewController: UIViewController {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    
    @IBOutlet weak var totalSeatLabel: UILabel!
    @IBOutlet weak var eventNameLabel: UILabel!
    @IBOutlet weak var eventDateLabel: UILabel!
    @IBOutlet weak var joinMemberButton: UILabel!
    @IBOutlet weak var eventAddressLabel: UILabel!
    @IBOutlet weak var eventDistanceLabel: UILabel!
    @IBOutlet weak var amountPayableLabel: UILabel!
    
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var eventImageView: UIImageView!
    @IBOutlet weak var numberOfSeatsImage: UIImageView!
    @IBOutlet weak var joinMemberOneImage: UIImageView!
    @IBOutlet weak var joinMemberTwoImage: UIImageView!
    @IBOutlet weak var joinMemberThreeImage: UIImageView!

    @IBOutlet weak var seatCollectionView: UICollectionView!
    @IBOutlet weak var foodCollectionView: UICollectionView!
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var eventMapView: MKMapView!
    @IBOutlet weak var eventTableView: UITableView!
    
    var seatSelect = ""
    var foodArray = [UserInfo]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        navView.backgroundColor = .clear
        self.beverageSetup()
        
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Helper Methods
    func beverageSetup (){
        for i in 0..<10{
            let obj = UserInfo()
            obj.foodImage = "logo"
            obj.foodName = "5 packs corn"
            obj.foodAmount = "R 67"
            obj.foodCount = "\(i)"
            foodArray.append(obj)
        }
    }
    
    //MARK:- UIButton Actions
    @IBAction func backAction(_ sender: UIButton) {
        self.dismiss(animated: true) {
            //call delegate
        }
    }
    
    @IBAction func searchAction(_ sender: UIButton) {
        _ = AlertController.alert("", message: WIP)
    }
    
    @IBAction func shareAction(_ sender: UIButton) {
        _ = AlertController.alert("", message: WIP)
    }
    @IBAction func joinMemberOneAction(_ sender: UIButton) {
        let controller = HomeStoryboard.instantiateViewController(withIdentifier: "SOtherUserProfileViewController") as! SOtherUserProfileViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func joinMemberTwoAction(_ sender: UIButton) {
        let controller = HomeStoryboard.instantiateViewController(withIdentifier: "SOtherUserProfileViewController") as! SOtherUserProfileViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func joinMemberThreeAction(_ sender: UIButton) {
        let controller = HomeStoryboard.instantiateViewController(withIdentifier: "SUsersListViewController") as! SUsersListViewController
        controller.userType = .participantList
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func interestedButtonAction(_ sender: UIButton) {

        if (sender.title(for: .normal)?.lowercased().contains("interested"))! {
            sender.setTitle("Book Now".uppercased(), for: .normal)
        } else {
            _ = AlertController.alert("", message: WIP)
        }
    }
    
    @objc func addAction(_sender:UIButton){
        
        let index = _sender.tag - 1000
        let obj = self.foodArray[index]
        var myInt : Int = (obj.foodCount as NSString).integerValue
        myInt = myInt + 1
        obj.foodCount = "\(myInt)"
        self.foodArray[index] = obj
        self.foodCollectionView.reloadData()

    }
    
    @objc func delAction(_sender:UIButton){
        
        let index = _sender.tag - 2000
        let obj = self.foodArray[index]
        var myInt : Int = (obj.foodCount as NSString).integerValue
        myInt = 0
        obj.foodCount = "\(myInt)"
        self.foodArray[index] = obj
        self.foodCollectionView.reloadData()
        
    }
    
    //MARK:- ScrollView method
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.eventTableView {
            if eventTableView.contentOffset.y >= 96.0{
                UIView.animate(withDuration: 0.5) {
                    self.navView.backgroundColor = #colorLiteral(red: 0.9843137255, green: 0.01568627451, blue: 0.0431372549, alpha: 0.9022634846)
                }
            } else {
                navView.backgroundColor = .clear
            }
        }
    }
    
}

//MARK:- UITableView Delegate datasource
extension SEventDetailViewController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SEventDetailTableViewCell") as! SEventDetailTableViewCell
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

//MARK:- UICollection View delegate and datasource
extension SEventDetailViewController : UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView.tag == 10{
            return 10
        }else{
            return foodArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == 10{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SEventSeatCollectionViewCell", for: indexPath) as! SEventSeatCollectionViewCell
            cell.seatLabel.text = "\(indexPath.row + 1)"
            
            if self.seatSelect != "\(indexPath.row)"{
                cell.seatView.backgroundColor = .clear
            }else{
                cell.seatView.backgroundColor = #colorLiteral(red: 0.7803921569, green: 0.01176470588, blue: 0.05882352941, alpha: 1)
            }
            
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SEventFoodCollectionViewCell", for: indexPath) as! SEventFoodCollectionViewCell
            let obj = self.foodArray[indexPath.row]
            cell.addButton.tag = indexPath.row + 1000
            cell.deleteButton.tag = indexPath.row + 2000
            
            cell.addButton.addTarget(self, action: #selector(addAction), for: .touchUpInside)
            cell.deleteButton.addTarget(self, action: #selector(delAction), for: .touchUpInside)
            cell.foodLabel.text = obj.foodName
            cell.quantityLabel.text = obj.foodCount
            cell.amountLabel.text = obj.foodAmount
            
            if obj.foodCount == "0"
            {
                cell.deleteButton.isHidden = true
                cell.quantityLabel.isHidden = true
                
            }else{
                cell.deleteButton.isHidden = false
                cell.quantityLabel.isHidden = false
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 10{
            
            self.seatSelect = "\(indexPath.row)"
            self.seatCollectionView.reloadData()
        }else{
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: collectionView.tag == 10 ? 24 : 125, height: collectionView.tag == 10 ? 24 : 135)
    }
}

class SEventDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var descriptionLabel: UILabel!
    
}

class SEventSeatCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var seatLabel: UILabel!
    @IBOutlet weak var seatView: UIView!
    
}

class SEventFoodCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var foodImage: UIImageView!
    @IBOutlet weak var foodLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!

}
