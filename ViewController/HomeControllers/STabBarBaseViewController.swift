//
//  STabBarBaseViewController.swift
//  Splooze
//
//  Created by ETHANEMAC on 26/03/19.
//  Copyright © 2019 Deepak Kumar. All rights reserved.
//

import UIKit

class STabBarBaseViewController: UITabBarController, UITabBarControllerDelegate  {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.delegate = self
        self.tabBar.barTintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.09655928938)
        self.tabBar.backgroundColor = .clear
        
    }
    

  

}
