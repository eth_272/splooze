//
//  SClubAddEventViewController.swift
//  Splooze
//
//  Created by ETHANEMAC on 15/05/19.
//  Copyright © 2019 Deepak Kumar. All rights reserved.
//

import UIKit
import UnderLineTextField

class SClubAddEventViewController: UIViewController {
    
    var eventInfo = EventInfo()
    var errorArray = [false, false, true, false, false, true, false, true, true, false]
    var picker = UIImagePickerController()
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var categoryTextField: UnderLineTextField!
    @IBOutlet weak var eventNameTextField: UnderLineTextField!
    @IBOutlet weak var eventDateTextField: UnderLineTextField!
    @IBOutlet weak var eventTimeTextField: UnderLineTextField!
    @IBOutlet weak var eventSeatTextField: UnderLineTextField!
    @IBOutlet weak var eventImageTextField: UnderLineTextField!
    @IBOutlet weak var eventVenueTextField: UnderLineTextField!
    @IBOutlet weak var eventAmountTextField: UnderLineTextField!
    @IBOutlet weak var eventBeveragesTextField: UnderLineTextField!
    @IBOutlet weak var shortDescriptionTextField: UnderLineTextField!
    
    @IBOutlet weak var eventTableViewLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var eventTableViewRightConstraint: NSLayoutConstraint!
    @IBOutlet weak var seatTextFieldHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var beverageTextFieldHeightConstraint: NSLayoutConstraint!
    
    
    //MARK:- UIView life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        // Do any additional setup after loading the view.
        
        initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    //MARK:- Helper Methods
    func initialSetup() {
        
        eventTableViewLeftConstraint.constant = Window_Width == 320 ? 30 : Window_Width == 375 ? 40 : 50
        eventTableViewRightConstraint.constant = Window_Width == 320 ? 30 : Window_Width == 375 ? 40 : 50
        
        if getUserDefaultValue("userType") == "UR" || getUserDefaultValue("userType").lowercased() == "ur" {
            seatTextFieldHeightConstraint.constant = 0
            beverageTextFieldHeightConstraint.constant = 0
            eventSeatTextField.isHidden = true
            eventBeveragesTextField.isHidden = true
        }
        
    }
    
    //MARK:- UIButton Action Methods
    @IBAction func categoryButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        RPicker.sharedInstance.selectOption(dataArray: ["Music", "Singing", "Dance"], selectedIndex: 0) { (selectedValue, selectedIndex) in
            self.eventInfo.eventCategory = selectedValue
            self.categoryTextField.text = self.eventInfo.eventCategory
        }
        
    }
    
    @IBAction func eventDateButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        RPicker.selectDate(datePickerMode: .date, selectedDate: Date(), minDate: Date(), maxDate: nil) { (selectedDate) in
            self.eventInfo.eventDate = convertToDate(date: selectedDate, format: "dd-MM-yyyy")
            self.eventDateTextField.text = self.eventInfo.eventDate
        }
        
    }
    
    @IBAction func eventTimeButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        RPicker.selectDate(datePickerMode: .time, selectedDate: Date(), minDate: Date(), maxDate: nil) { (selectedDate) in
            self.eventInfo.eventDate = convertToDate(date: selectedDate, format: "HH:mm")
            self.eventDateTextField.text = self.eventInfo.eventDate
        }
        
    }
    
    @IBAction func uploadPhotoButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.optionForImageUpload()
        
    }
    
    @IBAction func beveragesButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        let controller = HomeStoryboard.instantiateViewController(withIdentifier: "SBeveragesListViewController") as! SBeveragesListViewController
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    @IBAction func venueButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        //open google place picker
        _ = AlertController.alert("", message: WIP)
        
    }
    
    @IBAction func submitButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        let filtered = errorArray.filter { $0 == false }
        
        if filtered.count <= 0 {
            _ = AlertController.alert("Thanks!", message: "Your are successfully add an event. Please wait for admin approval.", acceptMessage: "OK", acceptBlock: {
                //Move on home
            })
        } else if !errorArray[0] {
            do {
                try (self.view.viewWithTag(1000) as! UnderLineTextField).validate()
            } catch {
                // handle error
            }
        } else if !errorArray[1] {
            do {
                try (self.view.viewWithTag(1001) as! UnderLineTextField).validate()
            } catch {
                // handle error
            }
        } else if !errorArray[3] {
            do {
                try (self.view.viewWithTag(1003) as! UnderLineTextField).validate()
            } catch {
                // handle error
            }
        } else if !errorArray[4] {
            do {
                try (self.view.viewWithTag(1004) as! UnderLineTextField).validate()
            } catch {
                // handle error
            }
        } else if !errorArray[6] {
            do {
                try (self.view.viewWithTag(1006) as! UnderLineTextField).validate()
            } catch {
                // handle error
            }
        } else if !errorArray[9] {
            do {
                try (self.view.viewWithTag(1009) as! UnderLineTextField).validate()
            } catch {
                // handle error
            }
        }
    }
    
}

extension SClubAddEventViewController: UnderLineTextFieldDelegate, UITextFieldDelegate {
    
    //==================================
    // MARK: - UnderLineTextField Delegate
    //==================================
    func textFieldValidate(underLineTextField: UnderLineTextField) throws {
        switch underLineTextField.tag {
        case 1000:
            let verifyObj = ValidationClass.verifyEventName(text: underLineTextField.text!)
            if !verifyObj.1 {
                throw UnderLineTextFieldErrors
                    .error(message: verifyObj.0)
            } else {
                errorArray[0] = true
            }
        case 1001:
            let verifyObj = ValidationClass.verifyCategroyName(text: underLineTextField.text!)
            if !verifyObj.1 {
                throw UnderLineTextFieldErrors
                    .error(message: verifyObj.0)
            } else {
                errorArray[1] = true
            }
        case 1003:
            let verifyObj = ValidationClass.verifyDate(text: underLineTextField.text!)
            if !verifyObj.1 {
                throw UnderLineTextFieldErrors
                    .error(message: verifyObj.0)
            } else {
                errorArray[3] = true
            }
        case 1004:
            let verifyObj = ValidationClass.verifyTime(text: underLineTextField.text!)
            if !verifyObj.1 {
                throw UnderLineTextFieldErrors
                    .error(message: verifyObj.0)
            } else {
                errorArray[4] = true
            }
        case 1006:
            let verifyObj = ValidationClass.verifyUploadImage(text: underLineTextField.text!)
            if !verifyObj.1 {
                throw UnderLineTextFieldErrors
                    .error(message: verifyObj.0)
            } else {
                errorArray[6] = true
            }
        case 1009:
            let verifyObj = ValidationClass.verifyVenue(text: underLineTextField.text!)
            if !verifyObj.1 {
                throw UnderLineTextFieldErrors
                    .error(message: verifyObj.0)
            } else {
                errorArray[9] = true
            }
        default:
            break
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let nextTextField = view.viewWithTag(textField.tag + 1) else {
            view.endEditing(true)
            return false
        }
        _ = nextTextField.becomeFirstResponder()
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.textInputMode?.primaryLanguage == "emoji") || !((textField.textInputMode?.primaryLanguage) != nil) {
            return false
        }
        
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        switch textField.tag {
        case 1000:
            if str.length > 80 {
                return false
            }
            break
        case 1002:
            if str.length > 256 {
                return false
            }
            break
        case 1005:
            if str.length > 5 || string == " " {
                return false
            }
            break
        case 1007:
            if str.length > 4 || string == " " {
                return false
            }
            break
        default:
            break
        }
        return true
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 1000:
            eventInfo.eventName = textField.text!.trimWhiteSpace
            eventNameTextField.text = eventInfo.eventName
            break
        case 1002:
            eventInfo.eventDescription = textField.text!.trimWhiteSpace
            shortDescriptionTextField.text = eventInfo.eventDescription
            break
        case 1005:
            eventInfo.eventAmount = textField.text!.trimWhiteSpace
            eventAmountTextField.text = eventInfo.eventAmount
            break
        case 1007:
            eventInfo.eventSeatCount = textField.text!.trimWhiteSpace
            eventSeatTextField.text = eventInfo.eventSeatCount
            break
        default:
            break
        }
    }
    
    func textFieldTextChanged(underLineTextField: UnderLineTextField) {
        print("im empty \(underLineTextField)")
    }
    
}

// MARK: Image Picker Handler
extension SClubAddEventViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func optionForImageUpload() {
        self.view.endEditing(true)
        
        _ = AlertController.actionSheet("Choose image", message: "", sourceView: self.view, buttons: ["Camera","Gallery","Cancel"], tapBlock: { (action, index) in
            if index == 0 {
                self.openCamera()
            } else if index == 1 {
                self.openGallery()
            }
        })
    }
    
    func openCamera() {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {
            picker.delegate = self
            picker.isEditing = true
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.navigationBar.tintColor = UIColor.black
            self .present(picker, animated: true, completion: nil)
        } else {
            openGallery()
        }
    }
    
    func openGallery() {
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        picker.delegate = self
        if UIDevice.current.userInterfaceIdiom == .phone {
            self.present(picker, animated: true, completion: nil)
            picker.navigationBar.tintColor = UIColor.black
        } else {
            
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        picker .dismiss(animated: true, completion: nil)
        var imageData = Data()
        imageData = ((info[UIImagePickerController.InfoKey.originalImage] as? UIImage)?.jpegData(compressionQuality: 0.2))!
        self.eventInfo.eventImageData = imageData
        
        if let url =  info[.mediaURL] as? URL {
            Debug.log("\(url)")
            self.eventInfo.eventImageName = "\(url)"
            self.eventImageTextField.text = self.eventInfo.eventImageName
        }
        
        if let images = info[.originalImage] as? UIImage {
            self.eventInfo.eventImage = images
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker .dismiss(animated: true, completion: nil)
    }
    
}
