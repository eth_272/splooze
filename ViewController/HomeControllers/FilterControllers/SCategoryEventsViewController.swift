//
//  SCategoryEventsViewController.swift
//  Splooze
//
//  Created by ETHANEMAC on 28/03/19.
//  Copyright © 2019 Deepak Kumar. All rights reserved.
//

import UIKit

class SCategoryEventsViewController: UIViewController,iCarouselDataSource, iCarouselDelegate {
    
    @IBOutlet weak var carousalView: iCarousel!
    @IBOutlet weak var categoryNameButton: UIButton!
    @IBOutlet weak var changeCategoryButton: UIButton!
    @IBOutlet weak var eventCollectionView: UICollectionView!
    var categoryArray = [UserInfo]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        carousalView.type = .coverFlow
        for i in (0..<20){
            let obj = UserInfo()
            let _ = (i%2 == 0) ? (obj.categoryImage = "img_1" , obj.categoryName = "Real Music") : (obj.categoryImage = "img_3" , obj.categoryName = "Guitar Event")
            self.categoryArray.append(obj)
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.carousalView.isHidden = true
        self.eventCollectionView.isHidden = false
    }
    
    //MARK:- Carousal View Delegate
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        return 10
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        
        let subviewArray = Bundle.main.loadNibNamed("CarousalCustomView", owner: self, options: nil)
        return subviewArray![0] as! UIView
    }
    
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        
        let controller = HomeStoryboard.instantiateViewController(withIdentifier: "SEventDetailViewController") as! SEventDetailViewController
        //        controller.modalTransitionStyle = .flipHorizontal
        controller.modalTransitionStyle = .coverVertical
        let navNew = UINavigationController.init(rootViewController: controller)
        self.present(navNew, animated: true, completion: nil)
    }
    
    //MARK:- UIButton Actions
    @IBAction func changeCategoryButtonAction(_ sender: UIButton) {
//        let controller = HomeStoryboard.instantiateViewController(withIdentifier: "SCategoryListViewController") as! SCategoryListViewController
//        controller.modalPresentationStyle = .currentContext
//        controller.modalTransitionStyle = .crossDissolve
//        self.present(controller, animated: true, completion: nil)
        self.carousalView.isHidden = true
        self.eventCollectionView.isHidden = false
    }
    
}
//MARK:- collection view delegte and datasource
extension SCategoryEventsViewController :  UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoryArray.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SEventsCollectionViewCell", for: indexPath) as! SEventsCollectionViewCell
        
        cell.postCategory = self.categoryArray[indexPath.row]
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellSquare : CGFloat = Window_Width/3 - 10
        
        return CGSize.init(width: cellSquare, height: cellSquare)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        self.dismiss(animated: true, completion: nil)
        self.carousalView.isHidden = false
        self.eventCollectionView.isHidden = true
    }
}
