//
//  SAllEventsViewController.swift
//  Splooze
//
//  Created by ETHANEMAC on 28/03/19.
//  Copyright © 2019 Deepak Kumar. All rights reserved.
//

import UIKit

class SAllEventsViewController: UIViewController ,iCarouselDataSource, iCarouselDelegate {
    
    @IBOutlet weak var carousalView: iCarousel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        carousalView.type = .coverFlow
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //MARK:- Carousal View Delegate
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        return 10
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        
        let subviewArray = Bundle.main.loadNibNamed("CarousalCustomView", owner: self, options: nil)
//        let subView  = subviewArray![0] as! CarousalCustomView
        return subviewArray![0] as! UIView
    }

    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        
        //move on detail screen
        
        let controller = HomeStoryboard.instantiateViewController(withIdentifier: "SEventDetailViewController") as! SEventDetailViewController
//        controller.modalTransitionStyle = .flipHorizontal
        controller.modalTransitionStyle = .coverVertical
        let navNew = UINavigationController.init(rootViewController: controller)
        self.present(navNew, animated: true, completion: nil)
        
    }
    
}
