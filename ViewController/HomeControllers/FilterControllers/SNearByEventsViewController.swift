//
//  SNearByEventsViewController.swift
//  Splooze
//
//  Created by ETHANEMAC on 28/03/19.
//  Copyright © 2019 Deepak Kumar. All rights reserved.
//

import UIKit

class SNearByEventsViewController: UIViewController,iCarouselDataSource, iCarouselDelegate {
    
    @IBOutlet weak var carousalView: iCarousel!
    
    @IBOutlet weak var nearBySlider: UISlider!
    @IBOutlet weak var nearByButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        carousalView.type = .coverFlow
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        nearBySlider.value = 10.0
        nearByButton.setTitle("\(nearBySlider.value)", for: .normal)
    }
    
    //MARK:- Carousal View Delegate
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        return 10
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        
        let subviewArray = Bundle.main.loadNibNamed("CarousalCustomView", owner: self, options: nil)
        //        let subView  = subviewArray![0] as! CarousalCustomView
        //        subView.rewardLabel.text = "\(self.dataList[index].name)"
        //        //    subSiew.carousalImg.image = UIImage(named: "reward-bg-purple")
        //        subView.redeemButton.isHidden = true
        //        subView.titleLabel.isHidden = true
        return subviewArray![0] as! UIView
    }
    
    //    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
    //        if (option == .spacing) {
    //            return value * 0.95
    //
    //        }
    //        return value
    //    }
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        
        let controller = HomeStoryboard.instantiateViewController(withIdentifier: "SEventDetailViewController") as! SEventDetailViewController
        //        controller.modalTransitionStyle = .flipHorizontal
        controller.modalTransitionStyle = .coverVertical
        let navNew = UINavigationController.init(rootViewController: controller)
        self.present(navNew, animated: true, completion: nil)
    }
    
    //MARK:- UISlider Action
    
    @IBAction func nearBySlider(_ sender: UISlider) {
        nearByButton.setTitle("\(sender.value)", for: .normal)
    }
    
}
