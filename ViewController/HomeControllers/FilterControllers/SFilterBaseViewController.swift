//
//  SFilterBaseViewController.swift
//  Splooze
//
//  Created by ETHANEMAC on 28/03/19.
//  Copyright © 2019 Deepak Kumar. All rights reserved.
//

import UIKit

class SFilterBaseViewController: GLViewPagerViewController,GLViewPagerViewControllerDataSource,GLViewPagerViewControllerDelegate {
    
    // MARK: - cache properties
    var viewControllers: NSArray = NSArray()
    var tabTitles: NSArray = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationController?.isNavigationBarHidden = true
        let imageView = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: Window_Width, height: Window_Height + 100))
        imageView.image = UIImage(named: "home_bg")
        
        self.view.addSubview(imageView)
        self.view.sendSubviewToBack(imageView)
//        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "home_bg")!)
        self.setDataSource(newDataSource: self)
        self.setDelegate(newDelegate: self)
        self.padding = 10
        self.leadingPadding = 10
        self.trailingPadding = 10
        self.defaultDisplayPageIndex = 0
        self.tabAnimationType = GLTabAnimationType.GLTabAnimationType_WhileScrolling
        self.indicatorColor = #colorLiteral(red: 0.8970001936, green: 0.2620319426, blue: 0.2811509669, alpha: 1)
        self.supportArabic = false
        self.fixTabWidth = true
        self.fixIndicatorWidth = false
//            self.indicatorWidth = 20.0
        self.viewControllers = [
            HomeStoryboard.instantiateViewController(withIdentifier: "SAllEventsViewController") as! SAllEventsViewController,
            HomeStoryboard.instantiateViewController(withIdentifier: "SPremiumEventsViewController") as! SPremiumEventsViewController,
            HomeStoryboard.instantiateViewController(withIdentifier: "SNearByEventsViewController") as! SNearByEventsViewController,
            HomeStoryboard.instantiateViewController(withIdentifier: "SCategoryEventsViewController") as! SCategoryEventsViewController,
            HomeStoryboard.instantiateViewController(withIdentifier: "SCityEventsViewController") as! SCityEventsViewController
            
        ]
        
        self.tabTitles = [ "All",
                           "Premium" ,
                           "Near By" ,
                           "Category",
                           "City"
        ]
        
    }
    
    // MARK: - GLViewPagerViewControllerDataSource
    func numberOfTabsForViewPager(_ viewPager: GLViewPagerViewController) -> Int {
        return self.viewControllers.count
    }
    
    func viewForTabIndex(_ viewPager: GLViewPagerViewController, index: Int) -> UIView {
        let label:UILabel = UILabel.init()
        label.text = self.tabTitles.object(at: index) as? String
        label.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        label.textAlignment = NSTextAlignment.center
        label.transform = CGAffineTransform.init(scaleX: 0.9, y: 0.9)
        return label
    }
    
    func contentViewControllerForTabAtIndex(_ viewPager: GLViewPagerViewController, index: Int) -> UIViewController {
        return self.viewControllers.object(at: index) as! UIViewController
    }
    
    // MARK: - GLViewPagaerViewControllerDelegate
    func didChangeTabToIndex(_ viewPager: GLViewPagerViewController, index: Int, fromTabIndex: Int) {
        let prevLabel:UILabel = viewPager.tabViewAtIndex(index: fromTabIndex) as! UILabel
        let currentLabel:UILabel = viewPager.tabViewAtIndex(index: index) as! UILabel
        prevLabel.transform = CGAffineTransform.identity.scaledBy(x: 0.9, y: 0.9)
        currentLabel.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0)
        prevLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        currentLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    func willChangeTabToIndex(_ viewPager: GLViewPagerViewController, index: Int, fromTabIndex: Int, progress: CGFloat) {
        if fromTabIndex == index {
            return;
        }
        
        let prevLabel:UILabel = viewPager.tabViewAtIndex(index: fromTabIndex) as! UILabel
        let currentLabel:UILabel = viewPager.tabViewAtIndex(index: index) as! UILabel
        prevLabel.transform = CGAffineTransform.identity.scaledBy(x: 1.0 - (0.1 * progress), y: 1.0 - (0.1 * progress))
        currentLabel.transform = CGAffineTransform.identity.scaledBy(x: 0.9 + (0.1 * progress), y: 0.9 + (0.1 * progress))
        currentLabel.textColor =  #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        prevLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    func widthForTabIndex(_ viewPager: GLViewPagerViewController, index: Int) -> CGFloat {
        let prototypeLabel:UILabel = UILabel.init()
        prototypeLabel.text = self.tabTitles.object(at: index) as? String
        prototypeLabel.textAlignment = NSTextAlignment.center
        prototypeLabel.font = UIFont.systemFont(ofSize: 16.0)
        return prototypeLabel.intrinsicContentSize.width
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

