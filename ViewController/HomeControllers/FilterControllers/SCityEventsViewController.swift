//
//  SCityEventsViewController.swift
//  Splooze
//
//  Created by ETHANEMAC on 28/03/19.
//  Copyright © 2019 Deepak Kumar. All rights reserved.
//

import UIKit

class SCityEventsViewController: UIViewController,iCarouselDataSource, iCarouselDelegate {
    
    @IBOutlet weak var carousalView: iCarousel!
    @IBOutlet weak var cityNameButton: UIButton!
    @IBOutlet weak var changeCityButton: UIButton!
    @IBOutlet weak var eventCollectionView: UICollectionView!
    var cityArray = [UserInfo]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        carousalView.type = .coverFlow
        for i in (0..<20){
            let obj = UserInfo()
            obj.cityName = "Luanda, Angola"
            let _ = (i%2 == 0) ? (obj.cityImage = "pic_2") : (obj.cityImage = "pic_3")
            self.cityArray.append(obj)
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.carousalView.isHidden = true
        self.eventCollectionView.isHidden = false
    }
    
    //MARK:- Carousal View Delegate
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        return 10
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        
        let subviewArray = Bundle.main.loadNibNamed("CarousalCustomView", owner: self, options: nil)
        return subviewArray![0] as! UIView
    }
    
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        
        let controller = HomeStoryboard.instantiateViewController(withIdentifier: "SEventDetailViewController") as! SEventDetailViewController
        //        controller.modalTransitionStyle = .flipHorizontal
        controller.modalTransitionStyle = .coverVertical
        let navNew = UINavigationController.init(rootViewController: controller)
        self.present(navNew, animated: true, completion: nil)
    }
    
    //MARK:- UIButton Action
    
    @IBAction func changeCityButtonAction(_ sender: UIButton) {
//        let controller = HomeStoryboard.instantiateViewController(withIdentifier: "SCityListViewController") as! SCityListViewController
//        controller.modalPresentationStyle = .currentContext
//        controller.modalTransitionStyle = .crossDissolve
//        self.present(controller, animated: true, completion: nil)
        self.carousalView.isHidden = true
        self.eventCollectionView.isHidden = false
    }
    
}

//MARK:- collection view delegte and datasource
extension SCityEventsViewController :  UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cityArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SEventsCollectionViewCell", for: indexPath) as! SEventsCollectionViewCell
        cell.postCity = self.cityArray[indexPath.row]
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellSquare : CGFloat = Window_Width/3 - 10
        
        return CGSize.init(width: cellSquare, height: cellSquare)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.carousalView.isHidden = false
        self.eventCollectionView.isHidden = true
//        self.dismiss(animated: true, completion: nil)
    }
}
