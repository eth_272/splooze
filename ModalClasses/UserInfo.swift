//
//  UserInfo.swift
//  Molla Kuqe
//
//  Created by ETHANEMAC on 22/10/18.
//  Copyright © 2018 ETHANE TECHNOLOGIES PVT. LTD. All rights reserved.
//

import UIKit

class UserInfo: NSObject {
    
    static let sharedInstance = UserInfo()
    
    var email = ""
    var password = ""
    var confirmPassword = ""
    var contact = ""
    var gender = ""
    var firstName = ""
    var lastName = ""
    var latitude = ""
    var longitude = ""
    var city = ""
    var state = ""
    var country = ""
    var restro_clubName = ""
    var restro_clubAddress = ""
    var additionalComment = ""
    var DOB = ""
    var OTP = ""
    var isOTPVerified = false
    var provideType = ""
    var provideId = ""
    var userId = ""
    var userType = ""//0 for end user and 1 for club owner
    var userImage = ""
    var userImageIcon = UIImage()
    
    //Event
    var categoryName = ""
    var categoryImage = ""
    var cityName = ""
    var cityImage = ""
    
    //
    var seatSelect = ""
    
    var foodImage = ""
    var foodName = ""
    var foodAmount = ""
    var foodCount = ""
    
    
    
    class func setSharedInstance(objectData : UserInfo) {
        var object = UserInfo.sharedInstance
        object = objectData
        Debug.log("\(object)")
    }
    
    class func getUserProfileDetail(dict: [String : Any]) -> UserInfo {
        let object = UserInfo()
        
        object.userId = dict.validatedValue("id", expected: "" as AnyObject) as! String
        object.email = dict.validatedValue("email", expected: "" as AnyObject) as! String
        object.contact = dict.validatedValue("contact", expected: "" as AnyObject) as! String
        object.firstName = dict.validatedValue("first_name", expected: "" as AnyObject) as! String
        object.lastName = dict.validatedValue("last_name", expected: "" as AnyObject) as! String
        object.userType = dict.validatedValue("user_type", expected: "" as AnyObject) as! String
        object.gender = dict.validatedValue("gender", expected: "" as AnyObject) as! String
        object.DOB = dict.validatedValue("dob", expected: "" as AnyObject) as! String
        object.provideId = dict.validatedValue("provider_id", expected: "" as AnyObject) as! String
        object.provideType = dict.validatedValue("provider_type", expected: "" as AnyObject) as! String
        object.OTP = dict.validatedValue("otp_token", expected: "" as AnyObject) as! String
        object.userImage = dict.validatedValue("avatar", expected: "" as AnyObject) as! String
        object.isOTPVerified = dict.validatedValue("is_active", expected: false as AnyObject) as! Bool
        self.setSharedInstance(objectData: object)
        return object
    }
    
}

class EventInfo : NSObject {
    
    var eventId = ""
    var eventName = ""
    var eventCategory = ""
    var eventDescription = ""
    var eventDate = ""
    var eventTime = ""
    var eventAmount = ""
    var eventImageName = ""
    var eventImageData = Data()
    var eventImage = UIImage()
    var eventSeatCount = ""
    var eventBeverageList = [EventBeverageInfo]()
    var eventVenue = ""
    var eventLat = 0.0
    var eventLong = 0.0
    
}

class EventBeverageInfo : NSObject {
    
    var beverageName = ""
    var beveragePrice = ""
    var beverageId = ""
    var beverageImageData = Data()
    var beverageImage = UIImage()
    
}

struct HistoryEventDetail {
    var id = ""
    var address = ""
    var image = ""
    var ticketEventNameString = ""
    var locationicon = ""
    var EventLabel = ""
    
    init (){
    }
    
    init(id: String, image:String,address: String,ticketEventNameString: String,locationicon:String) {
        self.id = id
        self.image = image
        self.address = address
        self.ticketEventNameString = ticketEventNameString
        self.locationicon = locationicon
    }
    
    
}

class HistoryEventDetailInfo : NSObject {
    
    let HistorydetailArray = [
        HistoryEventDetail(id: "1", image: "1",address:"A-45 Sec 64 Noida Uttar Pradesh 201307",ticketEventNameString: "Let's Dance",locationicon:"map"),
        HistoryEventDetail(id: "2", image: "2",address:"A-45 Sec 64 Noida Uttar Pradesh 201307",ticketEventNameString: "Let's Dance",locationicon:"map"),
        HistoryEventDetail(id: "3", image: "3",address:"A-45 Sec 64 Noida Uttar Pradesh 201307",ticketEventNameString: "Let's Dance",locationicon:"map"),
        HistoryEventDetail(id: "4", image: "4",address:"A-45 Sec 64 Noida Uttar Pradesh 201307",ticketEventNameString: "Let's Dance",locationicon:"map"),
        HistoryEventDetail(id: "5", image: "5",address:"A-45 Sec 64 Noida Uttar Pradesh 201307",ticketEventNameString: "Let's Dance",locationicon:"map"),
        HistoryEventDetail(id: "6", image: "6",address:"A-45 Sec 64 Noida Uttar Pradesh 201307",ticketEventNameString: "Let's Dance",locationicon:"map"),
    ]
}

struct NotificationInfo {
    var n_id = ""
    var n_image = ""
    var n_name = ""
    var n_text = ""
    var n_date = ""
    var n_type = ""
    var n_status = ""//follow status
    
    init (){
    }
    
    init(n_id: String, n_image:String,n_name: String,n_text: String,n_date:String,n_type: String,n_status:String) {
        self.n_id = n_id
        self.n_image = n_image
        self.n_name = n_name
        self.n_text = n_text
        self.n_date = n_date
        self.n_type = n_type
        self.n_status = n_status
    }
}

class NotificationListInfo : NSObject {
    
    let NotificationArray = [
        NotificationInfo.init(n_id: "1", n_image: "i_1", n_name: "Music Club", n_text: "A 45, Sector 63, Noida, Uttar Pradesh 201301", n_date: "Wednesday, 22 May 2019, 5:50PM", n_type: "Created Event", n_status: "1"),
        NotificationInfo.init(n_id: "1", n_image: "img_1", n_name: "Naddia", n_text: "Naddia accept your follow request.", n_date: "Wednesday, 22 May 2019, 5:50PM", n_type: "Follow", n_status: "1"),
        NotificationInfo.init(n_id: "1", n_image: "profile_pic", n_name: "Shishir", n_text: "Shishir started following you.", n_date: "Wednesday, 22 May 2019, 5:50PM", n_type: "Follower", n_status: "1"),
        NotificationInfo.init(n_id: "1", n_image: "i_3", n_name: "Celebration Event", n_text: "Someone interested in your event.", n_date: "Wednesday, 22 May 2019, 5:50PM", n_type: "Attandance", n_status: "1"),
        NotificationInfo.init(n_id: "1", n_image: "i_5", n_name: "Roaming Ring", n_text: "A 45, Sector 63, Noida, Uttar Pradesh 201301", n_date: "Wednesday, 22 May 2019, 5:50PM", n_type: "Created Event", n_status: "1")
    ]
}

struct EventDetail {
    var id = ""
    var bgImage = ""
    var address = ""
    var pricelabel = ""
    var Costlabel = ""
    var image = ""
    var dayString = ""
    var dateString = ""
    var timeString = ""
    var ticketEventNameString = ""
    var locationicon = ""
    var EventLabel = ""
    
    init (){
    }
    
    init(id: String, image:String,bgImage: String,address: String, pricelabel:String,Costlabel:String, dayString: String, dateString:String, timeString: String,ticketEventNameString: String,locationicon:String) {
        self.id = id
        self.image = image
        self.bgImage = bgImage
        self.address = address
        self.pricelabel = pricelabel
        self.Costlabel = Costlabel
        self.dayString = dayString
        self.dateString = dateString
        self.timeString = timeString
        self.ticketEventNameString = ticketEventNameString
        self.locationicon = locationicon
    }
}

class EventDetailInfo : NSObject {
    
    let detailArray = [
        EventDetail(id: "1", image: "club",bgImage: "ticket_bg" ,address:"A-45 Sec 64 Noida Uttar Pradesh 201307",pricelabel: "Price",Costlabel: "50" ,dayString: "Monday,", dateString:"12-03-2019",timeString:"10:30PM",ticketEventNameString: "Let's Dance",locationicon:"map"),
        EventDetail(id: "2", image: "event_1",bgImage: "ticket_bg" ,address:"A-45 Sec 64 Noida Uttar Pradesh 201307",pricelabel: "Price",Costlabel: "50" ,dayString: "Monday,", dateString:"12-03-2019",timeString:"10:30PM",ticketEventNameString: "Music Club",locationicon:"map"),
        EventDetail(id: "3", image: "event_1",bgImage: "ticket_bg" ,address:"A-45 Sec 64 Noida Uttar Pradesh 201307",pricelabel: "Price",Costlabel: "50" ,dayString: "Monday,", dateString:"12-03-2019",timeString:"10:30PM",ticketEventNameString: "Music Club",locationicon:"map"),
        EventDetail(id: "4", image: "event_1",bgImage: "ticket_bg" ,address:"A-45 Sec 64 Noida Uttar Pradesh 201307",pricelabel: "Price",Costlabel: "50" ,dayString: "Monday,", dateString:"12-03-2019",timeString:"10:30PM",ticketEventNameString: "Music Club",locationicon:"map"),
        EventDetail(id: "5", image: "event_1",bgImage: "ticket_bg" ,address:"A-45 Sec 64 Noida Uttar Pradesh 201307",pricelabel: "Price",Costlabel: "50" ,dayString: "Monday,", dateString:"12-03-2019",timeString:"10:30PM",ticketEventNameString: "Music Club",locationicon:"map"),
        EventDetail(id: "6", image: "event_1",bgImage: "ticket_bg" ,address:"A-45 Sec 64 Noida Uttar Pradesh 201307",pricelabel: "Price",Costlabel: "50" ,dayString: "Monday,", dateString:"12-03-2019",timeString:"10:30PM",ticketEventNameString: "Music Club",locationicon:"map"),
        EventDetail(id: "1", image: "event_1",bgImage: "ticket_bg" ,address:"A-45 Sec 64 Noida Uttar Pradesh 201307",pricelabel: "Price",Costlabel: "50" ,dayString: "Monday,", dateString:"12-03-2019",timeString:"10:30PM",ticketEventNameString: "Music Club",locationicon:"map")
        
    ]
}

struct BeverageDetail {
    var b_id = ""
    var b_image = UIImage(named: "camGray")
    var b_name = ""
    var b_price = ""
    var b_edit_status = false
    
    init (){
    }
    
    init(b_id: String, b_image:UIImage,b_name: String,b_price: String, b_edit_status:Bool) {
        self.b_id = b_id
        self.b_image = b_image
        self.b_name = b_name
        self.b_price = b_price
        self.b_edit_status = b_edit_status
    }
}

class BeverageList : NSObject {
    
    var beverageArray = [
        BeverageDetail.init(b_id: "1", b_image: UIImage(named: "pic_5")!, b_name: "Pop Corn", b_price: "30", b_edit_status: false),
        BeverageDetail.init(b_id: "2", b_image: UIImage(named: "pic_6")!, b_name: "Cold Drink", b_price: "30", b_edit_status: false)
    ]
    
    var beveragesArray = [
        BeverageDetail.init(b_id: "1", b_image: UIImage(named: "pic_5")!, b_name: "Pop Corn", b_price: "30", b_edit_status: false),
        BeverageDetail.init(b_id: "2", b_image: UIImage(named: "pic_6")!, b_name: "Cold Drink", b_price: "30", b_edit_status: false),
        BeverageDetail.init(b_id: "1", b_image: UIImage(named: "pic_5")!, b_name: "Pop Corn", b_price: "30", b_edit_status: false),
        BeverageDetail.init(b_id: "2", b_image: UIImage(named: "pic_6")!, b_name: "Cold Drink", b_price: "30", b_edit_status: false),
        BeverageDetail.init(b_id: "1", b_image: UIImage(named: "pic_5")!, b_name: "Pop Corn", b_price: "30", b_edit_status: false),
        BeverageDetail.init(b_id: "2", b_image: UIImage(named: "pic_6")!, b_name: "Cold Drink", b_price: "30", b_edit_status: false),
        BeverageDetail.init(b_id: "1", b_image: UIImage(named: "pic_5")!, b_name: "Pop Corn", b_price: "30", b_edit_status: false),
        BeverageDetail.init(b_id: "2", b_image: UIImage(named: "pic_6")!, b_name: "Cold Drink", b_price: "30", b_edit_status: false),
        BeverageDetail.init(b_id: "1", b_image: UIImage(named: "pic_5")!, b_name: "Pop Corn", b_price: "30", b_edit_status: false),
        BeverageDetail.init(b_id: "2", b_image: UIImage(named: "pic_6")!, b_name: "Cold Drink", b_price: "30", b_edit_status: false),
        BeverageDetail.init(b_id: "1", b_image: UIImage(named: "pic_5")!, b_name: "Pop Corn", b_price: "30", b_edit_status: false),
        BeverageDetail.init(b_id: "2", b_image: UIImage(named: "pic_6")!, b_name: "Cold Drink", b_price: "30", b_edit_status: false),
        BeverageDetail.init(b_id: "1", b_image: UIImage(named: "pic_5")!, b_name: "Pop Corn", b_price: "30", b_edit_status: false),
        BeverageDetail.init(b_id: "2", b_image: UIImage(named: "pic_6")!, b_name: "Cold Drink", b_price: "30", b_edit_status: false),
        BeverageDetail.init(b_id: "1", b_image: UIImage(named: "pic_5")!, b_name: "Pop Corn", b_price: "30", b_edit_status: false),
        BeverageDetail.init(b_id: "2", b_image: UIImage(named: "pic_6")!, b_name: "Cold Drink", b_price: "30", b_edit_status: false)
    ]
}
