package com.splooze.interfaces;

import android.content.DialogInterface;

public interface DialogListener {
    void onClickOfOk(DialogInterface builder);
}

