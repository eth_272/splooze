package com.splooze.activity;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.splooze.R;
import com.splooze.databinding.ActivityTicketDetailBinding;

public class TicketDetailActivity extends AppCompatActivity implements View.OnClickListener{

    ActivityTicketDetailBinding binding;
    Context context;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_ticket_detail);
        context=TicketDetailActivity.this;
        binding.ivClose.setOnClickListener(this);
    }

    @Override 
    public void onClick(View v) {
        binding.ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
