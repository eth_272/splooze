package com.splooze.activity;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Toast;
import com.splooze.R;
import com.splooze.adapter.SelectBeverageAdapter;
import com.splooze.databinding.ActivitySelectBeveragesBinding;
import com.splooze.interfaces.BeveragesItemListener;
import com.splooze.model.BeveragesModel;

import java.util.ArrayList;


public class SelectBeveragesActivity extends AppCompatActivity implements View.OnClickListener, BeveragesItemListener {
    private ActivitySelectBeveragesBinding binding;
    private Context context;
    private LinearLayoutManager mLayoutManager;
    private ArrayList<BeveragesModel> myList=new ArrayList<>();
    private String [] bevName={"Soft Drink","Burger","Soft Drink","Burger","Soft Drink","Burger"};
    private String [] bevPrice={"13","56","13","56","13","56"};
    private static final int REQUEST_CODE_ASK_PERMISSIONS=2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this, R.layout.activity_select_beverages);
        context= SelectBeveragesActivity.this;
        setOnClickListener();
        setRecyclerView();
    }

    private void setOnClickListener() {
        binding.toolbar.ivToolbarLeft.setOnClickListener(this);
        binding.toolbar.ivToolbarRight.setOnClickListener(this);
        binding.toolbar.tvToolbarTitle.setText("BEVERAGES");
        binding.toolbar.ivToolbarRight.setVisibility(View.VISIBLE);
        binding.toolbar.ivToolbarRight.setImageResource(R.drawable.right);
    }

    private void setRecyclerView() {
        addDataToList();
        mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        binding.rvBeverages.setLayoutManager(mLayoutManager);
        SelectBeverageAdapter adapter=new SelectBeverageAdapter(context,this,myList);
        binding.rvBeverages.setAdapter(adapter);
    }

    private void addDataToList() {
        myList.clear();
        for (int i = 0; i <bevName.length ; i++) {
            BeveragesModel model=new BeveragesModel();
            model.beverageName=bevName[i];
            model.beveragePrice=bevPrice[i];
            model.beverageImage="";
            model.isSelected=false;
            myList.add(model);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ivToolbarLeft:
                finish();
                break;

            case R.id.ivToolbarRight:
                Toast.makeText(context, "Beverages Select Successfully!", Toast.LENGTH_SHORT).show();
                finish();
                break;
        }
    }

    @Override
    public void onClickOfOk(int position,String performOperation) {
        if (performOperation.equalsIgnoreCase("edit")) {

        }
    }


}
