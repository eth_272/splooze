package com.splooze.activity;

import android.annotation.TargetApi;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Toast;
import com.splooze.R;
import com.splooze.databinding.ActivityChangePasswordBinding;

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityChangePasswordBinding changeBinding;
    private Context context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        changeBinding= DataBindingUtil.setContentView(this, R.layout.activity_change_password);
        context = ChangePasswordActivity.this;
        setOnClickListener();
    }
     
   private void setOnClickListener() {
        disableAutoFill();
        changeBinding.btnSubmit.setOnClickListener(this);
        changeBinding.backSignup.setOnClickListener(this);
        changeBinding.etOldPassword.addTextChangedListener(new ChangePasswordActivity.MyWatcher(changeBinding.etOldPassword));
        changeBinding.etNewPassword.addTextChangedListener(new ChangePasswordActivity.MyWatcher(changeBinding.etNewPassword));
        changeBinding.etConfirmPassword.addTextChangedListener(new ChangePasswordActivity.MyWatcher(changeBinding.etConfirmPassword));
    }
    //**checking device version code**//
    @TargetApi(Build.VERSION_CODES.O)
    private void disableAutoFill() {
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O)
            getWindow().getDecorView().setImportantForAutofill(View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS);
    }
    
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_Submit:
                if (checkValidation()) {
                    Toast.makeText(context, "Old password change successfully", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                }
                break;
            case R.id.back_signup:
                finish();
                break;
        }
    }
    //***********************Check validation from clint side*****************************
    private boolean checkValidation() {
        if(changeBinding.etOldPassword.getText().toString().trim().equalsIgnoreCase("")){
            changeBinding.layoutOldPassword.setErrorEnabled(true);
            changeBinding.layoutOldPassword.setError(getString(R.string.ple_enter_old_password));
            changeBinding.etOldPassword.requestFocus();
            return false;
        }
        else  if (changeBinding.etOldPassword.getText().toString().trim().length() < 6) {
            changeBinding.layoutOldPassword.setErrorEnabled(true);
            changeBinding.layoutOldPassword.setError(getString(R.string.pass_contain_6_char));
            changeBinding.etOldPassword.requestFocus();
            return false;
        }
        if(changeBinding.etNewPassword.getText().toString().isEmpty()){
            changeBinding.layoutNewPassword.setErrorEnabled(true);
            changeBinding.layoutNewPassword.setError(getString(R.string.err_pass_empty));
            changeBinding.etNewPassword.requestFocus();
            return false;
        }
        else if(changeBinding.etNewPassword.getText().toString().trim().length()<6){
            changeBinding.layoutNewPassword.setErrorEnabled(true);
            changeBinding.layoutNewPassword.setError(getString(R.string.err_msg_pass_value_not_match));
            changeBinding.etNewPassword.requestFocus();
            return false;
        }

        else if(changeBinding.etConfirmPassword.getText().toString().isEmpty())
        {
            changeBinding.layoutConfirmPassword.setErrorEnabled(true);
            changeBinding.layoutConfirmPassword.setError(getString(R.string.errconfirm_empty));
            changeBinding.etConfirmPassword.requestFocus();
            return false;
        }
        else if(changeBinding.etConfirmPassword.getText().toString().trim().length()<6)
        {
            changeBinding.layoutConfirmPassword.setErrorEnabled(true);
            changeBinding.layoutConfirmPassword.setError(getString(R.string.errconfirm_empty_length_not_match));
            changeBinding.etConfirmPassword.requestFocus();
            return false;
        }
        else if (!changeBinding.etNewPassword.getText().toString().equalsIgnoreCase(changeBinding.etConfirmPassword.getText().toString().trim())) {
            changeBinding.layoutConfirmPassword.setErrorEnabled(true);
            changeBinding.layoutConfirmPassword.setError(getString(R.string.confirm_pass_not_match));
            changeBinding.etConfirmPassword.requestFocus();
            return false;
        }
        return true;
    }

    //****************  Text Watcher Class Working On EditText Field **********************
    public class MyWatcher implements TextWatcher {
        private View view;

        private MyWatcher(View view) {
            this.view = view;
        }


        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            switch (view.getId()) {
                case R.id.et_OldPassword:
                    if (!changeBinding.etOldPassword.getText().toString().trim().isEmpty()) {
                        changeBinding.layoutOldPassword.setErrorEnabled(false);
                    }
                case R.id.et_NewPassword:

                    if (!changeBinding.etNewPassword.getText().toString().trim().isEmpty()) {
                        changeBinding.layoutNewPassword.setErrorEnabled(false);
                    }

                case R.id.et_ConfirmPassword:
                    if (!changeBinding.etConfirmPassword.getText().toString().trim().isEmpty()) {
                        changeBinding.layoutConfirmPassword.setErrorEnabled(false);
                    }

            }
        }
    }
}
