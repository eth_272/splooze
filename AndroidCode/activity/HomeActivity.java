package com.splooze.activity;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import com.splooze.R;
import com.splooze.databinding.ActivityHomeBinding;
import com.splooze.fragment.AddEventFragment;
import com.splooze.fragment.EditClubFragment;
import com.splooze.fragment.EditProfileFragment;
import com.splooze.fragment.EventDetailsFragment;
import com.splooze.fragment.ExploreFragment;
import com.splooze.fragment.ExploreSearchFragment;
import com.splooze.fragment.FollowersFragment;
import com.splooze.fragment.FollowingFragment;
import com.splooze.fragment.HistoryEventFragment;
import com.splooze.fragment.HomeFragment;
import com.splooze.fragment.LikedFragment;
import com.splooze.fragment.NotificationFragment;
import com.splooze.fragment.ParticipantsFragment;
import com.splooze.fragment.ProfileFragment;
import com.splooze.fragment.TicketListFragment;
import com.splooze.utils.AppConstants;
import com.splooze.utils.Commonutils;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {
    public static ActivityHomeBinding binding;
    private Fragment fragment;
    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        context=HomeActivity.this;
        setOnClickListeners();

        if(getIntent().getStringExtra(AppConstants.NAVIGATION_FOR).equalsIgnoreCase("profile")){
            Bundle bundle=new Bundle();
            bundle.putString(AppConstants.NAVIGATE_FROM,"Home");
            fragment = new ProfileFragment();
            fragment.setArguments(bundle);
            Commonutils.setFragment(fragment, true, (FragmentActivity) context, R.id.flHome);
            manageBottomTab("profile");
        }else {
            Bundle bundle=new Bundle();
            bundle.putString(AppConstants.NAVIGATE_FROM,"home");
            bundle.putInt(AppConstants.SELECT_POSITION,0);
            fragment = new HomeFragment();
            fragment.setArguments(bundle);
            Commonutils.setFragment(fragment, true, (FragmentActivity) context, R.id.flHome);
        }

    }

    private void setOnClickListeners() {
        disableAutoFill();
        binding.bottomTab.llHome.setOnClickListener(this);
        binding.bottomTab.llExplore.setOnClickListener(this);
        binding.bottomTab.llAdd.setOnClickListener(this);
        binding.bottomTab.llNotification.setOnClickListener(this);
        binding.bottomTab.llProfile.setOnClickListener(this);
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void disableAutoFill() {
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O)
            getWindow().getDecorView().setImportantForAutofill(View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.llHome:
                //manageBottomTab("home");
                fragment = new HomeFragment();
                Commonutils.setFragment(fragment, true, (FragmentActivity) context, R.id.flHome);
                break;

            case R.id.llExplore:
                fragment = new ExploreSearchFragment();
                Commonutils.setFragment(fragment, true, (FragmentActivity) context, R.id.flHome);
                manageBottomTab("explore");
                break;

            case R.id.llAdd:
                fragment = new AddEventFragment();
                Commonutils.setFragment(fragment, true, (FragmentActivity) context, R.id.flHome);
                manageBottomTab("addEvent");
                break;

            case R.id.llNotification:
                fragment = new NotificationFragment();
                Commonutils.setFragment(fragment, true, (FragmentActivity) context, R.id.flHome);
                manageBottomTab("notification");
                break;

            case R.id.llProfile:
                Bundle bundle=new Bundle();
                bundle.putString(AppConstants.NAVIGATE_FROM,"Home");
                fragment = new ProfileFragment();
                fragment.setArguments(bundle);
                Commonutils.setFragment(fragment, true, (FragmentActivity) context, R.id.flHome);
                manageBottomTab("profile");
                break;
        }
    }

    public void manageBottomTab(String home) {
        if(home.equalsIgnoreCase("home")){
            binding.bottomTab.ivHome.setImageResource(R.mipmap.homeclick);
            binding.bottomTab.ivExplore.setImageResource(R.mipmap.explore);
            binding.bottomTab.ivAddEvent.setImageResource(R.mipmap.add);
            binding.bottomTab.ivNotification.setImageResource(R.mipmap.notification);
            binding.bottomTab.ivProfile.setImageResource(R.mipmap.profile);

            binding.bottomTab.tvHome.setTextColor(ContextCompat.getColor(context,R.color.colorRed));
            binding.bottomTab.tvExplore.setTextColor(ContextCompat.getColor(context,R.color.colorGrey));
            binding.bottomTab.tvAddEvent.setTextColor(ContextCompat.getColor(context,R.color.colorGrey));
            binding.bottomTab.tvNotification.setTextColor(ContextCompat.getColor(context,R.color.colorGrey));
            binding.bottomTab.tvProfile.setTextColor(ContextCompat.getColor(context,R.color.colorGrey));
        }

        else  if(home.equalsIgnoreCase("explore")){
            binding.bottomTab.ivHome.setImageResource(R.mipmap.home);
            binding.bottomTab.ivExplore.setImageResource(R.mipmap.exploreclick);
            binding.bottomTab.ivAddEvent.setImageResource(R.mipmap.add);
            binding.bottomTab.ivNotification.setImageResource(R.mipmap.notification);
            binding.bottomTab.ivProfile.setImageResource(R.mipmap.profile);

            binding.bottomTab.tvHome.setTextColor(ContextCompat.getColor(context,R.color.colorGrey));
            binding.bottomTab.tvExplore.setTextColor(ContextCompat.getColor(context,R.color.colorRed));
            binding.bottomTab.tvAddEvent.setTextColor(ContextCompat.getColor(context,R.color.colorGrey));
            binding.bottomTab.tvNotification.setTextColor(ContextCompat.getColor(context,R.color.colorGrey));
            binding.bottomTab.tvProfile.setTextColor(ContextCompat.getColor(context,R.color.colorGrey));
        }

        else  if(home.equalsIgnoreCase("addEvent")){
            binding.bottomTab.ivHome.setImageResource(R.mipmap.home);
            binding.bottomTab.ivExplore.setImageResource(R.mipmap.explore);
            binding.bottomTab.ivAddEvent.setImageResource(R.mipmap.addclick);
            binding.bottomTab.ivNotification.setImageResource(R.mipmap.notification);
            binding.bottomTab.ivProfile.setImageResource(R.mipmap.profile);

            binding.bottomTab.tvHome.setTextColor(ContextCompat.getColor(context,R.color.colorGrey));
            binding.bottomTab.tvExplore.setTextColor(ContextCompat.getColor(context,R.color.colorGrey));
            binding.bottomTab.tvAddEvent.setTextColor(ContextCompat.getColor(context,R.color.colorRed));
            binding.bottomTab.tvNotification.setTextColor(ContextCompat.getColor(context,R.color.colorGrey));
            binding.bottomTab.tvProfile.setTextColor(ContextCompat.getColor(context,R.color.colorGrey));
        }

        else  if(home.equalsIgnoreCase("notification")){
            binding.bottomTab.ivHome.setImageResource(R.mipmap.home);
            binding.bottomTab.ivExplore.setImageResource(R.mipmap.explore);
            binding.bottomTab.ivAddEvent.setImageResource(R.mipmap.add);
            binding.bottomTab.ivNotification.setImageResource(R.mipmap.notificationclick);
            binding.bottomTab.ivProfile.setImageResource(R.mipmap.profile);

            binding.bottomTab.tvHome.setTextColor(ContextCompat.getColor(context,R.color.colorGrey));
            binding.bottomTab.tvExplore.setTextColor(ContextCompat.getColor(context,R.color.colorGrey));
            binding.bottomTab.tvAddEvent.setTextColor(ContextCompat.getColor(context,R.color.colorGrey));
            binding.bottomTab.tvNotification.setTextColor(ContextCompat.getColor(context,R.color.colorRed));
            binding.bottomTab.tvProfile.setTextColor(ContextCompat.getColor(context,R.color.colorGrey));
        }

        else  if(home.equalsIgnoreCase("profile")){
            binding.bottomTab.ivHome.setImageResource(R.mipmap.home);
            binding.bottomTab.ivExplore.setImageResource(R.mipmap.explore);
            binding.bottomTab.ivAddEvent.setImageResource(R.mipmap.add);
            binding.bottomTab.ivNotification.setImageResource(R.mipmap.notification);
            binding.bottomTab.ivProfile.setImageResource(R.mipmap.profileclick);

            binding.bottomTab.tvHome.setTextColor(ContextCompat.getColor(context,R.color.colorGrey));
            binding.bottomTab.tvExplore.setTextColor(ContextCompat.getColor(context,R.color.colorGrey));
            binding.bottomTab.tvAddEvent.setTextColor(ContextCompat.getColor(context,R.color.colorGrey));
            binding.bottomTab.tvNotification.setTextColor(ContextCompat.getColor(context,R.color.colorGrey));
            binding.bottomTab.tvProfile.setTextColor(ContextCompat.getColor(context,R.color.colorRed));
        }
    }


    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        if (getSupportFragmentManager().findFragmentById(R.id.flHome) != null) {
            Fragment f = getSupportFragmentManager().findFragmentById(R.id.flHome);
            if (f instanceof HomeFragment) {
                new android.app.AlertDialog.Builder(this).setTitle("Exit")
                        .setMessage("Do you want to exit from App?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                moveTaskToBack(true);
                            }
                        }).setNegativeButton("No", null).show();
            }
            else if(f instanceof EditProfileFragment){
                fm.popBackStack();
            }

            else if(f instanceof EventDetailsFragment){
                fm.popBackStack();
            }

            else if(f instanceof FollowersFragment){
                fm.popBackStack();
            }

            else if(f instanceof FollowingFragment){
                fm.popBackStack();
            }

            else if(f instanceof HistoryEventFragment){
                fm.popBackStack();
            }

            else if(f instanceof TicketListFragment){
                fm.popBackStack();
            }
            else if(f instanceof ExploreFragment){
                fm.popBackStack();
            }
            else if(f instanceof LikedFragment){
                fm.popBackStack();
            }
            else if(f instanceof  ProfileFragment){
                fm.popBackStack();
            }
            else if(f instanceof ParticipantsFragment){
                fm.popBackStack();
            }
            else if(f instanceof  EventDetailsFragment){
                fm.popBackStack();
                HomeActivity.binding.bottomTab.llRoot.setVisibility(View.VISIBLE);
            }

            else {
                manageBottomTab("home");
                Bundle bundle=new Bundle();
                bundle.putString(AppConstants.NAVIGATE_FROM,"home");
                bundle.putInt(AppConstants.SELECT_POSITION,0);
                fragment = new HomeFragment();
                fragment.setArguments(bundle);
                Commonutils.setFragment(fragment, true, (FragmentActivity) context, R.id.flHome);
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.flHome);
        fragment.onActivityResult(requestCode, resultCode, data);
    }
}
