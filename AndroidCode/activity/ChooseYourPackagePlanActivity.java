package com.splooze.activity;


import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;
import com.splooze.R;
import com.splooze.adapter.ChooseYourPackagePlanAdapter;
import com.splooze.databinding.ActivityChooseYourPackagePlanBinding;
import com.splooze.model.ChooseYourPackagePlanModel;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;
import java.util.ArrayList;


public class ChooseYourPackagePlanActivity extends AppCompatActivity  implements View.OnClickListener {
    private ActivityChooseYourPackagePlanBinding binding;
    private Context context;
    private ArrayList<ChooseYourPackagePlanModel> MyList=new ArrayList<>();
    private  String[] PlanMoney={"$ 30.12","$40.55","$ 55.30","$ 30.12","$40.55","$ 55.30","$ 30.12","$40.55","$ 55.30","$ 30.12","$40.55","$ 55.30"};
    private String[] PlanType={"Music Club","Let's Dance","Annual Party","Music Club","Let's Dance","Annual Party","Music Club","Let's Dance","Annual Party","Music Club","Let's Dance","Annual Party"};
    private Integer[] PlanDone={R.mipmap.donechoose,R.mipmap.donechoose,R.mipmap.donechoose,R.mipmap.donechoose,R.mipmap.donechoose,R.mipmap.donechoose,R.mipmap.donechoose,R.mipmap.donechoose,R.mipmap.donechoose,R.mipmap.donechoose,R.mipmap.donechoose,R.mipmap.donechoose};
    private String[] PlanDetail;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this, R.layout.activity_choose_your_package_plan);
        context= ChooseYourPackagePlanActivity.this;
        PlanDetail= new String[]{getString(R.string.lorely_ipsim), getString(R.string.lorely_ipsim),getString(R.string.lorely_ipsim), getString(R.string.lorely_ipsim), getString(R.string.lorely_ipsim), getString(R.string.lorely_ipsim), getString(R.string.lorely_ipsim),getString(R.string.lorely_ipsim),getString(R.string.lorely_ipsim), getString(R.string.lorely_ipsim),getString(R.string.lorely_ipsim),getString(R.string.lorely_ipsim)};
        setOnClickListeners();
        setSliderWithRecyclerView();
    }
    //***User click event perform*****///
    private void setOnClickListeners() {
        binding.tvNext.setOnClickListener(this);
        binding.tvTermCondition.setOnClickListener(this);
        binding.ivBack.setOnClickListener(this);
    }
    ///validation for check box/////
    private boolean checkDetail() {
        if (!binding.cbPlanAccept.isChecked()) {
            Toast.makeText(context, "Please Check I accept the condition", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
    ///set the recycler view with animation /////
    private void setSliderWithRecyclerView() {
        addDataToList();
        binding.dsAllEvent.setSlideOnFling(true);
        ChooseYourPackagePlanAdapter adapter=new ChooseYourPackagePlanAdapter(context,MyList);
        binding.dsAllEvent.setAdapter(adapter);
        binding.dsAllEvent.scrollToPosition(0);
        binding.dsAllEvent.setItemTransformer(new ScaleTransformer.Builder().setMinScale(0.99f).build());
    }
    ///Add data to list 
    private void addDataToList() {
        for(int m=0;m<PlanMoney.length;m++){
            ChooseYourPackagePlanModel model=new ChooseYourPackagePlanModel();
            model.setPlanDetail(PlanDetail[m]);
            model.setPlanMoney(PlanMoney[m]);
            model.setPlanType(PlanType[m]);
            model.setPlanDone(PlanDone[m]);
            MyList.add(model);
        }
    }
    ///****User click event perform by id*****///
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tvNext:
                if (checkDetail()) {
                    Toast.makeText(context, "Thank You", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.tvTermCondition:
                Intent intent=new Intent(context,TermConditionsActivity.class);
                startActivity(intent);
                break;
            case R.id.ivBack:
                onBackPressed();
                break;

        }
    }
}
