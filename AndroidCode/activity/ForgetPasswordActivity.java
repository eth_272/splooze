package com.splooze.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.splooze.R;
import com.splooze.databinding.ActivityForgetPasswordBinding;
import com.splooze.utils.AppConstants;
import com.splooze.utils.Commonutils;

public class ForgetPasswordActivity extends AppCompatActivity implements View.OnClickListener {
   private   ActivityForgetPasswordBinding binding;
   private Context context;
   public static Activity mActivity;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this, R.layout.activity_forget_password);
        context=ForgetPasswordActivity.this;
        mActivity=ForgetPasswordActivity.this;
        setOnClickListener();
    }

    //**************Implement Button Click Listener**************
    private void setOnClickListener() {
        disableAutoFill();
        binding.tvSendotp.setOnClickListener(this);
        binding.ivBackForget.setOnClickListener(this);
        binding.etNumber.addTextChangedListener(new ForgetPasswordActivity.MyEditor(binding.etNumber));
    }
    //**************Check device version code**************
    @TargetApi(Build.VERSION_CODES.O)
    private void disableAutoFill() {
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O)
            getWindow().getDecorView().setImportantForAutofill(View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv__sendotp:{
                if (checkValidation()) {
/*                    Intent intent=new Intent(context, Signup4Activity.class);
                    intent.putExtra(AppConstants.NAVIGATE_FROM,"ForgotPassword");
                    startActivity(intent);*/
                    Intent last = new Intent(context, Signup4Activity.class);
                    last.putExtra(AppConstants.NAVIGATE_FROM,"signup");
                    last.putExtra(AppConstants.USER_ID,"");
                    last.putExtra(AppConstants.OTP,"");
                    startActivity(last);

                }
                break;
            }

            case R.id.iv_back_forget:
            {
                onBackPressed();
                break;
            }
        }
    }
    //************--Check validation from Client Side********
    private boolean checkValidation() {
        if(binding.etNumber.getText().toString().equalsIgnoreCase("")){
            binding.inputLayoutNumber.setErrorEnabled(true);
            binding.inputLayoutNumber.setError("Please enter email /contact number.");
            binding.etNumber.requestFocus();
            return false;
        }
        else if(isEmail(binding.etNumber.getText().toString().trim())){
            if(!Commonutils.isValidEmail(binding.etNumber.getText().toString().trim())){
                binding.inputLayoutNumber.setErrorEnabled(true);
                binding.inputLayoutNumber.setError("Please enter valid email.");
                binding.etNumber.requestFocus();
                return false;
            }
        }else {
            if(binding.etNumber.getText().toString().trim().length()<8){
                binding.inputLayoutNumber.setErrorEnabled(true);
                binding.inputLayoutNumber.setError("Please enter valid contact number.");
                binding.etNumber.requestFocus();
                return false;
            }
        }

        return true;
    }

    //*****************  Confirm that input string is Mail Or Phone ****************

    public static boolean isEmail(String s) {
        boolean atleastOneAlpha = s.matches(".*[a-zA-Z-@]+.*");
        return atleastOneAlpha;
    }


    //****************  Text Watcher Class Working On EditText Field **********************
    private class MyEditor implements TextWatcher{
        private View view;
        private MyEditor(View view) {
            this.view = view;
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
        @Override
        public void afterTextChanged(Editable s) {

            switch (view.getId()){
                case R.id.et_number:
                    if (!binding.etNumber.getText().toString().trim().isEmpty()) {
                        binding.inputLayoutNumber.setErrorEnabled(false);
                    }
            }
        }
    }

}
