package com.splooze.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.splooze.R;
import com.splooze.adapter.BeverageAdapter;
import com.splooze.adapter.SheetCountAdapter;
import com.splooze.databinding.FragmentEventDetailsBinding;
import com.splooze.interfaces.RecyclerItemListener;
import com.splooze.model.SheetCountModel;

import java.util.ArrayList;
public class EventDetailsActivity extends AppCompatActivity implements View.OnClickListener , RecyclerItemListener {

    private FragmentEventDetailsBinding binding;
    private Context context;
    private ArrayList<SheetCountModel> sheetCountList=new ArrayList<>();
    private Integer [] noOfSheet=new Integer[]{1,2,3,4,5,6,7,8,9,10};
    private LinearLayoutManager mLayoutManager;
    private LinearLayoutManager mLayoutManager1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=DataBindingUtil.setContentView(this,R.layout.fragment_event_details);
        context= EventDetailsActivity.this;
        setOnClickListiner();
    }

    private void setOnClickListiner() {
        binding.tvBookNow.setText("Interested");
        binding.tvBookNow.setOnClickListener(this);
        binding.tvNotInterested.setOnClickListener(this);
        binding.ivBack.setOnClickListener(this);
        binding.flParticipant.setOnClickListener(this);
        mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        mLayoutManager1 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        addDataToSheetCountList();
    }

    private void addDataToSheetCountList() {
        sheetCountList.clear();
        for (int i = 0; i <noOfSheet.length ; i++) {
            SheetCountModel model=new SheetCountModel();
            if(i==0){
                model.isSelected=true;
            }else {
                model.isSelected=false;
            }
            model.countNo=noOfSheet[i];
            sheetCountList.add(model);
        }
        setRecyclerForSheetBooking();
        setRecyclerForBeverage();
    }


    private void setRecyclerForSheetBooking() {
        binding.rvTableCount.setLayoutManager(mLayoutManager);
        SheetCountAdapter adapter=new SheetCountAdapter(context,this,sheetCountList);
        binding.rvTableCount.setAdapter(adapter);
    }

    private void setRecyclerForBeverage() {
        binding.rvBeverages.setLayoutManager(mLayoutManager1);
        BeverageAdapter adapter=new BeverageAdapter(context,this);
        binding.rvBeverages.setAdapter(adapter);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.tvBookNow:
                if(binding.tvBookNow.getText().toString().trim().equalsIgnoreCase("Interested")){
                    binding.tvBookNow.setText("Book Now");
                }else {
                    Toast.makeText(context, R.string.work_in_progress, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.ivBack:
                onBackPressed();
                break;

            case R.id.tvNotInterested:
                onBackPressed();
                break;

            case R.id.flParticipant:
                Intent intent=new Intent(context,ParticipantActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onClickOfOk(int position) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Animatoo.animateSlideDown(context);
    }

}
