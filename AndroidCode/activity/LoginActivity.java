package com.splooze.activity;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.splooze.R;
import com.splooze.databinding.ActivityLoginBinding;
import com.splooze.utils.AppConstants;
import com.splooze.utils.Commonutils;
import com.splooze.web_services_java.RetrofitExecuter;
import com.splooze.web_services_java.response_model.LoginResponse;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import org.json.JSONObject;

import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Response;

public  class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "";
    private ActivityLoginBinding binding;
    public static final String PREFS_NAME = "MyPrefsFile";
    private Context context;
    private CallbackManager callbackManager;
    private static final String EMAIL = "email";
    private String social_id, socialImageUrl, socialFirstName, socialLastName, socialEmail, socialType;
    private AccessToken accessToken;
    private TwitterAuthClient client;
    public static Activity mActivity;
    String userName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        context = LoginActivity.this;
        mActivity = LoginActivity.this;
        setOnClickListeners();
        Twitter.initialize(this);
        //setOnTwitter();
    }
    //******************  Implement Click Listeners on Buttons *******************
    private void setOnClickListeners() {
        disableAutoFill();
        binding.txtForget.setOnClickListener(this);
        binding.tvLoginSubmit.setOnClickListener(this);
        binding.txtSign.setOnClickListener(this);
        binding.civTwitter.setOnClickListener(this);
        binding.civInstagram.setOnClickListener(this);
        binding.civFacebook.setOnClickListener(this);

        // binding.backLogin.setOnClickListener(this);
        binding.etmail.addTextChangedListener(new LoginActivity.MyEditor(binding.etmail));
        binding.etpasslogin.addTextChangedListener(new LoginActivity.MyEditor(binding.etpasslogin));

        getUserRememberMeData();
        binding.remember.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    saveRememberMeUserData(true);
                } else {
                    saveRememberMeUserData(false);
                }
            }
        });

        binding.etmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus && binding.etmail.getText().toString().trim().length() == 0) {
                    binding.etmail.requestFocus();
                }
            }
        });

    }

    @TargetApi(Build.VERSION_CODES.O)
    private void disableAutoFill() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            getWindow().getDecorView().setImportantForAutofill(View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS);
    }

    private void getUserRememberMeData() {
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        if (prefs.getBoolean("check", true)) {
            if (prefs.getString("email", null) != null) {
                binding.etmail.setText(prefs.getString("email", null));
                binding.etmail.setSelection(binding.etmail.getText().length());  //Shift the cursor after text
            }
            if (prefs.getString("pass", null) != null) {
                binding.etpasslogin.setText(prefs.getString("pass", null));
                binding.remember.setChecked(true);
            }

        } else {
            binding.remember.setChecked(false);
        }

    }

    private void saveRememberMeUserData(boolean isClicked) {
        SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString("email", binding.etmail.getText().toString());
        // email.setSelection(0);
        editor.putString("pass", binding.etpasslogin.getText().toString());
        editor.putBoolean("check", isClicked);
        editor.apply();
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.tv_login_submit: {
                if (checkValidation()) {

                    if(Commonutils.isOnline(context)){
                        callLoginApi();
                    }else {
                        Toast.makeText(context, R.string.ple_check_internet, Toast.LENGTH_SHORT).show();
                    }

                }

                break;
            }

            case R.id.civ_twitter: {
                //setOnTwitter();
                Toast.makeText(LoginActivity.this, R.string.work_in_progress, Toast.LENGTH_SHORT).show();
                break;
            }

            case R.id.civ_facebook: {
                //onFacebookLoginClick();
                Toast.makeText(LoginActivity.this, R.string.work_in_progress, Toast.LENGTH_SHORT).show();
                break;
            }

            case R.id.civ_instagram: {
                Toast.makeText(LoginActivity.this, R.string.work_in_progress, Toast.LENGTH_SHORT).show();
                break;

            }
            case R.id.txt_sign: {
                Intent userScreen = new Intent(this, UserSelectionActivity.class);
                startActivity(userScreen);
                break;
            }
            case R.id.txt_forget: {
                Intent forget = new Intent(this, ForgetPasswordActivity.class);
                startActivity(forget);
                break;
            }
        }
    }
      /* *//*******Open twitter page for login and get email/***** ////***    */      
    public void setOnTwitter() {
        client = new TwitterAuthClient();
        client.authorize((LoginActivity.this), new Callback<TwitterSession>() {
            //twitterLoginButton.setCallback(new Callback<TwitterSession>()
            @Override
            public void success(Result<TwitterSession> result) {
                // Do something with result, which provides a TwitterSession for making API calls  
                TwitterSession session = TwitterCore.getInstance().getSessionManager().getActiveSession();
                TwitterAuthToken authToken = session.getAuthToken();
                String twitterName = session.getUserName();

                Log.i("Class: ", "Twitter token and secret: " + authToken + "\t" + twitterName);
                Log.i("Class: ", "Twitter name: " + twitterName);
                String token = authToken.token;
                String secret = authToken.secret;
                //loginMethod(session);
                fetchTwitterEmail(session);
            }

            /*private void loginMethod(TwitterSession session) {
                String userName = session.getUserName();
                Toast.makeText(context, "user Name" + userName,
                        Toast.LENGTH_LONG).show();
            }*/


            @Override
            public void failure(TwitterException exception) {
                // Do something on failure  
                Toast.makeText(getApplicationContext(), "Login fail", Toast.LENGTH_LONG).show();
            }
        });
    }
    //*/********Fetch twitter Email*//////////
    public void fetchTwitterEmail(final TwitterSession twitterSession) {
        TwitterAuthClient authClient = new TwitterAuthClient();
        authClient.requestEmail(twitterSession, new Callback<String>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void success(Result<String> result) {
                //here it will give u only email and rest of other information u can get from TwitterSession
                Toast.makeText(context, "Email Id : " + result.data, Toast.LENGTH_SHORT).show();
                binding.etmail.setText( "" + result.data);
            }

            @Override
            public void failure(TwitterException exception) {
                Toast.makeText(context, "Failed to authenticate. Please try again.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /******facebook login here*****///////
    private void onFacebookLoginClick() {
        callbackManager = CallbackManager.Factory.create();
        callFacebookButton();
    }

    private void callFacebookButton() {
        LoginManager.getInstance().logOut();
        LoginManager.getInstance().logInWithReadPermissions((Activity) context, Arrays.asList("public_profile", "email"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                accessToken = loginResult.getAccessToken();
                if (accessToken != null) {
                    GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {
                            if (response.getJSONObject() != null) {
                                social_id = response.getJSONObject().optString("id");
                                Toast.makeText(context, "Email:"+social_id, Toast.LENGTH_SHORT).show();
                                socialImageUrl = "https://graph.facebook.com/" + social_id + "/picture?type=large";

                                socialFirstName = response.getJSONObject().optString("first_name");
                                socialLastName = response.getJSONObject().optString("last_name");
//                                String user_name = response.getJSONObject().optString("name");
//                                String genderFB = response.getJSONObject().optString("gender");
                                socialEmail = response.getJSONObject().optString("email");
                                Toast.makeText(context, "Email:"+socialEmail, Toast.LENGTH_SHORT).show();
                                binding.etmail.setText(""+socialEmail);
                                socialType = "facebook";

                                String name = "" + socialFirstName + " " + socialLastName;

                                if (Commonutils.isOnline(context)) {
                                    if (social_id.length() == 0) {
                                        //ToastUtils.showToastLong(activity, activity.getString(R.string.network_error));
                                    } else {
                                        if (social_id != null && socialEmail != null) {
                                           // callLoginApi("facebook", social_id);
                                        } else {
                                            // ToastUtils.showToastLong(activity, activity.getString(R.string.network_error));
                                        }
                                    }
                                }
                            }
                        }
                    });

                    Bundle parameters = new Bundle();
                    parameters.putString("fields", "id,first_name,last_name,name,gender,email,picture,friends");
                    request.setParameters(parameters);
                    request.executeAsync();
                }
            }
            @Override
            public void onCancel() {
//                ToastUtils.showToastShort(getApplication(), "Cancelled");
            }

            @Override
            public void onError(FacebookException e) {
                // ToastUtils.showToastShort(activity, activity.getString(R.string.error));
                e.printStackTrace();
            }
        });
    }

    private void loginMethod(TwitterSession session) {
        String userName=session.getUserName();
        Toast.makeText(context, "user Name"+userName,
                Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(callbackManager!=null){
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        else if(client!=null){
            client.onActivityResult(requestCode, resultCode, data);
        }

    }
    //************************  Check Validation method at client side *********************
    private boolean checkValidation() {

        if (binding.etmail.getText().toString().equalsIgnoreCase((""))) {
            binding.emailLayout.setErrorEnabled(true);
            binding.emailLayout.setError(" Please enter email/contact number.");
            binding.etmail.requestFocus();
            return false;

        }
        else if (isEmail(binding.etmail.getText().toString().trim())) {
            if (!Commonutils.isValidEmail(binding.etmail.getText().toString().trim())) {
                binding.emailLayout.setErrorEnabled(true);
                binding.emailLayout.setError(" Please enter valid email id.");
                binding.etmail.requestFocus();
                return false;
            }
            else if (binding.etpasslogin.getText().toString().trim().isEmpty()) {
                binding.passLayout.setErrorEnabled(true);
                binding.passLayout.setError(getString(R.string.err_pass_empty));
                binding.etpasslogin.requestFocus();
                return false;
            } else if (binding.etpasslogin.getText().toString().trim().length() < 6) {
                binding.passLayout.setErrorEnabled(true);
                binding.passLayout.setError(getString(R.string.err_msg_pass_value_not_match));
                binding.etpasslogin.requestFocus();
                return false;
            }
        }else {
            if (binding.etmail.getText().toString().trim().length() < 8) {
                binding.emailLayout.setErrorEnabled(true);
                binding.emailLayout.setError(" Please enter valid contact number.");
                binding.etmail.requestFocus();
                return false;
            }
            else if (binding.etpasslogin.getText().toString().trim().isEmpty()) {
                binding.passLayout.setErrorEnabled(true);
                binding.passLayout.setError(getString(R.string.err_pass_empty));
                binding.etpasslogin.requestFocus();
                return false;
            } else if (binding.etpasslogin.getText().toString().trim().length() < 6) {
                binding.passLayout.setErrorEnabled(true);
                binding.passLayout.setError(getString(R.string.err_msg_pass_value_not_match));
                binding.etpasslogin.requestFocus();
                return false;
            }
        }
        return true;
    }

    public static boolean isEmail(String s) {
        boolean atleastOneAlpha = s.matches(".*[a-zA-Z-@]+.*");
        return atleastOneAlpha;
    }

    ///*************** Custom Class For Check TextWatcher For EditText ********************
    private class MyEditor implements TextWatcher {
        private View view;
        private MyEditor(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            switch (view.getId()) {
                case R.id.etmail:
                    if (!binding.etmail.getText().toString().trim().isEmpty()) {
                        binding.emailLayout.setErrorEnabled(false);
                    }
                case R.id.etpasslogin:
                    if (!binding.etpasslogin.getText().toString().trim().isEmpty()) {
                        binding.passLayout.setErrorEnabled(false);
                    }
            }
        }
    }


//********************************  Call Login Api *****************************************************
    private void callLoginApi() {
        Commonutils.showProgressDialog(context);
        final Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        Call<LoginResponse> call = null;
        call = RetrofitExecuter.getApiInterface("userProfile","","").
                callLoginApi(
                        binding.etmail.getText().toString().trim(),
                        binding.etpasslogin.getText().toString().trim());
        Log.e("url", "" + call.request().url());
        call.enqueue(new retrofit2.Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                Commonutils.disMissProgressDialog(context);
                if (response.body() != null && response.code()==200) {
                    Log.e("Get response", "" + new Gson().toJson(response.body()).toString());
                    if(response.body().getUser().getIsActive()){
                        Intent intent=new Intent(context,HomeActivity.class);
                        intent.putExtra(AppConstants.NAVIGATE_FROM,"login");
                        intent.putExtra(AppConstants.NAVIGATION_FOR,"explore");
                        startActivity(intent);
                    }else {
                        Intent last = new Intent(context, Signup4Activity.class);
                        last.putExtra(AppConstants.NAVIGATE_FROM,"Login");
                        last.putExtra(AppConstants.USER_ID,""+response.body().getUser().getId());
                        last.putExtra(AppConstants.OTP,""+response.body().getUser().otp_token);
                        startActivity(last);

                        Toast.makeText(LoginActivity.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(LoginActivity.this, "No active account found with this given credentials.", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Commonutils.disMissProgressDialog(context);
                call.cancel();
                Toast.makeText(context, "Server down.", Toast.LENGTH_SHORT).show();
            }
        });
    }



}


