package com.splooze.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.splooze.R;
import com.splooze.databinding.ActivitySignup2Binding;
import com.splooze.utils.AppConstants;
import com.splooze.utils.Commonutils;
import com.splooze.web_services_java.RetrofitExecuter;
import com.splooze.web_services_java.response_model.OtpVerification;

import retrofit2.Call;
import retrofit2.Response;

public  class Signup4Activity extends AppCompatActivity implements View.OnClickListener {
    private ActivitySignup2Binding binding;
    private Context context;
    public  static Activity mActivity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this, R.layout.activity_signup2);
        context= Signup4Activity.this;
        mActivity= Signup4Activity.this;
        setOnClickListeners();
    }
    //******************  Implement Click Listeners on Buttons *******************
    private void setOnClickListeners() {
        disableAutoFill();
        binding.tvVerify.setOnClickListener(this);
        binding.tvClick.setOnClickListener(this);
        binding.ivBackVerify.setOnClickListener(this);
        if(getIntent().getStringExtra(AppConstants.NAVIGATE_FROM).equalsIgnoreCase("ForgotPassword")){
            binding.tvHeader.setText("OTP VERIFICATION");
            binding.tvVerifyNumber.setVisibility(View.INVISIBLE);
            binding.llSelectedIndication.setVisibility(View.INVISIBLE);
        }
        else  if(getIntent().getStringExtra(AppConstants.NAVIGATE_FROM).equalsIgnoreCase("Login")){
            binding.tvHeader.setText("OTP VERIFICATION");
            binding.tvVerifyNumber.setVisibility(View.INVISIBLE);
            binding.llSelectedIndication.setVisibility(View.INVISIBLE);
        }
        else{
            binding.tvHeader.setText(R.string.txtview_s);
            binding.tvVerifyNumber.setVisibility(View.VISIBLE);
            binding.llSelectedIndication.setVisibility(View.VISIBLE);
        }

        binding.etInputotp.setText(getIntent().getStringExtra(AppConstants.OTP));

    }

    @TargetApi(Build.VERSION_CODES.O)
    private void disableAutoFill() {
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O)
            getWindow().getDecorView().setImportantForAutofill(View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS);
    }

    //******************  Check Validation in OTP  from Client Side*******************
    private boolean validateOtp() {
        if (binding.etInputotp.getText().toString().trim().length() < 6) {
            binding.inputLayoutOtp.setErrorEnabled(true);
            binding.inputLayoutOtp.setError(getString(R.string.err_msg_otp));
            binding.etInputotp.requestFocus();
            return false;
        } else {
            binding.inputLayoutOtp.setErrorEnabled(false);
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back_verify: {
                onBackPressed();
                break;
            }
            case R.id.tv_click:
                if (Commonutils.isOnline(context)){
                    callResendOtpApi();
                }else {
                    Toast.makeText(context, R.string.ple_check_internet, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.tv_verify: {
                if(validateOtp()){
                    if (Commonutils.isOnline(context)){
                        callVerifyOtpApi();
                    }else {
                        Toast.makeText(context, R.string.ple_check_internet, Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            }
        }
    }



    //**************************************  Call Verify OTP Api *****************************************************
    private void callVerifyOtpApi() {
        Commonutils.showProgressDialog(context);
        final Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        Call<OtpVerification> call = null;
        call = RetrofitExecuter.getApiInterface("userProfile","","").
                callOtpVerificationApi(
                        getIntent().getStringExtra(AppConstants.USER_ID),
                        binding.etInputotp.getText().toString().trim());
        Log.e("url", "" + call.request().url());
        call.enqueue(new retrofit2.Callback<OtpVerification>() {
            @Override
            public void onResponse(Call<OtpVerification> call, Response<OtpVerification> response) {
                Commonutils.disMissProgressDialog(context);
                if (response.body() != null && response.code()==201) {
                    Log.e("Get response", "" + new Gson().toJson(response.body()).toString());
                    if(response.body().getUser().getIsActive()){

                        if(getIntent().getStringExtra(AppConstants.NAVIGATE_FROM).equalsIgnoreCase("ForgotPassword")){
                            Intent i = new Intent(context, ResetPasswordActivity.class);
                            startActivity(i);
                        }else {
                            Intent last = new Intent(context, WelcomeSploozeActivity.class);
                            startActivity(last);
                        }

                    }else {
                        Toast.makeText(context, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(context,  "OTP not verified", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<OtpVerification> call, Throwable t) {
                Commonutils.disMissProgressDialog(context);
                call.cancel();
                Toast.makeText(context, "Server down.", Toast.LENGTH_SHORT).show();
            }
        });
    }


    //**************************************  Call Verify OTP Api *****************************************************
    private void callResendOtpApi() {
        Commonutils.showProgressDialog(context);
        final Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        Call<OtpVerification> call = null;
        call = RetrofitExecuter.getApiInterface("userProfile","","").
                callResendOtpApi(
                        getIntent().getStringExtra(AppConstants.USER_ID));
        Log.e("url", "" + call.request().url());
        call.enqueue(new retrofit2.Callback<OtpVerification>() {
            @Override
            public void onResponse(Call<OtpVerification> call, Response<OtpVerification> response) {
                Commonutils.disMissProgressDialog(context);
                if (response.body() != null && response.code()==200) {
                    Log.e("Get response", "" + new Gson().toJson(response.body()).toString());

                    Toast.makeText(Signup4Activity.this, ""+response.body().getMessage().toString(), Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(context,  "OTP not verified", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<OtpVerification> call, Throwable t) {
                Commonutils.disMissProgressDialog(context);
                call.cancel();
                Toast.makeText(context, "Server down.", Toast.LENGTH_SHORT).show();
            }
        });
    }


}


