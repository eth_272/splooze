package com.splooze.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import com.splooze.R;
import com.splooze.databinding.ActivityUserSelectionBinding;
import com.splooze.utils.AppConstants;


public class UserSelectionActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityUserSelectionBinding userbinding;
    public static Activity mActivity;
    private Context context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userbinding= DataBindingUtil.setContentView(this, R.layout.activity_user_selection);
        context=UserSelectionActivity.this;
        mActivity=UserSelectionActivity.this;
        setOnClickListener();
    }

    private void setOnClickListener() {

        userbinding.ivVibe.setOnClickListener(this);
        userbinding.ivClub.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_vibe:
            {
                Intent intent=new Intent(context,Signup1Activity.class);
                startActivity(intent);
                break;
            }
            case R.id.iv_club:
            {
                Intent club=new Intent(context,ClubSignup1Activity.class);
                startActivity(club);
                break;
            }
        }
    }
}
