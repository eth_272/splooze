package com.splooze.activity;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.splooze.R;
import com.splooze.databinding.ActivityTermsConditionsBinding;

public class TermConditionsActivity extends AppCompatActivity {
    private ActivityTermsConditionsBinding binding;
    private Context context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_terms_conditions);
        context = TermConditionsActivity.this;
        setOnClickListeners();
    }
    private void setOnClickListeners() {
        //click on left button of toolbar
        binding.toolbar.ivToolbarLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        binding.toolbar.tvToolbarTitle.setText(getString(R.string.term));
    }
}
