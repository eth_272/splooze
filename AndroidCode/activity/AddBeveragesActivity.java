package com.splooze.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.splooze.R;
import com.splooze.adapter.AddBeverageAdapter;
import com.splooze.databinding.ActivityAddBeveragesBinding;
import com.splooze.image_manager.SelectPictureUtils;
import com.splooze.image_manager.TakePictureUtils;
import com.splooze.interfaces.BeveragesItemListener;
import com.splooze.model.BeveragesModel;
import com.splooze.utils.AppConstants;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static com.splooze.image_manager.TakePictureUtils.TAKE_PICTURE;


public class AddBeveragesActivity extends AppCompatActivity implements View.OnClickListener, BeveragesItemListener {
    private ActivityAddBeveragesBinding binding;
    private Context context;
    private LinearLayoutManager mLayoutManager;
    private ArrayList<BeveragesModel> myList=new ArrayList<>();
    private String [] bevName={"Soft Drink","Burger"};
    private String [] bevPrice={"13","56"};
    private static final int REQUEST_CODE_ASK_PERMISSIONS=2;
    private SelectPictureUtils pictureUtils;
    private Bitmap bitmap=null;
    private String filepath="";
    private String path = null;
    private String clickFrom="";
    private  int  selectedPosition;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this, R.layout.activity_add_beverages);
        context= AddBeveragesActivity.this;
        pictureUtils=new SelectPictureUtils(context);
        setOnClickListener();
        setRecyclerView();
    }

    private void setOnClickListener() {
        binding.toolbar.ivToolbarLeft.setOnClickListener(this);
        binding.tvAddMore.setOnClickListener(this);
        binding.ivCamera.setOnClickListener(this);
        binding.flBeverage.setOnClickListener(this);
        binding.toolbar.ivToolbarRight.setOnClickListener(this);
        binding.toolbar.tvToolbarTitle.setText("BEVERAGES");
        binding.toolbar.ivToolbarRight.setVisibility(View.VISIBLE);
        binding.toolbar.ivToolbarRight.setImageResource(R.drawable.right);
    }

    //***
    private void setRecyclerView() {
        addDataToList();
        mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        binding.rvBeverages.setLayoutManager(mLayoutManager);
        AddBeverageAdapter adapter=new AddBeverageAdapter(context,this,myList);
        binding.rvBeverages.setAdapter(adapter);
    }

    private void addDataToList() {
        myList.clear();
        for (int i = 0; i <bevName.length ; i++) {
            BeveragesModel model=new BeveragesModel();
            model.beverageName=bevName[i];
            model.beveragePrice=bevPrice[i];
            model.beverageImage="";
            model.isSelected=false;
            myList.add(model);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ivToolbarLeft:
                finish();
                break;

            case R.id.ivCamera:
                clickFrom="";
                marshmallow();
                break;

            case R.id.flBeverage:
                clickFrom="";
                marshmallow();
                break;

            case R.id.ivToolbarRight:
                Toast.makeText(context, "Beverages Add Successfully!", Toast.LENGTH_SHORT).show();
                finish();
                break;

            case R.id.tvAddMore:
                if(checkValidation()){
                    BeveragesModel model=new BeveragesModel();
                    model.beveragePrice=binding.etBeveragePrice.getText().toString().trim();
                    model.beverageName=binding.etBeverageName.getText().toString().trim();
                    model.beverageImage=path;
                    model.isSelected=false;
                    myList.add(model);
                    //  Collections.reverse(myList);
                    binding.rvBeverages.getAdapter().notifyDataSetChanged();
                    binding.etBeverageName.setText("");
                    binding.etBeveragePrice.setText("");
                    bitmap=null;
                    binding.ivBeverage.setImageResource(0);
                    binding.ivCamera.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    private boolean checkValidation() {
        if(binding.etBeverageName.getText().toString().trim().equalsIgnoreCase("")){
            // binding.tilBeverageName.setErrorEnabled(true);
            // binding.tilBeverageName.setError("*Please enter name.");
            binding.etBeverageName.requestFocus();
            Toast.makeText(context, "*Please enter name.", Toast.LENGTH_LONG).show();
            return false;
        }else if(binding.etBeveragePrice.getText().toString().trim().equalsIgnoreCase("")){
            //binding.tilBeveragePrice.setErrorEnabled(true);
            // binding.tilBeveragePrice.setError("*Please enter price.");
            Toast.makeText(context, "*Please enter price.", Toast.LENGTH_LONG).show();
            binding.tilBeveragePrice.requestFocus();
            return false;
        }
        else if(bitmap==null){
            Toast.makeText(context, "*Please  select  beverage Image.", Toast.LENGTH_LONG).show();
            binding.tilBeveragePrice.requestFocus();
            return false;
        }

        return true;
    }


    private void marshmallow() {
        if (Build.VERSION.SDK_INT < 23) {
            addPhotoDialog();
        } else {
            int hasPermissionCounter = 0;
            String[] permission = {Manifest.permission.CAMERA, READ_EXTERNAL_STORAGE};
            for (String aPermission : permission) {
                if (context.checkSelfPermission(aPermission) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{aPermission}, REQUEST_CODE_ASK_PERMISSIONS);
                } else {
                    hasPermissionCounter++;
                }
            }
            if (hasPermissionCounter == 2) {
                addPhotoDialog();
            }
        }
    }


    /*create a cutom dialog view which is perform action onclient click*/
    private void addPhotoDialog() {
        LayoutInflater inflater = LayoutInflater.from(context);
        final Dialog dialog=new Dialog(context,android.R.style.Theme_Translucent_NoTitleBar);
        if (dialog.getWindow() != null)
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow();
        // custom dialog
        View dialogLayout = inflater.inflate(R.layout.popup_profilepic, null);
        dialog.setContentView(dialogLayout);
        dialog.show();

        TextView tvCamera = (TextView) dialog.findViewById(R.id.tvCamera);
        TextView tvGallery = (TextView) dialog.findViewById(R.id.tvGallery);
        TextView tvCancel = (TextView) dialog.findViewById(R.id.tvCancel);

        tvCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                pictureUtils.takePictureFromCamera();
            }
        });
        tvGallery.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                pictureUtils.openGallery();
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TakePictureUtils.PICK_GALLERY) {
            if (resultCode == RESULT_OK) {
                try {
                    InputStream inputStream = context.getContentResolver().openInputStream(data.getData());
                    FileOutputStream fileOutputStream = new FileOutputStream(new File(context.getExternalFilesDir(AppConstants.TEMP), pictureUtils.galleryImagePath + AppConstants.JPG_EXT));
                    TakePictureUtils.copyStream(inputStream, fileOutputStream);
                    fileOutputStream.close();
                    if (inputStream != null) {
                        inputStream.close();
                    }
                } catch (Exception ee) {
                    ee.printStackTrace();
                }
                if(data.getData()!=null){
                    try {
                        CropImage.activity(data.getData()).setGuidelines(CropImageView.Guidelines.ON).start((Activity) context);
                    } catch (Exception e) {
                    }
                }

            }
        }

        else if (requestCode == TAKE_PICTURE) {
            if(pictureUtils.getImagePath()!=null){
                Uri uri = Uri.fromFile(new File(pictureUtils.getImagePath()));
                CropImage.activity(uri).setGuidelines(CropImageView.Guidelines.ON).start((Activity) context);
            }
        }

        else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), result.getUri());
                    if (data != null) {
                        path = String.valueOf(result.getUri());
                        if (path == null) {
                            return;
                        }

                        if(clickFrom.equalsIgnoreCase("edit")){
                            if(myList.get(selectedPosition).isSelected){
                                BeveragesModel model=new BeveragesModel();
                                model.isSelected=true;
                                model.beverageName=myList.get(selectedPosition).beverageName;
                                model.beveragePrice=myList.get(selectedPosition).beveragePrice;
                                model.beverageImage=path;
                                myList.set(selectedPosition,model);
                                binding.rvBeverages.getAdapter().notifyItemChanged(selectedPosition);
                            }
                        }
                        else {
                            if(bitmap!=null){
                                binding.ivCamera.setVisibility(View.GONE);
                                binding.ivBeverage.setImageBitmap(bitmap);
                            }
                        }

                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }


        }
    }


    @Override
    public void onClickOfOk(int position,String performOperation) {
        if (performOperation.equalsIgnoreCase("edit")) {
            clickFrom="edit";
            selectedPosition=position;
            marshmallow();
        }
    }


}
