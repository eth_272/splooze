package com.splooze.activity;

import android.annotation.TargetApi;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Toast;
import com.splooze.R;
import com.splooze.databinding.ActivityResetPasswordBinding;

public class ResetPasswordActivity extends AppCompatActivity implements View.OnClickListener
{
    private ActivityResetPasswordBinding changebinding;
    private Context context;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        changebinding= DataBindingUtil.setContentView(this,R.layout.activity_reset_password);
        context=ResetPasswordActivity.this;
        setOnClickListeners();
    }

    private void setOnClickListeners() {
        disableAutoFill();
        changebinding.etpasswordReset.addTextChangedListener(new ResetPasswordActivity.MyEditor(changebinding.etpasswordReset));
        changebinding.passconfirm.addTextChangedListener(new ResetPasswordActivity.MyEditor(changebinding.passconfirm));
        changebinding.tvResetsubmit.setOnClickListener(this);
        changebinding.ivBack.setOnClickListener(this);
    }
    //**************Check device version code**************
    @TargetApi(Build.VERSION_CODES.O)
    private void disableAutoFill() {
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O)
            getWindow().getDecorView().setImportantForAutofill(View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_resetsubmit:
            {
                if (checkValidation()) {
                    Toast.makeText(context, "Password reset successfully.", Toast.LENGTH_SHORT).show();
                    finish();
                    ForgetPasswordActivity.mActivity.finish();
                    Signup4Activity.mActivity.finish();
                }
                break;
            }

            case R.id.ivBack:
                finish();
                break;

        }
    }
    //****************  Check Validation AT Client End **************************

    private boolean checkValidation() {
        if(changebinding.etpasswordReset.getText().toString().isEmpty()){
            changebinding.layoutPassreset.setErrorEnabled(true);
            changebinding.layoutPassreset.setError(getString(R.string.err_pass_empty));
            changebinding.etpasswordReset.requestFocus();
            return false;
        }
        else if(changebinding.etpasswordReset.getText().toString().trim().length()<6){
            changebinding.layoutPassreset.setErrorEnabled(true);
            changebinding.layoutPassreset.setError(getString(R.string.err_msg_pass_value_not_match));
            changebinding.etpasswordReset.requestFocus();
            return false;
        }

        else if(changebinding.passconfirm.getText().toString().isEmpty())
        {
            changebinding.layoutPassconfirm.setErrorEnabled(true);
            changebinding.layoutPassconfirm.setError(getString(R.string.errconfirm_empty));
            changebinding.passconfirm.requestFocus();
            return false;
        }
        else if(changebinding.passconfirm.getText().toString().trim().length()<6)
        {
            changebinding.layoutPassconfirm.setErrorEnabled(true);
            changebinding.layoutPassconfirm.setError(getString(R.string.errconfirm_empty_length_not_match));
            changebinding.passconfirm.requestFocus();
            return false;
        }
        else if (!changebinding.etpasswordReset.getText().toString().equalsIgnoreCase(changebinding.passconfirm.getText().toString().trim())) {
            changebinding.layoutPassconfirm.setErrorEnabled(true);
            changebinding.layoutPassconfirm.setError(getString(R.string.confirm_pass_not_match));
            changebinding.layoutPassconfirm.requestFocus();
            return false;
        }
        return true;
    }

    //****************  Text Watcher Class Working On EditText Field **********************

    public class MyEditor implements TextWatcher {
        private View view;
        private MyEditor(View view) {
            this.view = view;
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
        @Override
        public void afterTextChanged(Editable s) {
            switch (view.getId()){
                case R.id.etpassword_reset:
                    if (!changebinding.etpasswordReset.getText().toString().trim().isEmpty()) {
                        changebinding.layoutPassreset.setErrorEnabled(false);
                    }
                    break;
                case R.id.passconfirm:
                    if (!changebinding.passconfirm.getText().toString().trim().isEmpty()) {
                        changebinding.layoutPassconfirm.setErrorEnabled(false);
                    }
                    break;
                case R.id.ivBack:
                    onBackPressed();
                    break;
            }

        }
    }

}
