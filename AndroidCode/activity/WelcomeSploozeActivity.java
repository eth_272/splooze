package com.splooze.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.splooze.R;
import com.splooze.databinding.ActivityWelcomeSploozeBinding;
import com.splooze.utils.AppConstants;

public class WelcomeSploozeActivity extends AppCompatActivity implements View.OnClickListener {
    private ActivityWelcomeSploozeBinding binding;
    private Context context;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this, R.layout.activity_welcome_splooze);
        context=WelcomeSploozeActivity.this;
        setOnClickListeners();
    }

    private void setOnClickListeners() {
        binding.tvViewprofile.setOnClickListener(this);
        binding.tvExplorenow.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_viewprofile:
            {
                Intent intent=new Intent(context,HomeActivity.class);
                intent.putExtra(AppConstants.NAVIGATE_FROM,"signup");
                intent.putExtra(AppConstants.NAVIGATION_FOR,"profile");
                startActivity(intent);
                break;
            }
            case R.id.tv_explorenow:
            {
                Intent intent=new Intent(context,HomeActivity.class);
                intent.putExtra(AppConstants.NAVIGATE_FROM,"signup");
                intent.putExtra(AppConstants.NAVIGATION_FOR,"explore");
                startActivity(intent);
                break;
            }
            
        }
    }

    @Override
    public void onBackPressed() {

    }
}
