package com.splooze.activity;

import android.app.Activity;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.splooze.R;
import com.splooze.adapter.ParticipantsAdapter;
import com.splooze.databinding.FragmentParticipantsBinding;
import com.splooze.model.ParticipantsModel;

import java.util.ArrayList;


public class ParticipantActivity extends AppCompatActivity implements View.OnClickListener {
    private FragmentParticipantsBinding binding;
    private Context context;
    private ArrayList<ParticipantsModel> MyList=new ArrayList<>();
    private Integer[] participantsImage=new Integer[]{R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,
            R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3};
    private String[] participantsName={"Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy",
            "Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy",};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this, R.layout.fragment_participants);
        context= ParticipantActivity.this;
        setOnClickListener();
        setRecyclerView();
    }

    private void setOnClickListener() {
        binding.toolbar.tvToolbarTitle.setText(getString(R.string.participants));
        binding.toolbar.ivToolbarLeft.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ivToolbarLeft:
                finish();
                break;

        }
    }
    private void setRecyclerView() {
        addDataToList();
        GridLayoutManager manager=new GridLayoutManager(context,3, LinearLayoutManager.VERTICAL,false);
        binding.rvParticipants.setLayoutManager(manager);
        //create a custom adapter for adding mutiple items.
        ParticipantsAdapter adapter=new ParticipantsAdapter(context,MyList);
        binding.rvParticipants.setAdapter(adapter);
    }

    //Add or set the item from model class
    private void addDataToList() {
        for(int a=0;a<participantsImage.length;a++){
            ParticipantsModel model=new ParticipantsModel();
            model.setParticipantsImage(participantsImage[a]);
            model.setParticipantsName(participantsName[a]);
            MyList.add(model);
        }
    }
}
