package com.splooze.activity;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;

import com.splooze.R;
import com.splooze.databinding.ActivitySignup1Binding;
import com.splooze.utils.AppConstants;

public class Signup1Activity extends AppCompatActivity {

    private ActivitySignup1Binding binding;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_signup1);
        context=Signup1Activity.this;
        setOnClickListeners();
    }


    private void setOnClickListeners() {
        disableAutoFill();
        
        binding.etInputname.addTextChangedListener(new Signup1Activity.MyWatcher(binding.etInputname));
        binding.etLastname.addTextChangedListener(new Signup1Activity.MyWatcher(binding.etLastname));
        binding.etEmail.addTextChangedListener(new Signup1Activity.MyWatcher(binding.etEmail));
        binding.etpassword.addTextChangedListener(new Signup1Activity.MyWatcher(binding.etpassword));
        binding.tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidation()) {
                    Intent intent = new Intent(context, Signup2Activity.class);
                    intent.putExtra(AppConstants.NAVIGATE_FROM,"SignUp");
                    intent.putExtra(AppConstants.FIRST_NAME,binding.etInputname.getText().toString().trim());
                    intent.putExtra(AppConstants.LAST_NAME,binding.etLastname.getText().toString().trim());
                    intent.putExtra(AppConstants.EMAIL,binding.etEmail.getText().toString().trim());
                    intent.putExtra(AppConstants.CONTACT,binding.etpassword.getText().toString().trim());
                    startActivity(intent);
                }
            }
        });


        /*Back Button Listener*/
        binding.ivBackSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        binding.etEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus && binding.etEmail.getText().toString().trim().length()==0)
                {
                    binding.etEmail.requestFocus();
                }
            }
        });
        binding.etLastname.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus && binding.etLastname.getText().toString().trim().length()==0)
                {
                    // First fill name
                    binding.etLastname.requestFocus();
                }
            }
        });

        binding.etInputname.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus && binding.etInputname.getText().toString().trim().length()==0)
                {
                    binding.etInputname.requestFocus();
                }
            }
        });
    }
    @TargetApi(Build.VERSION_CODES.O)
    private void disableAutoFill() {
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O)
            getWindow().getDecorView().setImportantForAutofill(View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS);
    }


    //***********************Check validaton from clint side*****************************
    private boolean checkValidation() {
        if (binding.etInputname.getText().toString().trim().isEmpty()) {
            binding.inputLayoutName.setErrorEnabled(true);
            binding.inputLayoutName.setError(getString(R.string.err_msg_name_empty));
            binding.etInputname.requestFocus();
            return false;
        } else if (binding.etInputname.getText().toString().length() < 3) {
            binding.inputLayoutName.setErrorEnabled(true);
            binding.inputLayoutName.setError(getString(R.string.valid_first_mane));
            binding.etInputname.requestFocus();
            return false;
        } else if (binding.etLastname.getText().toString().trim().isEmpty()) {
            binding.inputLayoutLastName.setErrorEnabled(true);
            binding.inputLayoutLastName.setError(getString(R.string.err_msg_last_empty));
            binding.etLastname.requestFocus();
            return false;
        } else if (binding.etLastname.getText().toString().trim().length() < 3) {
            binding.inputLayoutLastName.setErrorEnabled(true);
            binding.inputLayoutLastName.setError(getString(R.string.err_msg_last_isvalid));
            binding.etLastname.requestFocus();
            return false;
        } else if (binding.etEmail.getText().toString().trim().isEmpty()) {
            binding.inputLayoutEmail.setErrorEnabled(true);
            binding.inputLayoutEmail.setError(getString(R.string.err_msg_email_empty));
            binding.etEmail.requestFocus();
            return false;
        }
        else if (!Patterns.EMAIL_ADDRESS.matcher(binding.etEmail.getText().toString()).matches()) {
            binding.etEmail.requestFocus();
            binding.inputLayoutEmail.setErrorEnabled(true);
            binding.inputLayoutEmail.setError(getString(R.string.err_msg_email_value_not_match));
            //Toast.makeText(this, "Enter valid email", Toast.LENGTH_SHORT).show();
            return false;

        } else if (binding.etpassword.getText().toString().trim().isEmpty()) {

            binding.inputLayoutPassword.setErrorEnabled(true);
            binding.inputLayoutPassword.setError(getString(R.string.err_phone_empty));
            binding.etpassword.requestFocus();
            return false;
        } else if (binding.etpassword.getText().toString().trim().length() < 8) {
            binding.inputLayoutPassword.setErrorEnabled(true);
            binding.inputLayoutPassword.setError(getString(R.string.err_phone_lengthnotmatch));
            binding.etpassword.requestFocus();
            return false;
        }
        return true;
    }


    //****************  Text Watcher Class Working On EditText Field **********************

    private class MyWatcher implements TextWatcher {
        private View view;
        private MyWatcher(View view) {
            this.view = view;
        }
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }
        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.et_inputname:
                    if (!binding.etInputname.getText().toString().trim().isEmpty()) {
                        binding.inputLayoutName.setErrorEnabled(false);
                    }
                case R.id.et_lastname:
                    if (!binding.etLastname.getText().toString().trim().isEmpty()) {
                        binding.inputLayoutLastName.setErrorEnabled(false);
                    }
                case R.id.et_email:
                    if (!binding.etEmail.getText().toString().trim().isEmpty()) {
                        binding.inputLayoutEmail.setErrorEnabled(false);
                    }
                case R.id.etpassword:
                    if (!binding.etpassword.getText().toString().trim().isEmpty()) {
                        binding.inputLayoutPassword.setErrorEnabled(false);
                    }
            }
        //    18602662666
        }
    }
}





