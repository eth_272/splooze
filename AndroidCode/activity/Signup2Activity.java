package com.splooze.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.bruce.pickerview.popwindow.DatePickerPopWin;
import com.splooze.R;
import com.splooze.databinding.ActivitySignup3Binding;
import com.splooze.utils.AppConstants;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Signup2Activity extends AppCompatActivity implements View.OnClickListener {
    private ActivitySignup3Binding binding;
    static TextView DateEdit;
    private String[] Spinner_list={"Gender*","Male","Female"};
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_signup3);
        context= Signup2Activity.this;
        setOnClickListeners();
    }
    //************************  Implement Button Click Listener *********************
    private void setOnClickListeners() {
        disableAutoFill();
        //************************  Check Validation On Edit is empty or full from client side*********************
        binding.btnFourthNext.setOnClickListener(this);
        binding.etdate.setOnClickListener(this);
        binding.ivBack.setOnClickListener(this);
        binding.etpasswordNext.addTextChangedListener(new MyWatcher(binding.etpasswordNext));
        binding.etdate.addTextChangedListener(new MyWatcher(binding.etdate));
        binding.etConfirmpasswordNext.addTextChangedListener(new MyWatcher(binding.etConfirmpasswordNext));

        ArrayAdapter aa = new ArrayAdapter(this,R.layout.new_spinner,Spinner_list);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spinner.setAdapter(aa);
        //String text = binding.spinner.getSelectedItem().toString();
        String name= null;
        DateEdit=binding.etdate;
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void disableAutoFill() {
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O)
            getWindow().getDecorView().setImportantForAutofill(View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS);
    }

    ///*****************Dialog box of date picker*******
    private void showDatePickerDiolog(View v) {
        DialogFragment newFragment=new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_fourth_next:
                if (submitEntry()) {
                    Intent intent=new  Intent(context, Signup3Activity.class);
                    intent.putExtra(AppConstants.NAVIGATE_FROM,"SignUp");
                    intent.putExtra(AppConstants.FIRST_NAME,getIntent().getStringExtra(AppConstants.FIRST_NAME));
                    intent.putExtra(AppConstants.LAST_NAME,getIntent().getStringExtra(AppConstants.LAST_NAME));
                    intent.putExtra(AppConstants.EMAIL,getIntent().getStringExtra(AppConstants.EMAIL));
                    intent.putExtra(AppConstants.CONTACT,getIntent().getStringExtra(AppConstants.CONTACT));
                    intent.putExtra(AppConstants.PASSWORD,binding.etpasswordNext.getText().toString().trim());
                    intent.putExtra(AppConstants.DOB,binding.etdate.getText().toString().trim());
                    intent.putExtra(AppConstants.GENDER,binding.spinner.getSelectedItem().toString());
                    startActivity(intent);
                }
                break;
                case R.id.etdate:
                    //showDatePickerDiolog(v);
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(binding.etConfirmpasswordNext.getWindowToken(), 0);
                    showDatePickerDialog();
                    break;

                case R.id.ivBack:
                    onBackPressed();
                break;
        }
    }
    private void showDatePickerDialog() {
        DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(context, new DatePickerPopWin.OnDatePickedListener() {
            @Override
            public void onDatePickCompleted(int year, int month, int day, String dateDesc) {
                changeToNewDateFormat(dateDesc);
            }
        }).textConfirm("CONFIRM") //text of confirm button
                .textCancel("CANCEL") //text of cancel button
                .btnTextSize(16) // button text size
                .viewTextSize(30) // pick view text size
                .colorCancel(Color.parseColor("#999999")) //color of cancel button
                .colorConfirm(Color.parseColor("#009900"))//color of confirm button
                .minYear(1900) //min year in loop
                .maxYear(2019) // max year in loop
                .showDayMonthYear(true) // shows like dd mm yyyy (default is false)
                .dateChose("2018-01-01") // date chose when init popwindow
                .build();
        pickerPopWin.showPopWin((Activity) context);
    }

///*********** Method is Used Change Date Format as Per Our Requirement ************************

    private void changeToNewDateFormat(String selectedDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date sourceDate = null;
        try {
            sourceDate = dateFormat.parse(selectedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat targetFormat = new SimpleDateFormat("dd-MM-yyyy");
        String targetdatevalue= targetFormat.format(sourceDate);
        binding.etdate.setText(targetdatevalue);
    }


    //***Date Picker class which open dialog of date
    public  static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener{
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int dayy = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog dialog = new DatePickerDialog(getContext(), this, year, month, dayy);
            Field mDatePickerField;
            try {
                mDatePickerField = dialog.getClass().getDeclaredField("mDatePicker");
                mDatePickerField.setAccessible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
            dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            return dialog;
        }

        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            DateEdit.setText(dayOfMonth+ "/" + (month + 1) + "/" + year);
        }
    }
    //************************  Check Validation method at client side *********************
    private boolean submitEntry() {
        if(binding.etpasswordNext.getText().toString().isEmpty()){
            binding.layoutPassword.setErrorEnabled(true);
            binding.layoutPassword.setError(getString(R.string.err_pass_empty));
            binding.etpasswordNext.requestFocus();
            return false;

        }
        else if(binding.etpasswordNext.getText().toString().trim().length()<6){
            binding.layoutPassword.setErrorEnabled(true);
            binding.layoutPassword.setError(getString(R.string.err_msg_pass_value_not_match));
            binding.etpasswordNext.requestFocus();
            return false;
        }
        else if(binding.etConfirmpasswordNext.getText().toString().isEmpty())
        {
            binding.layoutConfirmpassword.setErrorEnabled(true);
            binding.layoutConfirmpassword.setError(getString(R.string.errconfirm_empty));
            binding.etConfirmpasswordNext.requestFocus();
            return false;
        }
        else if(binding.etConfirmpasswordNext.getText().toString().trim().length()<6)
        {
            binding.layoutConfirmpassword.setErrorEnabled(true);
            binding.layoutConfirmpassword.setError(getString(R.string.errconfirm_empty_length_not_match));
            binding.etConfirmpasswordNext.requestFocus();
            return false;
        }

        else if(!binding.etpasswordNext.getText().toString().trim().equalsIgnoreCase(binding.etConfirmpasswordNext.getText().toString().trim())){
            binding.layoutConfirmpassword.setErrorEnabled(true);
            binding.layoutConfirmpassword.setError(getString(R.string.confirm_pass_not_match1));
            binding.etConfirmpasswordNext.requestFocus();
            return false;
        }
        else if(binding.etdate.getText().toString().trim().equalsIgnoreCase("DOB*")){
            binding.layoutDate.setErrorEnabled(true);
            binding.layoutDate.setError("Please select DOB.");
            return false;
        }
        else if(binding.spinner.getSelectedItem().toString().equalsIgnoreCase("Gender*")){
            Toast.makeText(context, "Please select your gender.", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
    //****************  Text Watcher Class Working On EditText Field **********************
    private class MyWatcher implements TextWatcher {
        private View v;
        public MyWatcher(View v) {
            this.v=v;
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
        @Override
        public void afterTextChanged(Editable s) {
            switch (v.getId()) {
                case R.id.etpassword_next:
                    if (!binding.etpasswordNext.getText().toString().trim().isEmpty()) {
                        binding.layoutPassword.setErrorEnabled(false);
                        binding.layoutDate.setErrorEnabled(false);
                        binding.layoutConfirmpassword.setErrorEnabled(false);
                    }
                    break;
                case R.id.etdate:
                    if (!binding.etdate.getText().toString().trim().isEmpty()) {
                        binding.layoutDate.setErrorEnabled(false);
                        binding.layoutPassword.setErrorEnabled(false);
                        binding.layoutConfirmpassword.setErrorEnabled(false);
                    }
                    break;
                case R.id.et_confirmpassword_next:
                {
                    if(!binding.etConfirmpasswordNext.getText().toString().isEmpty()){
                        binding.layoutConfirmpassword.setErrorEnabled(false);
                        binding.layoutPassword.setErrorEnabled(false);
                        binding.layoutDate.setErrorEnabled(false);
                    }
                    break;
                }
            }
        }
    }
}





