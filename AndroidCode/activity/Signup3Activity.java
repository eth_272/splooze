package com.splooze.activity;


import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.splooze.R;
import com.splooze.databinding.ActivitySignup4Binding;
import com.splooze.image_manager.SelectPictureUtils;
import com.splooze.image_manager.TakePictureUtils;
import com.splooze.utils.AppConstants;
import com.splooze.utils.Commonutils;
import com.splooze.web_services_java.RetrofitExecuter;
import com.splooze.web_services_java.response_model.SignUpResponse;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static com.splooze.image_manager.TakePictureUtils.TAKE_PICTURE;


/**
 * A simple {@link Fragment} subclass.
 */
public class Signup3Activity extends AppCompatActivity implements View.OnClickListener {
    private ActivitySignup4Binding binding;
    private Context context;
    private static final int REQUEST_CODE_ASK_PERMISSIONS=2;
    private SelectPictureUtils pictureUtils;
    private Bitmap bitmap=null;
    private String filepath="";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(Signup3Activity.this, R.layout.activity_signup4);
        context = Signup3Activity.this;
        pictureUtils=new SelectPictureUtils(context);
        setOnClickListener();
    }

    private void setOnClickListener() {
        disableAutoFill();
        binding.tvSubmitdata.setOnClickListener(this);
        binding.ivImageCameraAllow.setOnClickListener(this);
        binding.civImageProfile.setOnClickListener(this);
        binding.txtConfirm.setOnClickListener(this);
        binding.ivBack.setOnClickListener(this);
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void disableAutoFill() {
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O)
            getWindow().getDecorView().setImportantForAutofill(View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS);
    }
    /*------On Textview or image click listener*-----*/

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_confirm: {
                Intent intent=new Intent(context,TermConditionsActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.iv_image_camera_allow: {
                marshmallow();
                break;
            }
            case R.id.tv_submitdata: {
                if (checkDetail()) {

                    if(Commonutils.isOnline(context)){
                        callUserRegistrationApi();
                    }
                    Toast.makeText(context, R.string.ple_check_internet, Toast.LENGTH_SHORT).show();
                }
                break;
            }
            case R.id.ivBack:
                onBackPressed();
                break;
        }
    }


    private void marshmallow() {
        if (Build.VERSION.SDK_INT < 23) {
            addPhotoDialog();
        } else {
            int hasPermissionCounter = 0;
            String[] permission = {Manifest.permission.CAMERA, READ_EXTERNAL_STORAGE};
            for (String aPermission : permission) {
                if (context.checkSelfPermission(aPermission) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{aPermission}, REQUEST_CODE_ASK_PERMISSIONS);
                } else {
                    hasPermissionCounter++;
                }
            }
            if (hasPermissionCounter == 2) {
                addPhotoDialog();
            }
        }
    }


    /*create a cutom dialog view which is perform action onclient click*/
    private void addPhotoDialog() {
        LayoutInflater inflater = LayoutInflater.from(context);
        final Dialog dialog=new Dialog(context,android.R.style.Theme_Translucent_NoTitleBar);
        if (dialog.getWindow() != null)
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow();
        // custom dialog
        View dialogLayout = inflater.inflate(R.layout.popup_profilepic, null);
        dialog.setContentView(dialogLayout);
        dialog.show();

        TextView tvCamera = (TextView) dialog.findViewById(R.id.tvCamera);
        TextView tvGallery = (TextView) dialog.findViewById(R.id.tvGallery);
        TextView tvCancel = (TextView) dialog.findViewById(R.id.tvCancel);

        tvCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                pictureUtils.takePictureFromCamera();
            }
        });
        tvGallery.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                pictureUtils.openGallery();
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }



    /*------Check validation on checkbox*-----*/

    private boolean checkDetail() {
        if (!binding.cbAccept.isChecked()) {
            Toast.makeText(context, "Check Accept the Conditions", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TakePictureUtils.PICK_GALLERY) {
            if (resultCode == RESULT_OK) {
                try {
                    InputStream inputStream = context.getContentResolver().openInputStream(data.getData());
                    FileOutputStream fileOutputStream = new FileOutputStream(new File(context.getExternalFilesDir(AppConstants.TEMP), pictureUtils.galleryImagePath + AppConstants.JPG_EXT));
                    TakePictureUtils.copyStream(inputStream, fileOutputStream);
                    fileOutputStream.close();
                    if (inputStream != null) {
                        inputStream.close();
                    }
                } catch (Exception ee) {
                    ee.printStackTrace();
                }
                if(data.getData()!=null){
                    try {
                        CropImage.activity(data.getData()).setGuidelines(CropImageView.Guidelines.ON).start((Activity) context);
                    } catch (Exception e) {
                    }
                }

            }
        }

        else if (requestCode == TAKE_PICTURE) {
            if(pictureUtils.getImagePath()!=null){
                Uri uri = Uri.fromFile(new File(pictureUtils.getImagePath()));
                CropImage.activity(uri).setGuidelines(CropImageView.Guidelines.ON).start((Activity) context);
            }
        }

        else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), result.getUri());
                    String path = null;
                    String first=null;
                    String second=null;
                    if (data != null) {
                        path = String.valueOf(result.getUri());
                        if(bitmap!=null)
                            binding.civImageProfile.setImageBitmap(bitmap);
                    }
                    if (path == null) {
                        return;
                    }
                    filepath = path;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }




    //************************ Call  User Registration Api *****************
    private void callUserRegistrationApi() {
        Commonutils.showProgressDialog(context);
        File file1;
        RequestBody reqFile1;
        MultipartBody.Part fileBody1 = null;

        if(!filepath.equalsIgnoreCase("")){
            file1 = new File(filepath);
            reqFile1 = RequestBody.create(MediaType.parse("image/*"), file1);
            fileBody1 = MultipartBody.Part.createFormData("avatar", file1.getName(), reqFile1);
        }
        RequestBody email,contact,first_name,last_name,user_type,password,dob,gender,club_name,address,city;
        RequestBody state,country,message,latitude,longitude,device_id,device_type;

        email = RequestBody.create(MediaType.parse("multipart/form-data"),getIntent().getStringExtra(AppConstants.EMAIL));
        contact = RequestBody.create(MediaType.parse("multipart/form-data"),getIntent().getStringExtra(AppConstants.CONTACT));
        first_name = RequestBody.create(MediaType.parse("multipart/form-data"),getIntent().getStringExtra(AppConstants.FIRST_NAME));
        last_name = RequestBody.create(MediaType.parse("multipart/form-data"),getIntent().getStringExtra(AppConstants.LAST_NAME));
        user_type = RequestBody.create(MediaType.parse("multipart/form-data"),"UR");
        password = RequestBody.create(MediaType.parse("multipart/form-data"),getIntent().getStringExtra(AppConstants.PASSWORD));
        dob = RequestBody.create(MediaType.parse("multipart/form-data"),getIntent().getStringExtra(AppConstants.DOB));
        gender = RequestBody.create(MediaType.parse("multipart/form-data"),getIntent().getStringExtra(AppConstants.GENDER));
        club_name = RequestBody.create(MediaType.parse("multipart/form-data"),"");
        address = RequestBody.create(MediaType.parse("multipart/form-data"),"");
        city = RequestBody.create(MediaType.parse("multipart/form-data"),"");
        state = RequestBody.create(MediaType.parse("multipart/form-data"),"");
        country = RequestBody.create(MediaType.parse("multipart/form-data"),"");
        message = RequestBody.create(MediaType.parse("multipart/form-data"),"");
        latitude = RequestBody.create(MediaType.parse("multipart/form-data"),"");
        longitude = RequestBody.create(MediaType.parse("multipart/form-data"),"");
        device_id = RequestBody.create(MediaType.parse("multipart/form-data"),"12345678");
        device_type = RequestBody.create(MediaType.parse("multipart/form-data"), "android");

        Call<SignUpResponse> call = RetrofitExecuter.getApiInterface("","","").
                callRegestrationApi(
                        fileBody1,
                        email,
                        contact,
                        first_name,
                        last_name,
                        user_type,
                        password,
                        dob,
                        gender,
                        club_name,
                        address,
                        city,
                        state,
                        country,
                        message,
                        latitude,
                        longitude,
                        device_id,
                        device_type);

        Log.e("url", "" + call.request().url());
        call.enqueue(new retrofit2.Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                Commonutils.disMissProgressDialog(context);

                if (response.body() != null && response.code()==201) {
                    Log.e("Get response", "" + new Gson().toJson(response.body()).toString());

                    Intent last = new Intent(context, Signup4Activity.class);
                    last.putExtra(AppConstants.NAVIGATE_FROM,"signup");
                    last.putExtra(AppConstants.USER_ID,""+response.body().getId());
                    last.putExtra(AppConstants.OTP,""+response.body().getOtpToken());
                    startActivity(last);
                }

            }
            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                Commonutils.disMissProgressDialog(context);
            }
        });


    }


}



