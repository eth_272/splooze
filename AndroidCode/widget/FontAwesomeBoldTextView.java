package com.splooze.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;


@SuppressWarnings("ALL")
public class FontAwesomeBoldTextView extends AppCompatTextView {
    public FontAwesomeBoldTextView(Context context) {
        super(context);
        this.setTypeface(setFont(context));

    }

    public FontAwesomeBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(setFont(context));

    }

    public FontAwesomeBoldTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.setTypeface(setFont(context));
    }
    public Typeface setFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/JosefinSans-SemiBold.ttf");
    }

}
