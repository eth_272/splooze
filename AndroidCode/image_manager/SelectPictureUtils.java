package com.splooze.image_manager;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


@SuppressWarnings("ALL")
public class SelectPictureUtils {

    private String imageName;
    private String mCurrentPhotoPath;
    public String galleryImagePath;
    private Context mContext;

    public SelectPictureUtils(Context context) {
        this.mContext = context;
    }

    public String getImagePath() {

        if (Build.VERSION.SDK_INT > 23) {
            return mCurrentPhotoPath;
        } else {
            return imageName + ".jpg";
        }

    }


    public void takePictureFromCamera() {
        if (Build.VERSION.SDK_INT > 23) {
            dispatchTakePictureIntent();
        } else {
            imageName = "picture_" + System.currentTimeMillis();
            takePicture((Activity) mContext, imageName);
        }

    }


    private void takePicture(Activity context, String fileName) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            Uri mImageCaptureUri = null;
            mImageCaptureUri = Uri.fromFile(new File(context.getExternalFilesDir("temp"), fileName + ".jpg"));
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            intent.putExtra("return-data", true);
            ((Activity) mContext).startActivityForResult(intent, TakePictureUtils.TAKE_PICTURE);

        } catch (ActivityNotFoundException e) {
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(mContext.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(mContext, "com.example.android.SploozeProvider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                ((Activity) mContext).startActivityForResult(takePictureIntent, TakePictureUtils.TAKE_PICTURE);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = mContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    public void openGallery() {
        galleryImagePath = "picture_" + System.currentTimeMillis();
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        ((Activity) mContext).startActivityForResult(photoPickerIntent, TakePictureUtils.PICK_GALLERY);
    }


    public String getGalleryImagePath(Intent intent) {
        if (Build.VERSION.SDK_INT > 23) {
            //  return galleryImagePath = imageName = "picture_" + System.currentTimeMillis();
            // return galleryImagePath = getRealPathFromURI(mContext, intent.getData());
            return galleryImagePath = galleryImagePath + ".jpg";
        } else {
            return galleryImagePath = galleryImagePath + ".jpg";
        }
    }

    private String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }


}
