package com.splooze.fragment;


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.splooze.R;
import com.splooze.activity.HomeActivity;
import com.splooze.adapter.ParticipantsAdapter;
import com.splooze.databinding.FragmentParticipantsBinding;
import com.splooze.model.ParticipantsModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ParticipantsFragment extends Fragment {
    private FragmentParticipantsBinding binding;
    private Context context;
    private ArrayList<ParticipantsModel> MyList=new ArrayList<>();
    private Integer[] participantsImage=new Integer[]{R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,
            R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3};
    private String[] participantsName={"Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy",
            "Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy",};
    
    public ParticipantsFragment() {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding= DataBindingUtil.inflate(inflater,R.layout.fragment_participants,container,false);
        setOnClickListeners();
        setRecyclerView();
        return binding.getRoot();
    }
    private void setOnClickListeners() {
        binding.toolbar.ivToolbarLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity)context).onBackPressed();
            }
        });
        binding.toolbar.tvToolbarTitle.setText(getString(R.string.participants));
    }
    //**************set the recycler view with the orientation of layout grid manager for grid view**************
    private void setRecyclerView() {
        addDataToList();
        HomeActivity.binding.bottomTab.llRoot.setVisibility(View.VISIBLE);
        GridLayoutManager manager=new GridLayoutManager(context,3,LinearLayoutManager.VERTICAL,false);
        binding.rvParticipants.setLayoutManager(manager);
        //create a custom adapter for adding mutiple items.
        ParticipantsAdapter adapter=new ParticipantsAdapter(context,MyList);
        binding.rvParticipants.setAdapter(adapter);
    }
    //Add or set the item from model class
    private void addDataToList() {
        for(int a=0;a<participantsImage.length;a++){
            ParticipantsModel model=new ParticipantsModel();
            model.setParticipantsImage(participantsImage[a]);
            model.setParticipantsName(participantsName[a]);
            MyList.add(model);
        }
    }
}
