package com.splooze.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.splooze.R;
import com.splooze.adapter.AllEventFilterAdapter;
import com.splooze.databinding.FragmentAllEventsBinding;
import com.splooze.interfaces.RecyclerItemListener;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;


/**
 * A simple {@link Fragment} subclass.
 */

@SuppressLint("ValidFragment")
public class PremiumEventFragment extends Fragment  implements  RecyclerItemListener {
    private Context context;
    private FragmentAllEventsBinding binding;
    public PremiumEventFragment(){

    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }
    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_all_events, container, false);
        setSliderWithRecyclerView();
        return binding.getRoot();
    }

    private void setSliderWithRecyclerView() {
        binding.llCityEvent.setVisibility(View.VISIBLE);
        binding.llFilterType.setVisibility(View.GONE);
        binding.dsAllEvent.setSlideOnFling(true);
        AllEventFilterAdapter adapter=new AllEventFilterAdapter(context,this);
        binding.dsAllEvent.setAdapter(adapter);
        binding.dsAllEvent.scrollToPosition(0);
        binding.dsAllEvent.setItemTransformer(new ScaleTransformer.Builder()
                .setMinScale(0.85f)
                .build());
    }

    @Override
    public void onClickOfOk(int position) {

    }
}
