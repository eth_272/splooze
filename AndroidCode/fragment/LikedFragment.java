package com.splooze.fragment;


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.splooze.R;
import com.splooze.activity.HomeActivity;
import com.splooze.adapter.LikedAdapter;
import com.splooze.databinding.FragmentLikedBinding;
import com.splooze.model.LikedModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class LikedFragment extends Fragment {
    private FragmentLikedBinding binding;
    private Context context;
    private ArrayList<LikedModel> MyList=new ArrayList();
    private Integer[] LikedImage=new Integer[]{R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,
            R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3};
    private String[] LikedName={"Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy",
            "Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy",};

    public LikedFragment() {
        // Required empty public constructor
    }
    
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding= DataBindingUtil.inflate(inflater,R.layout.fragment_liked,container,false);
        setOnClickListeners();
        setRecyclerView();
        return binding.getRoot();
    }
    private void setOnClickListeners() {
        binding.toolbar.ivToolbarLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity)context).onBackPressed();
            }
        });
        binding.toolbar.tvToolbarTitle.setText(getString(R.string.liked));
    }
    //**************set the recycler view with the orientation of layout grid manager for grid view**************
    private void setRecyclerView() {
        addDataToList();
        GridLayoutManager manager=new GridLayoutManager(context,3, LinearLayoutManager.VERTICAL,false);
        binding.rvLiked.setLayoutManager(manager);
        //create a custom adapter for adding multiple items.
        LikedAdapter adapter=new LikedAdapter(context,MyList);
        binding.rvLiked.setAdapter(adapter);
    }
    //Add or set the item from model class
    private void addDataToList() {
        for(int a=0;a<LikedImage.length;a++){
            LikedModel model=new LikedModel();
            model.setLikedImage(LikedImage[a]);
            model.setLikedName(LikedName[a]);
            MyList.add(model);
        }
    }
}
