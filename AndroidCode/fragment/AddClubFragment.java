package com.splooze.fragment;


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.splooze.R;
import com.splooze.databinding.FragmentAddClubBinding;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddClubFragment extends Fragment implements View.OnClickListener {
    private FragmentAddClubBinding binding;
    private Context context;
    
    public AddClubFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }
    @Override 
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_club, container, false);
        setOnClickListeners();
        return binding.getRoot();
    }
    private void  setOnClickListeners() {
        binding.etName.addTextChangedListener(new MyWatcher(binding.etName));
        binding.etCity.addTextChangedListener(new MyWatcher(binding.etCity));
        binding.etStreetAddress.addTextChangedListener(new MyWatcher(binding.etStreetAddress));
        binding.tvAddChooseLocation.setOnClickListener(this);
        binding.ivBack.setOnClickListener(this);
        binding.tvSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ivBack:
                Toast.makeText(context, "Under working", Toast.LENGTH_SHORT).show();
                break;
            case R.id.tvChooseLocation:
                if(!binding.tvAddChooseLocation.getText().toString().isEmpty()){
                    manageValidation();
                }
                break;
            case R.id.tvBeverage:
                Toast.makeText(context, "Under working", Toast.LENGTH_SHORT).show();
                break;
            case R.id.tvSubmit:
                if(checkValidation()){
                    Toast.makeText(context, "Thank you", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    //******************  Use Method TO Check Validation At Client Size ********************
    private boolean checkValidation() {
        if (binding.etName.getText().toString().trim().isEmpty()) {
            binding.tilName.setErrorEnabled(true);
            binding.tilName.setError(getString(R.string.err_name_empty));
            binding.etName.requestFocus();
            return false;
        }
        else if(binding.etName.getText().toString().trim().length()<3){
            binding.tilName.setErrorEnabled(true);
            binding.tilName.setError(getString(R.string.err_name_valid));
            binding.etName.requestFocus();
            return false;
        }
        /*else if (binding.tvAddChooseLocation.getText().toString().equalsIgnoreCase("Choose Location*")){
            binding.tilAddChooseLocation.setErrorEnabled(true);
            binding.tilAddChooseLocation.setError(getString(R.string.errChooseLocation));
            Toast.makeText(context,"*Please choose location." , Toast.LENGTH_SHORT).show();
            binding.tvAddChooseLocation.requestFocus();
            return false;
        }*/
        else if(binding.etCity.getText().toString().isEmpty()){
            binding.tilCity.setErrorEnabled(true);
            binding.tilCity.setError(getString(R.string.errCity));
            binding.etCity.requestFocus();
            return false;
        }
        else if(binding.etCity.getText().toString().length()<3){
            binding.tilCity.setErrorEnabled(true);
            binding.tilCity.setError(getString(R.string.errCityValid));
            binding.etCity.requestFocus();
            return false;
        }
        else if(binding.etStreetAddress.getText().toString().isEmpty()){
            binding.tilStreetAddress.setErrorEnabled(true);
            binding.tilStreetAddress.setError(getString(R.string.errStreetAdress));
            binding.etStreetAddress.requestFocus();
            return false;
        }
        else if(binding.etStreetAddress.getText().toString().length()<3){
            binding.tilStreetAddress.setErrorEnabled(true);
            binding.tilStreetAddress.setError(getString(R.string.errSreetAddressValid));
            binding.etStreetAddress.requestFocus();
            return false;
        }

        return true;
    }

    //***************  Manage All Error Validation ********************
    private void manageValidation() {
        binding.tilName.setErrorEnabled(false);
        binding.tilStreetAddress.setErrorEnabled(false);
       // binding.tilAddChooseLocation.setErrorEnabled(false);
        binding.tilCity.setErrorEnabled(false);
    }
    //*****************  This Is Text Watcher Class Perform Operation At RunTime On EditTextField *********************
    private class MyWatcher implements TextWatcher {

        private View view;
        private MyWatcher(View view) {
            this.view = view;
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            switch (view.getId()) {

                case R.id.etName:
                    if(!binding.etName.getText().toString().isEmpty()){
                        manageValidation();
                    }
                    break;
                case R.id.etCity:
                    if(!binding.etCity.getText().toString().isEmpty()){
                        manageValidation();
                    }
                    break;
                case R.id.etStreetAddress:
                    if(!binding.etStreetAddress.getText().toString().isEmpty()){
                        manageValidation();
                    }
                    break;
            }

        }
    }
}
