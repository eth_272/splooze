package com.splooze.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.labo.kaji.fragmentanimations.CubeAnimation;
import com.labo.kaji.fragmentanimations.MoveAnimation;
import com.splooze.R;
import com.splooze.activity.HomeActivity;
import com.splooze.adapter.BeverageAdapter;
import com.splooze.adapter.SheetCountAdapter;
import com.splooze.databinding.FragmentEventDetailsBinding;
import com.splooze.interfaces.RecyclerItemListener;
import com.splooze.model.SheetCountModel;
import com.splooze.utils.Commonutils;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */

@SuppressLint("ValidFragment")
public class EventDetailsFragment extends Fragment implements View.OnClickListener , RecyclerItemListener {
    private Context context;
    private FragmentEventDetailsBinding binding;
    private ArrayList<SheetCountModel> sheetCountList=new ArrayList<>();
    private Integer [] noOfSheet=new Integer[]{1,2,3,4,5,6,7,8,9,10};
    private LinearLayoutManager mLayoutManager;
    private LinearLayoutManager mLayoutManager1;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }
    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_event_details, container, false);
        setOnClickListeners();
        return binding.getRoot();
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (enter) {
            return MoveAnimation.create(MoveAnimation.UP, enter, 500);
        } else {
            return MoveAnimation.create(MoveAnimation.DOWN, enter, 500);
        }
    }

    private void setOnClickListeners() {
        HomeActivity.binding.bottomTab.llRoot.setVisibility(View.INVISIBLE);
        binding.tvBookNow.setText("Interested");
        binding.tvBookNow.setOnClickListener(this);
        binding.tvNotInterested.setOnClickListener(this);
        binding.ivBack.setOnClickListener(this);
        binding.ivSearch.setOnClickListener(this);
        binding.ivShare.setOnClickListener(this);
        binding.flParticipant.setOnClickListener(this);
        mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        mLayoutManager1 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);


        binding.etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    binding.llSearch.setVisibility(View.GONE);
                    binding.flTab.setVisibility(View.VISIBLE);
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(binding.etSearch.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });
        addDataToSheetCountList();
    }

    private void addDataToSheetCountList() {
        sheetCountList.clear();
        for (int i = 0; i <noOfSheet.length ; i++) {
            SheetCountModel model=new SheetCountModel();
            if(i==0){
                model.isSelected=true;
            }else {
                model.isSelected=false;
            }
            model.countNo=noOfSheet[i];
            sheetCountList.add(model);
        }
        setRecyclerForSheetBooking();
        setRecyclerForBeverage();
    }

    private void setRecyclerForSheetBooking() {
        binding.rvTableCount.setLayoutManager(mLayoutManager);
        SheetCountAdapter adapter=new SheetCountAdapter(context,this,sheetCountList);
        binding.rvTableCount.setAdapter(adapter);
    }

    private void setRecyclerForBeverage() {
        binding.rvBeverages.setLayoutManager(mLayoutManager1);
        BeverageAdapter adapter=new BeverageAdapter(context,this);
        binding.rvBeverages.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.tvBookNow:
                if(binding.tvBookNow.getText().toString().trim().equalsIgnoreCase("Interested")){
                    binding.tvBookNow.setText("Book Now");
                }else {
                    Toast.makeText(context, R.string.work_in_progress, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.ivBack:
                HomeActivity.binding.bottomTab.llRoot.setVisibility(View.VISIBLE);
                ((HomeActivity)context).onBackPressed();
                break;

            case R.id.tvNotInterested:
                HomeActivity.binding.bottomTab.llRoot.setVisibility(View.VISIBLE);
                ((HomeActivity)context).onBackPressed();
                break;

            case R.id.flParticipant:
                ParticipantsFragment fragment = new ParticipantsFragment();
                Commonutils.setFragment(fragment, false, (FragmentActivity) context, R.id.flHome);
                break;

            case R.id.ivSearch:
                binding.etSearch.setImeOptions(EditorInfo.IME_ACTION_DONE);
                binding.etSearch.requestFocus();
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
                binding.llSearch.setVisibility(View.VISIBLE);
                break;

            case R.id.ivShare:
                shareEventUrl();
                break;
        }
    }

    private void shareEventUrl() {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, "Sharing URL");
        i.putExtra(Intent.EXTRA_TEXT, "http://www.url.com");
        startActivity(Intent.createChooser(i, "Share URL"));
    }

    @Override
    public void onClickOfOk(int position) {

    }
}
