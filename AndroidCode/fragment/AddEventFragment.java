package com.splooze.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.splooze.R;
import com.splooze.activity.AddBeveragesActivity;
import com.splooze.activity.SelectBeveragesActivity;
import com.splooze.databinding.FragmentAddEventBinding;
import com.splooze.image_manager.SelectPictureUtils;
import com.splooze.image_manager.TakePictureUtils;
import com.splooze.utils.AppConstants;
import com.splooze.utils.Commonutils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.StringTokenizer;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_OK;
import static com.splooze.image_manager.TakePictureUtils.TAKE_PICTURE;


@SuppressLint("ValidFragment")
public class AddEventFragment extends Fragment implements View.OnClickListener {
    private Context context;
    private FragmentAddEventBinding binding;
    public static TextView DateEdit;
    private Calendar startTime;
    private String docFile1="";
    private Bitmap bm=null;
    private SelectPictureUtils selectPictureUtils;
    private int REQUEST_CODE_ASK_PERMISSIONS = 2;
    private String[] spinnerItems={"Event Category*","Music","Singing","Dance"};
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }
    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_event, container, false);
        selectPictureUtils = new SelectPictureUtils(context);
        setOnClickListeners();
        return binding.getRoot();
    }

    private void setOnClickListeners() {
        binding.tvSubmit.setOnClickListener(this);
        binding.tvEventDate.setOnClickListener(this);
        binding.tvUploadPhoto.setOnClickListener(this);
        binding.tvEventStartTime.setOnClickListener(this);
        binding.tvBeverage.setOnClickListener(this);

        DateEdit=binding.tvEventDate;
        ArrayAdapter aa = new ArrayAdapter(context,R.layout.new_spinner,spinnerItems);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spEventCategory.setAdapter(aa);

        binding.etEventVenue.addTextChangedListener(new MyWatcher(binding.etEventVenue));
        binding.etEventName.addTextChangedListener(new MyWatcher(binding.etEventName));
        binding.etEventSortDes.addTextChangedListener(new MyWatcher(binding.etEventSortDes));


        binding.spEventCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                if(position==0){

                }else {
                    manageValidationErrors();
                }

            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tvSubmit:
                //Toast.makeText(context, R.string.work_in_progress, Toast.LENGTH_SHORT).show();


                if(checkValidation()){
                    selectYourPlanDialog();
                }
                break;

            case R.id.tvUploadPhoto:
                manageValidationErrors();
                marshmallow();
                break;

            case R.id.tvBeverage:
                Intent intent=new Intent(context, SelectBeveragesActivity.class);
                startActivity(intent);
                break;

            case R.id.tvEventStartTime:
                selectStartTime();
                break;

            case R.id.tvEventDate:
                showDatePickerDialog();
                break;
        }
    }

    private void showDatePickerDialog() {
        DialogFragment newFragment=new DatePickerFragment();
        newFragment.show(getFragmentManager(), "datePicker");
        manageValidationErrors();
    }


    //******************   Call Method For Give Permission on Marshmallow and > *************************
    private void marshmallow() {
        if (Build.VERSION.SDK_INT < 23) {
            addPhotoDialog();
        } else {
            int hasPermissionCounter = 0;
            String[] permission = {Manifest.permission.CAMERA, READ_EXTERNAL_STORAGE};
            for (String aPermission : permission) {
                if (context.checkSelfPermission(aPermission) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{aPermission}, REQUEST_CODE_ASK_PERMISSIONS);
                } else {
                    hasPermissionCounter++;
                }
            }
            if (hasPermissionCounter == 2) {
                addPhotoDialog();
            }
        }
    }


    //**********  Open Dialog For Camera Or Gallery Picker ****************
    protected void addPhotoDialog() {
        LayoutInflater inflater = LayoutInflater.from(context);
        final Dialog mDialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        if (mDialog.getWindow() != null)
            mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        mDialog.getWindow().setGravity(Gravity.BOTTOM);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();
        View dialogLayout = inflater.inflate(R.layout.popup_profilepic, null);
        mDialog.setContentView(dialogLayout);
        mDialog.show();
        TextView tvCamera = (TextView) mDialog.findViewById(R.id.tvCamera);
        TextView tvGallery = (TextView) mDialog.findViewById(R.id.tvGallery);
        TextView tvCancel = (TextView) mDialog.findViewById(R.id.tvCancel);

        tvCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                selectPictureUtils.takePictureFromCamera();
            }
        });
        tvGallery.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                selectPictureUtils.openGallery();
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
    }

    //****************  This Method is use to check validation at client side **************
    private boolean checkValidation() {
        if(binding.etEventName.getText().toString().equalsIgnoreCase("")){
            binding.tilEventName.setErrorEnabled(true);
            binding.tilEventName.setError(getString(R.string.ple_enter_event_name));
            binding.etEventName.requestFocus();
            Toast.makeText(context,getString(R.string.ple_enter_event_name) , Toast.LENGTH_SHORT).show();
            return false;
        }
        else  if(binding.etEventName.getText().toString().length()<2){
            binding.tilEventName.setErrorEnabled(true);
            binding.tilEventName.setError(getString(R.string.ple_enter_valid_event_name));
            binding.etEventName.requestFocus();
            Toast.makeText(context,getString(R.string.ple_enter_valid_event_name) , Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(binding.spEventCategory.getSelectedItem().toString().equalsIgnoreCase("Event Category*")){
            binding.tilSpinner.setErrorEnabled(true);
            binding.tilSpinner.setError(getString(R.string.ple_select_event_category));
            binding.tilSpinner.requestFocus();
            Toast.makeText(context,getString(R.string.ple_select_event_category) , Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(binding.tvEventDate.getText().toString().trim().equalsIgnoreCase("Event Date*")){
            binding.tilEventDate.setErrorEnabled(true);
            binding.tilEventDate.setError(getString(R.string.ple_select_event_date));
            Toast.makeText(context,getString(R.string.ple_select_event_date) , Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(binding.tvEventStartTime.getText().toString().trim().equalsIgnoreCase("Start Time*")){
            binding.tilEventStartTime.setErrorEnabled(true);
            binding.tilEventStartTime.setError(getString(R.string.ple_select_event_start_time));
            Toast.makeText(context,getString(R.string.ple_select_event_start_time) , Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(binding.tvUploadPhoto.getText().toString().trim().equalsIgnoreCase("Upload Photo*")){
            binding.tilUploadPhoto.setErrorEnabled(true);
            binding.tilUploadPhoto.setError(getString(R.string.ple_upload_event_photo));
            Toast.makeText(context,getString(R.string.ple_upload_event_photo) , Toast.LENGTH_SHORT).show();
            return false;
        }

        if(binding.etEventVenue.getText().toString().equalsIgnoreCase("")){
            binding.tilEventVenue.setErrorEnabled(true);
            binding.tilEventVenue.setError("*Please enter event venue.");
            binding.tilEventVenue.requestFocus();
            return false;
        }

        return true;
    }


    //***********  This method is call for select Start Event Time **********************
    private void selectStartTime() {
        startTime = Calendar.getInstance();
        int hour = startTime.get(Calendar.HOUR_OF_DAY);
        int minute = startTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                manageValidationErrors();
                binding.tvEventStartTime.setText( String.format("%02d", selectedHour)+ ":" +String.format("%02d", selectedMinute));
            }
        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    public  static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener{
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int dayy = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog dialog = new DatePickerDialog(getContext(), this, year, month, dayy);
            Field mDatePickerField;
            try {
                mDatePickerField = dialog.getClass().getDeclaredField("mDatePicker");
                mDatePickerField.setAccessible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
            dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            return dialog;
        }

        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            DateEdit.setText(dayOfMonth+ "/" + (month + 1) + "/" + year);
        }
    }



    //****************  Text Watcher Class Working On EditText Field **********************
    private class MyWatcher implements TextWatcher {
        private View v;
        public MyWatcher(View v) {
            this.v=v;
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
        @Override
        public void afterTextChanged(Editable s) {
            switch (v.getId()) {
                case R.id.etEventName:
                    if (!binding.etEventName.getText().toString().trim().isEmpty()) {
                        manageValidationErrors();
                    }
                    break;
                case R.id.etEventSortDes:
                    if (!binding.etEventName.getText().toString().trim().isEmpty()) {
                        manageValidationErrors();
                    }
                    break;
                case R.id.etEventVenue:
                {
                    if(!binding.etEventVenue.getText().toString().isEmpty()){
                        manageValidationErrors();
                    }
                    break;
                }
            }
        }
    }


    //***************  Manage All Error Validation ********************
    public void manageValidationErrors(){
        binding.tilEventName.setErrorEnabled(false);
        binding.tilSpinner.setErrorEnabled(false);
        binding.tilEventSortDes.setErrorEnabled(false);
        binding.tilEventDate.setErrorEnabled(false);
        binding.tilEventStartTime.setErrorEnabled(false);
        binding.tilEventAmount.setErrorEnabled(false);
        binding.tilUploadPhoto.setErrorEnabled(false);
        binding.tilNumberOfSheet.setErrorEnabled(false);
        binding.tilBeverage.setErrorEnabled(false);
        binding.tilEventVenue.setErrorEnabled(false);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == TakePictureUtils.PICK_GALLERY) {
            if (resultCode == RESULT_OK) {
                try {
                    InputStream inputStream = context.getContentResolver().openInputStream(data.getData());
                    FileOutputStream fileOutputStream = new FileOutputStream(new File(context.getExternalFilesDir(AppConstants.TEMP), selectPictureUtils.galleryImagePath + AppConstants.JPG_EXT));
                    TakePictureUtils.copyStream(inputStream, fileOutputStream);
                    fileOutputStream.close();
                    if (inputStream != null) {
                        inputStream.close();
                    }
                } catch (Exception ee) {
                    ee.printStackTrace();
                }
                if(data.getData()!=null){
                    try {
                        CropImage.activity(data.getData()).setGuidelines(CropImageView.Guidelines.ON).start((Activity) context);
                    } catch (Exception e) {
                    }
                }

            }
        }

        else if (requestCode == TAKE_PICTURE) {
            if(selectPictureUtils.getImagePath()!=null){
                Uri uri = Uri.fromFile(new File(selectPictureUtils.getImagePath()));
                CropImage.activity(uri).setGuidelines(CropImageView.Guidelines.ON).start((Activity) context);
            }
        }

        else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                try {
                    bm = MediaStore.Images.Media.getBitmap(context.getContentResolver(), result.getUri());
                    String path = null;
                    if (data != null) {
                        path = String.valueOf(result.getUri());
                        StringTokenizer multiTokenizer = new StringTokenizer(path, "/");
                        while (multiTokenizer.hasMoreTokens())
                        {
                            binding.tvUploadPhoto.setText(multiTokenizer.nextToken());
                        }

                    }
                    if (path == null) {
                        return;
                    }
                    docFile1=path;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }

    }

    //***************Custom Dialog for Plan choose*************

    private void selectYourPlanDialog() {
        LayoutInflater inflater = LayoutInflater.from(context);
        final Dialog dialog=new Dialog(context);
        if (dialog.getWindow() != null)
           // dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow();
        // custom dialog
        View dialogLayout = inflater.inflate(R.layout.custom_dialog, null);
        dialog.setContentView(dialogLayout);
        dialog.show();

        TextView tvPaidPremium = (TextView) dialog.findViewById(R.id.tvPaidPremium);
        TextView tvFreeEvents = (TextView) dialog.findViewById(R.id.tvFreeEvents);
        FrameLayout flClose = (FrameLayout) dialog.findViewById(R.id.flClose);
        
        tvPaidPremium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Toast.makeText(context, R.string.work_in_progress, Toast.LENGTH_SHORT).show();
            }
        });
        tvFreeEvents.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Toast.makeText(context, "Your Event Create Successfully.", Toast.LENGTH_SHORT).show();
                HomeFragment  fragment = new HomeFragment();
                Commonutils.setFragment(fragment, true, (FragmentActivity) context, R.id.flHome);
            }
        });
        
        flClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
}
