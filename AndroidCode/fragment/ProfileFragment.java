package com.splooze.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.splooze.R;
import com.splooze.activity.HomeActivity;
import com.splooze.activity.LoginActivity;
import com.splooze.databinding.FragmentProfileBinding;
import com.splooze.utils.AppConstants;
import com.splooze.utils.Commonutils;


/**
 * A simple {@link Fragment} subclass.
 */

@SuppressLint("ValidFragment")
public class ProfileFragment extends Fragment implements View.OnClickListener {
    private Context context;
    private Fragment fragment;
    private boolean isLiked=true;
    private FragmentProfileBinding binding;
    private String navigationFrom="";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }
    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);
        setOnClickListeners();
        manageProfileFlow();
        return binding.getRoot();
    }


    private void manageProfileFlow() {
        if(getArguments().getString(AppConstants.NAVIGATE_FROM).equalsIgnoreCase("Home")){
            binding.ivBack.setVisibility(View.INVISIBLE);
            binding.tvFollowUser.setVisibility(View.INVISIBLE);
            binding.tvTitle.setText("MY PROFILE");
            binding.flLike.setVisibility(View.GONE);
        }else {
            binding.ivEdit.setVisibility(View.INVISIBLE);
            binding.ivLogout.setVisibility(View.INVISIBLE);
            binding.llMyPost.setVisibility(View.GONE);
            binding.llMyHangout.setVisibility(View.GONE);
            binding.llMyTicket.setVisibility(View.GONE);
            binding.viewMyPost.setVisibility(View.GONE);
            binding.viewMyHangout.setVisibility(View.GONE);
            binding.viewMyTicket.setVisibility(View.GONE);
            binding.tvTitle.setText("PROFILE");
            binding.flLike.setVisibility(View.VISIBLE);
        }
    }

    private void setOnClickListeners() {
        binding.llFollowers.setOnClickListener(this);
        binding.llFollow.setOnClickListener(this);
        binding.llMyPost.setOnClickListener(this);
        binding.llMyHangout.setOnClickListener(this);
        binding.llMyTicket.setOnClickListener(this);
        binding.llLiked.setOnClickListener(this);
        binding.ivBack.setOnClickListener(this);
        binding.ivEdit.setOnClickListener(this);
        binding.ivLogout.setOnClickListener(this);
        binding.flLike.setOnClickListener(this);
        binding.tvFollowUser.setOnClickListener(this);
        HomeActivity.binding.bottomTab.llRoot.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.llFollowers:
                fragment = new FollowersFragment();
                Commonutils.setFragment(fragment, false, (FragmentActivity) context, R.id.flHome);
                break;

            case R.id.llFollow:
                fragment = new FollowingFragment();
                Commonutils.setFragment(fragment, false, (FragmentActivity) context, R.id.flHome);
                break;

            case R.id.ivBack:
                ((HomeActivity)context).onBackPressed();
                break;

            case R.id.llMyPost:
                fragment = new ExploreFragment();
                Commonutils.setFragment(fragment, false, (FragmentActivity) context, R.id.flHome);
                break;

            case R.id.llMyHangout:
                fragment = new HistoryEventFragment();
                Commonutils.setFragment(fragment, false, (FragmentActivity) context, R.id.flHome);
                break;

            case R.id.llMyTicket:
                fragment = new TicketListFragment();
                Commonutils.setFragment(fragment, false, (FragmentActivity) context, R.id.flHome);
                //Toast.makeText(context, R.string.work_in_progress, Toast.LENGTH_SHORT).show();
                break;

            case R.id.llLiked:
                fragment = new LikedFragment();
                Commonutils.setFragment(fragment, false, (FragmentActivity) context, R.id.flHome);
                break;

            case R.id.tvFollowUser:
                Toast.makeText(context, R.string.work_in_progress, Toast.LENGTH_SHORT).show();
                break;

            case R.id.flLike:

                if(isLiked){
                    isLiked=false;
                    binding.ivLike.setImageResource(R.mipmap.likeclick);
                }else {
                    binding.ivLike.setImageResource(R.mipmap.like);
                    isLiked=true;
                }

                break;
            case R.id.ivEdit:
                fragment = new EditProfileFragment();
                Commonutils.setFragment(fragment, false, (FragmentActivity) context, R.id.flHome);
                break;

            case R.id.ivLogout:
                new android.app.AlertDialog.Builder(context).setTitle("Logout")
                        .setMessage("Are you sure you want to logout from the app?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getActivity().finish();
                                Intent intent=new Intent(context, LoginActivity.class);
                                startActivity(intent);
                            }
                        }).setNegativeButton("No", null).show();

                break;
        }
    }

}
