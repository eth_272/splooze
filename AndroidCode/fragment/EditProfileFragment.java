package com.splooze.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.bruce.pickerview.popwindow.DatePickerPopWin;
import com.splooze.R;
import com.splooze.activity.AddBeveragesActivity;
import com.splooze.activity.ChangePasswordActivity;
import com.splooze.activity.HomeActivity;
import com.splooze.databinding.FragmentEditProfileBinding;
import com.splooze.image_manager.SelectPictureUtils;
import com.splooze.image_manager.TakePictureUtils;
import com.splooze.utils.AppConstants;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_OK;
import static com.splooze.image_manager.TakePictureUtils.TAKE_PICTURE;

/**
 * A simple {@link Fragment} subclass.
 */

@SuppressLint("ValidFragment")
public class EditProfileFragment extends Fragment implements View.OnClickListener {
    private Context context;
    private FragmentEditProfileBinding binding;
    private SelectPictureUtils selectPictureUtils;
    private String docFile1="";
    private Bitmap bm=null;
    public static TextView tvSelectDOB;
    private String[] Spinner_list={"Gender*","Male","Female"};
    private int REQUEST_CODE_ASK_PERMISSIONS = 2;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }
    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_edit_profile, container, false);
        selectPictureUtils = new SelectPictureUtils(context);
        setOnClickListreners();
        return binding.getRoot();
    }

    private void setOnClickListreners() {
        tvSelectDOB=binding.tvSelectDOB;
        binding.toolbar.tvToolbarTitle.setText("EDIT PROFILE");
        binding.toolbar.ivToolbarLeft.setOnClickListener(this);
        binding.llChangePassword.setOnClickListener(this);
        binding.llEditClub.setOnClickListener(this);
        binding.tvSelectDOB.setOnClickListener(this);
        binding.tvSubmit.setOnClickListener(this);
        binding.tvEditBeverages.setOnClickListener(this);
        binding.ivImageCamera.setOnClickListener(this);
        binding.etFirstName.addTextChangedListener(new MyWatcher(binding.etFirstName));
        binding.etLastName.addTextChangedListener(new MyWatcher(binding.etLastName));
        binding.etContactNumber.addTextChangedListener(new MyWatcher(binding.etContactNumber));

        ArrayAdapter aa = new ArrayAdapter(context,R.layout.new_spinner,Spinner_list);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spinner.setAdapter(aa);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.ivToolbarLeft:
                ((HomeActivity)context).onBackPressed();
                break;

            case R.id.iv_image_camera:
                marshmallow();
                break;

            case R.id.llChangePassword:
                Intent intent=new Intent(context, ChangePasswordActivity.class);
                startActivity(intent);
                break;

            case R.id.llEditClub:
                break;

            case R.id.tvSelectDOB:
                clearEditTextFieldErrors();
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(binding.etContactNumber.getWindowToken(), 0);
                showDatePickerDialog();
                //showDatePickerDialog(v);
                break;

            case R.id.tvSubmit:
                if(checkValidation()){
                    Toast.makeText(context, "Profile Updated Successfully.", Toast.LENGTH_SHORT).show();
                    ((HomeActivity)context).onBackPressed();
                }
                break;

            case R.id.tvEditBeverages:
                Intent intentBeverage=new Intent(context, AddBeveragesActivity.class);
                startActivity(intentBeverage);
                break;
        }

    }

    private void showDatePickerDialog() {
        DatePickerPopWin pickerPopWin = new DatePickerPopWin.Builder(context, new DatePickerPopWin.OnDatePickedListener() {
            @Override
            public void onDatePickCompleted(int year, int month, int day, String dateDesc) {
                changeToNewDateFormat(dateDesc);
            }
        }).textConfirm("CONFIRM") //text of confirm button
                .textCancel("CANCEL") //text of cancel button
                .btnTextSize(16) // button text size
                .viewTextSize(30) // pick view text size
                .colorCancel(Color.parseColor("#999999")) //color of cancel button
                .colorConfirm(Color.parseColor("#009900"))//color of confirm button
                .minYear(1900) //min year in loop
                .maxYear(2019) // max year in loop
                .showDayMonthYear(true) // shows like dd mm yyyy (default is false)
                .dateChose("2018-01-01") // date chose when init popwindow
                .build();
        pickerPopWin.showPopWin((Activity) context);
    }

///*********** Method is Used Change Date Format as Per Our Requirement ************************

    private void changeToNewDateFormat(String selectedDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date sourceDate = null;
        try {
            sourceDate = dateFormat.parse(selectedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat targetFormat = new SimpleDateFormat("dd-MM-yyyy");
        String targetdatevalue= targetFormat.format(sourceDate);
        binding.tvSelectDOB.setText(targetdatevalue);
        //Toast.makeText(context, targetdatevalue, Toast.LENGTH_SHORT).show();
    }


    ///************   This Method is call For Date Picker Purpose ******************

    private void showDatePickerDialog(View v) {
        DialogFragment newFragment=new DatePickerFragment();
        newFragment.show(getChildFragmentManager(), "datePicker");
    }

    public  static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener{
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int dayy = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog dialog = new DatePickerDialog(getContext(), this, year, month, dayy);
            Field mDatePickerField;
            try {
                mDatePickerField = dialog.getClass().getDeclaredField("mDatePicker");
                mDatePickerField.setAccessible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }

            dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            return dialog;
        }

        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            tvSelectDOB.setText(dayOfMonth+ "/" + (month + 1) + "/" + year);
        }
    }



    //************  Call When we clear Edit Text Field Errors ***************
    private void clearEditTextFieldErrors(){
        binding.tilFirstName.setErrorEnabled(false);
        binding.tilLastName.setErrorEnabled(false);
        binding.tilContactNumber.setErrorEnabled(false);
        binding.tilDob.setErrorEnabled(false);
    }

    //******************   Call Method For Give Permission on Marshmallow and > *************************
    private void marshmallow() {
        if (Build.VERSION.SDK_INT < 23) {
            addPhotoDialog();
        } else {
            int hasPermissionCounter = 0;
            String[] permission = {Manifest.permission.CAMERA, READ_EXTERNAL_STORAGE};
            for (String aPermission : permission) {
                if (context.checkSelfPermission(aPermission) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{aPermission}, REQUEST_CODE_ASK_PERMISSIONS);
                } else {
                    hasPermissionCounter++;
                }
            }
            if (hasPermissionCounter == 2) {
                addPhotoDialog();
            }
        }
    }


    //**********  Open Dialog For Camera Or Gallery Picker ****************
    protected void addPhotoDialog() {
        LayoutInflater inflater = LayoutInflater.from(context);
        final Dialog mDialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        if (mDialog.getWindow() != null)
            mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        mDialog.getWindow().setGravity(Gravity.BOTTOM);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();
        View dialogLayout = inflater.inflate(R.layout.popup_profilepic, null);
        mDialog.setContentView(dialogLayout);
        mDialog.show();
        TextView tvCamera = (TextView) mDialog.findViewById(R.id.tvCamera);
        TextView tvGallery = (TextView) mDialog.findViewById(R.id.tvGallery);
        TextView tvCancel = (TextView) mDialog.findViewById(R.id.tvCancel);

        tvCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                selectPictureUtils.takePictureFromCamera();
            }
        });
        tvGallery.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                selectPictureUtils.openGallery();
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
    }


    //******************  Use Method TO Check Validation At Client Size ********************

    private boolean checkValidation() {
        if (binding.etFirstName.getText().toString().trim().isEmpty()) {
            binding.tilFirstName.setErrorEnabled(true);
            binding.tilFirstName.setError(getString(R.string.err_msg_name_empty));
            binding.etFirstName.requestFocus();
            return false;
        }
        else if (binding.etFirstName.getText().toString().trim().length()<3) {
            binding.tilFirstName.setErrorEnabled(true);
            binding.tilFirstName.setError(getString(R.string.valid_first_mane));
            binding.etFirstName.requestFocus();
            return false;
        }
        else if (binding.etLastName.getText().toString().trim().isEmpty()) {
            binding.tilLastName.setErrorEnabled(true);
            binding.tilLastName.setError(getString(R.string.err_msg_last_empty));
            binding.etLastName.requestFocus();
            return false;
        }
        else if (binding.etFirstName.getText().toString().trim().length()<3) {
            binding.tilLastName.setErrorEnabled(true);
            binding.tilLastName.setError(getString(R.string.err_msg_last_isvalid));
            binding.etLastName.requestFocus();
            return false;
        }
        else if (binding.etContactNumber.getText().toString().trim().isEmpty()) {
            binding.tilContactNumber.setErrorEnabled(true);
            binding.tilContactNumber.setError(getString(R.string.err_phone_empty));
            binding.etContactNumber.requestFocus();
            return false;
        }
        else if (binding.etContactNumber.getText().toString().trim().length()<8) {
            binding.tilContactNumber.setErrorEnabled(true);
            binding.tilContactNumber.setError(getString(R.string.err_phone_lengthnotmatch));
            binding.etContactNumber.requestFocus();
            return false;
        }
        else if(tvSelectDOB.getText().toString().trim().equalsIgnoreCase("DOB*")){
            binding.tilDob.setErrorEnabled(true);
            binding.tilDob.setError("Please select DOB.");
            return false;
        }

        else if(binding.spinner.getSelectedItem().toString().equalsIgnoreCase("Gender*")){
            Toast.makeText(context, "Please select your gender.", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == TakePictureUtils.PICK_GALLERY) {
            if (resultCode == RESULT_OK) {
                try {
                    InputStream inputStream = context.getContentResolver().openInputStream(data.getData());
                    FileOutputStream fileOutputStream = new FileOutputStream(new File(context.getExternalFilesDir(AppConstants.TEMP), selectPictureUtils.galleryImagePath + AppConstants.JPG_EXT));
                    TakePictureUtils.copyStream(inputStream, fileOutputStream);
                    fileOutputStream.close();
                    if (inputStream != null) {
                        inputStream.close();
                    }
                } catch (Exception ee) {
                    ee.printStackTrace();
                }
                if(data.getData()!=null){
                    try {
                        CropImage.activity(data.getData()).setGuidelines(CropImageView.Guidelines.ON).start((Activity) context);
                    } catch (Exception e) {
                    }
                }

            }
        }

        else if (requestCode == TAKE_PICTURE) {
            if(selectPictureUtils.getImagePath()!=null){
                Uri uri = Uri.fromFile(new File(selectPictureUtils.getImagePath()));
                CropImage.activity(uri).setGuidelines(CropImageView.Guidelines.ON).start((Activity) context);
            }
        }

        else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                try {
                    bm = MediaStore.Images.Media.getBitmap(context.getContentResolver(), result.getUri());
                    String path = null;
                    if (data != null) {
                        path = String.valueOf(result.getUri());
                        if(bm!=null)
                            binding.civImageProfile.setImageBitmap(bm);
                    }
                    if (path == null) {
                        return;
                    }
                    docFile1=path;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }

    }



    //*****************  This Is Text Watcher Class Perform Operation At RunTime On EditTextField *********************
    private class MyWatcher implements TextWatcher {
        private View view;
        private MyWatcher(View view) {
            this.view = view;
        }
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }
        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {

                case R.id.etFirstName:
                    clearEditTextFieldErrors();
                    break;
                case R.id.etLastName:
                    clearEditTextFieldErrors();
                    break;
                case R.id.etContactNumber:
                    clearEditTextFieldErrors();
                    break;
            }
        }
    }

}
