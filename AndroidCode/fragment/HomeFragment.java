package com.splooze.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.splooze.R;
import com.splooze.activity.HomeActivity;
import com.splooze.adapter.CustomViewPager;
import com.splooze.adapter.HomeFilterAdapter;
import com.splooze.adapter.ViewPagerAdapter;
import com.splooze.databinding.FragmentHomeBinding;
import com.splooze.interfaces.RecyclerItemListener;
import com.splooze.model.FilterModel;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */

@SuppressLint("ValidFragment")
public class HomeFragment extends Fragment implements RecyclerItemListener , CustomViewPager.OnPageChangeListener {
    private Context context;
    private HomeFilterAdapter adapter;
    private ViewPagerAdapter viewPagerAdapter;
    private FragmentHomeBinding binding;
    private LinearLayoutManager mLayoutManager;
    private String eventFrom="";
    private ArrayList<FilterModel> filterList=new ArrayList<>();
    private String [] filterName={"All","Premium","Near By","Category","City"};

    public HomeFragment(){

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
        mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
    }

    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(binding==null){
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
            addDataToList();
            setViewPager();
            ((HomeActivity)context).manageBottomTab("home");
        }else {
            HomeActivity.binding.bottomTab.llRoot.setVisibility(View.VISIBLE);
        }
        return binding.getRoot();
    }

    private void setViewPager() {
        AllEventFragment allEventFragment=new AllEventFragment();
        PremiumEventFragment premiumEventFragment=new PremiumEventFragment();
        NearByEventFragment nearByEventFragment=new NearByEventFragment();
        CategoryEventFragment categoryEventFragment=new CategoryEventFragment();
        CityEventFragment citySelectionFragment=new CityEventFragment();

        viewPagerAdapter=new ViewPagerAdapter(getChildFragmentManager());
        viewPagerAdapter.addFrag(allEventFragment,"all");
        viewPagerAdapter.addFrag(premiumEventFragment,"premium");
        viewPagerAdapter.addFrag(nearByEventFragment,"near_by");
        viewPagerAdapter.addFrag(categoryEventFragment,"category");
        viewPagerAdapter.addFrag(citySelectionFragment,"city");

        binding.viewPager.setOffscreenPageLimit(1);
        binding.viewPager.setAdapter(viewPagerAdapter);
        binding.viewPager.setOnPageChangeListener(this);

     /*   if(getArguments()!=null && getArguments().get(AppConstants.NAVIGATE_FROM).equals("home")){
            binding.viewPager.setCurrentItem(Commonutils.getPreferencesInt(context,AppConstants.SELECT_POSITION));
        }
        else if(getArguments().get(AppConstants.NAVIGATE_FROM).equals("city")){
            binding.viewPager.setCurrentItem(Commonutils.getPreferencesInt(context,AppConstants.SELECT_POSITION));
        }*/

    }


    //**************  Add Data to Filter List *****************
    private void addDataToList() {
        filterList.clear();
        for (int i = 0; i <filterName.length ; i++) {
            FilterModel filterModel=new FilterModel();
            filterModel.filterName=filterName[i];
            if(i==0){
                filterModel.isSelected=true;
            }else {
                filterModel.isSelected=false;
            }
            filterList.add(filterModel);
        }
        setRecyclerView();
    }

    private void setRecyclerView() {
        binding.rvHomeFilter.setLayoutManager(mLayoutManager);
        adapter=new HomeFilterAdapter(context,filterList,this);
        binding.rvHomeFilter.setAdapter(adapter);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    //************  Call When perform Click Event on Filter Item ************
    @Override
    public void onClickOfOk(int position) {
        eventFrom="recycler";
        manageTab(position);
        binding.rvHomeFilter.scrollToPosition(position);
    }

    private void manageTab(int position) {
        binding.viewPager.setCurrentItem(position,true);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                eventFrom="";
            }
        }, 600);
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if(eventFrom.equalsIgnoreCase("recycler")){

        }else {
            for (int i = 0; i <filterList.size() ; i++) {
                FilterModel model=new FilterModel();
                if(i==position){
                    model.isSelected=true;
                }else {
                    model.isSelected=false;
                }
                model.filterName=filterList.get(i).filterName;
                filterList.set(i,model);
            }
            binding.rvHomeFilter.getAdapter().notifyDataSetChanged();
            binding.rvHomeFilter.scrollToPosition(position);
            eventFrom="";
        }
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

}
