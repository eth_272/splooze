package com.splooze.fragment;


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.splooze.R;
import com.splooze.activity.HomeActivity;
import com.splooze.adapter.FollowingAdapter;
import com.splooze.databinding.FragmentFollowersBinding;
import com.splooze.databinding.FragmentFollowingBinding;
import com.splooze.model.FollowingModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FollowingFragment extends Fragment {
    private FragmentFollowingBinding binding;
    private Context context;
    private ArrayList<FollowingModel> MyList=new ArrayList();
    private Integer[] followingImage=new Integer[]{R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,
            R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3};
    private String[] followingName={"Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy",
            "Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy",};

    public FollowingFragment() {
        // Required empty public constructor
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding= DataBindingUtil.inflate(inflater,R.layout.fragment_following,container,false);
        setOnClickListeners();
        setRecyclerView();
        return binding.getRoot();

    }
    //**************set the recycler view with the orientation of layout grid manager for grid view**************
    private void setRecyclerView() {
        addDataToList();
        GridLayoutManager manager=new GridLayoutManager(getContext(),3, LinearLayoutManager.VERTICAL,false);
        binding.rvFollowing.setLayoutManager(manager);
        //create a custom adapter for adding mutiple items.
        FollowingAdapter adapter=new FollowingAdapter(context,MyList);
        binding.rvFollowing.setAdapter(adapter);
    }
    private void setOnClickListeners() {
        //click on left button of toolbar
        binding.toolbar.ivToolbarLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity)context).onBackPressed();
            }
        });
        binding.toolbar.tvToolbarTitle.setText(getString(R.string.following));

    }
    //Add or set the item from model class
    private void addDataToList() {
        for(int a=0;a<followingImage.length;a++){
            FollowingModel model=new FollowingModel();
            model.setFollowing_image(followingImage[a]);
            model.setFollowing_name(followingName[a]);
            MyList.add(model);
        }
    }
}
