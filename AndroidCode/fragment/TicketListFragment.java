package com.splooze.fragment;


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.splooze.R;
import com.splooze.activity.HomeActivity;
import com.splooze.adapter.TicketListAdapter;
import com.splooze.databinding.FragmentTicketlistBinding;
import com.splooze.model.TicketListModel;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class TicketListFragment extends Fragment {
    private Context context;
    private FragmentTicketlistBinding binding;
    private ArrayList<TicketListModel> myItem=new ArrayList();
    private Integer[] event_ticket_back_view=new Integer[]{R.drawable.bg_rectangle_white_ticketlist,R.drawable.bg_rectangle_white_ticketlist,R.drawable.bg_rectangle_white_ticketlist,R.drawable.bg_rectangle_white_ticketlist,R.drawable.bg_rectangle_white_ticketlist,
            R.drawable.bg_rectangle_white_ticketlist,R.drawable.bg_rectangle_white_ticketlist,R.drawable.bg_rectangle_white_ticketlist,R.drawable.bg_rectangle_white_ticketlist,R.drawable.bg_rectangle_white_ticketlist,R.drawable.bg_rectangle_white_ticketlist,R.drawable.bg_rectangle_white_ticketlist,R.drawable.bg_rectangle_white_ticketlist,R.drawable.bg_rectangle_white_ticketlist,R.drawable.bg_rectangle_white_ticketlist};
    private Integer[] eventImage=new Integer[]{R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img3};
    private String[] eventName= {"Music Club", "Lets's Dance","Music Club", "Lets's Dance","Music Club","Music Club", "Lets's Dance","Music Club", "Lets's Dance","Music Club","Music Club", "Lets's Dance","Music Club", "Lets's Dance","Music Club"};
    private String[] eventAddress={"A 45,Sector 63,Noida,Uttar Pradesh 201307","A 45,Sector 63,Noida,Uttar Pradesh 201307","A 45,Sector 63,Noida,Uttar Pradesh 201307","A 45,Sector 63,Noida,Uttar Pradesh 201307","A 45,Sector 63,Noida,Uttar Pradesh 201307","b -40 ,ghaziabad ","Opulent mall h-240,ghaziabad"
            ,"b -40 ,ghaziabad ","Opulent mall h-240,ghaziabad", "A 45,Sector 63,Noida,Uttar Pradesh 201307","noida city center sector -62","A 55,Sector 6,Noida,Uttar Pradesh 201307","Opulent mall h-240,ghaziabad","b -40 ,ghaziabad ","Opulent mall h-240,ghaziabad"};
    private String[] eventDate={"Monday,22 June 2019,10:30PM","Tuesday,22 April 2019,10:30PM","Wednesday,22 May 1998 12:30PM","Thursday,21 Dec 1996 12:30PM","Wednesday,22 May 1998 12:30PM","Monday,22 June 2019,10:30PM","Tuesday,22 April 2019,10:30PM","Wednesday,22 May 1998 12:30PM","Thursday,21 Dec 1996 12:30PM","Wednesday,22 May 1998 12:30PM","Monday,22 June 2019,10:30PM","Tuesday,22 April 2019,10:30PM"
            ,"Wednesday,22 May 1998 12:30PM","Thursday,21 Dec 1996 12:30PM","Wednesday,22 May 1998 12:30PM"};
    private String[] eventPriceTitle={"Price","Price","Price","Price","Price","","price","","","price","","price","","","price"};
    private String[] eventPrice={"R30","R50","R70","R90","R3","Free","R50","Free","Free","R3","Free","R50","Free","Free","R3"};

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }
    public TicketListFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding= DataBindingUtil.inflate(inflater,R.layout.fragment_ticketlist,container,false);
        setOnClickListeners();
        setRecyclerView();
        return binding.getRoot();
    }
    //**************set the recycler view with the orientation of Liner Layout Manager  for vertical view**************
    private void setRecyclerView() {
        addDatatoList();
        LinearLayoutManager manager=new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        binding.rvTicketList.setLayoutManager(manager);
        TicketListAdapter customAdapter = new TicketListAdapter(context,myItem);
        binding.rvTicketList.setAdapter(customAdapter);

    }
    private void setOnClickListeners() {
        HomeActivity.binding.bottomTab.llRoot.setVisibility(View.INVISIBLE);
        binding.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity)context).onBackPressed();
            }
        });

    }
    //Add or set the item from model class
    private void addDatatoList() {
        for(int i = 0; i<eventName.length; i++){
            TicketListModel model=new TicketListModel();
            model.setEvent_ticket_back_view(event_ticket_back_view[i ]);
            model.setEvent_image(eventImage[i]);
            model.setEvent_name(eventName[i]);
            model.setEvent_date(eventDate[i]);
            model.setEvent_address(eventAddress[i]);
            model.setEvent_price_title(eventPriceTitle[i]);
            model.setEvent_price(eventPrice[i]);
            myItem.add(model);
        }
    }
}
