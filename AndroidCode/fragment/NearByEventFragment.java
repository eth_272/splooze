package com.splooze.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import com.splooze.R;
import com.splooze.adapter.NearByEventFilterAdapter;
import com.splooze.databinding.FragmentNearByEventBinding;
import com.splooze.interfaces.RecyclerItemListener;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;


/**
 * A simple {@link Fragment} subclass.
 */

@SuppressLint("ValidFragment")
public class NearByEventFragment extends Fragment  implements  RecyclerItemListener {
    private Context context;
    private FragmentNearByEventBinding binding;
    public NearByEventFragment(){

    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }
    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_near_by_event, container, false);
        setSliderWithRecyclerView();
        getValueFromSeekBar();
        return binding.getRoot();
    }

    private void getValueFromSeekBar() {
        binding.simpleSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                binding.tvDistance.setText(String.valueOf(progress)+" Km");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void setSliderWithRecyclerView() {
        binding.dsAllEvent.setSlideOnFling(true);
        NearByEventFilterAdapter adapter=new NearByEventFilterAdapter(context,this);
        binding.dsAllEvent.setAdapter(adapter);
        binding.dsAllEvent.scrollToPosition(0);
        binding.dsAllEvent.setItemTransformer(new ScaleTransformer.Builder()
                .setMinScale(0.85f)
                .build());
    }

    @Override
    public void onClickOfOk(int position) {

    }
}
