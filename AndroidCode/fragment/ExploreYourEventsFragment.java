package com.splooze.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar;
import com.splooze.R;
import com.splooze.adapter.ExploreYouEventAdapter;
import com.splooze.databinding.FragmentExploreBinding;
import com.splooze.databinding.FragmentExploreYourEventsBinding;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * A simple {@link Fragment} subclass.
 */

@SuppressLint("ValidFragment")
public class ExploreYourEventsFragment extends Fragment {
    private Context context;
    private FragmentExploreYourEventsBinding binding;
    private LinearLayoutManager mLayoutManager;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }
    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_explore_your_events, container, false);
        setOnClickListeners();
        setRecyclerView();
        return binding.getRoot();
    }

    private void setRecyclerView() {
        mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        binding.rvYourEvents.setLayoutManager(mLayoutManager);
        ExploreYouEventAdapter adapter=new ExploreYouEventAdapter(context);
        binding.rvYourEvents.setAdapter(adapter);
    }

    private void setOnClickListeners() {
        Calendar today=new GregorianCalendar();
        binding.collapsibleCalendarView.addEventTag(today.get(Calendar.YEAR),today.get(Calendar.MONTH),today.get(Calendar.DAY_OF_MONTH));
        today.add(Calendar.DATE,1);
        binding.collapsibleCalendarView.addEventTag(today.get(Calendar.YEAR),today.get(Calendar.MONTH),today.get(Calendar.DAY_OF_MONTH), Color.BLUE);

        binding.collapsibleCalendarView.setCalendarListener(new CollapsibleCalendar.CalendarListener() {
            @Override
            public void onDaySelect() {
            }
            @Override
            public void onItemClick(View v) {
            }
            @Override
            public void onDataUpdate() {
            }
            @Override
            public void onMonthChange() {
            }
            @Override
            public void onWeekChange(int position) {
            }
        });

        binding.collapsibleCalendarView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(binding.collapsibleCalendarView.expanded){
                    binding.collapsibleCalendarView.collapse(400);
                }
                else{
                    binding.collapsibleCalendarView.expand(400);
                }
            }
        });
    }


}
