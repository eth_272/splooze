package com.splooze.fragment;


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.SnapHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.splooze.R;
import com.splooze.activity.HomeActivity;
import com.splooze.adapter.HistoryEventAdapter;
import com.splooze.databinding.FragmentHistoryEventBinding;
import com.splooze.model.HistoryEventModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryEventFragment extends Fragment {
    private FragmentHistoryEventBinding binding;
    private ArrayList<HistoryEventModel> Mylist=new ArrayList();
    private Context context;
    private Integer[] eventImage=new Integer[]{R.mipmap.event1,R.mipmap.event2,R.mipmap.event1,R.mipmap.event2,R.mipmap.event1,R.mipmap.event2,R.mipmap.event1,R.mipmap.event2,R.mipmap.event1,R.mipmap.event2,};
    private String[] eventName={"Celebration Event","Carnival Celebration","Music Club Event","Celebration Event","Carnival Celebration","Music Club Event","Celebration Event","Carnival Celebration","Music Club Event","Carnival Celebration"};
    private String[] eventAddress={"A 45,Sector 63,Noida,Uttar Pradesh 201307","A 45,Sector 63,Noida,Uttar Pradesh 201307","A 45,Sector 63,Noida,Uttar Pradesh 201307","A 45,Sector 63,Noida,Uttar Pradesh 201307","A 45,Sector 63,Noida,Uttar Pradesh 201307","b -40 ,ghaziabad ","Opulent mall h-240,ghaziabad"
            ,"b -40 ,ghaziabad ","Opulent mall h-240,ghaziabad", "A 45,Sector 63,Noida,Uttar Pradesh 201307"};

    public HistoryEventFragment() {
        // Required empty public constructor
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding= DataBindingUtil.inflate(inflater,R.layout.fragment_history_event,container,false);
        setOnClickListeners();
        setRecyclerView();
        return binding.getRoot();
    }

    private void setOnClickListeners() {
        HomeActivity.binding.bottomTab.llRoot.setVisibility(View.VISIBLE);
        binding.toolbar.tvToolbarTitle.setText("MY HANGOUTS");
        binding.toolbar.ivToolbarLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity)context).onBackPressed();
            }
        });

    }
    //**************set the recycler view with the orientation of Liner Layout Manger  for Vertical View**************
    private void setRecyclerView() {
        addDataToList();
        LinearLayoutManager manager=new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        binding.rvHistoryEvent.setLayoutManager(manager);
        //create a custom adapter for adding mutiple items.
        HistoryEventAdapter adapter=new HistoryEventAdapter(context,Mylist);
        binding.rvHistoryEvent.setAdapter(adapter);

    }
    //Add or set the item from model class .
    private void addDataToList() {
        for(int i=0;i<eventImage.length;i++){
            HistoryEventModel model=new HistoryEventModel();
            model.setEvent_image(eventImage[i]);
            model.setEvent_address(eventAddress[i]);
            model.setEvent_name(eventName[i]);
            Mylist.add(model);
        }
    }
}
