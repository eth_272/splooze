package com.splooze.fragment;


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.splooze.R;
import com.splooze.activity.HomeActivity;
import com.splooze.adapter.FollowersAdapter;
import com.splooze.databinding.FragmentExploreSearchBinding;
import com.splooze.model.FollowersModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExploreSearchFragment extends Fragment {
    private FragmentExploreSearchBinding binding;
    private Context context;
    private ArrayList<FollowersModel> mylist=new ArrayList();
    private Integer[] followers_image=new Integer[]{R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,
            R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3};
    private String[] followers_name={"Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy",
            "Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy",};

    public ExploreSearchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding= DataBindingUtil.inflate(inflater,R.layout.fragment_explore_search,container,false);
        setOnClickListeners();
        setRecyclerView();
        return binding.getRoot();
    }

    private void setOnClickListeners() {
        binding.ivToolbarLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity)context).onBackPressed();
            }
        });

        binding.etInputotp.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(binding.etInputotp.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });


    }

    private void setRecyclerView() {
        addDataToList();
        //Set the orientation of layout grid manager for grid view
        GridLayoutManager manager=new GridLayoutManager(getContext(),3,LinearLayoutManager.VERTICAL,false);
        binding.recyclerViewFollowers.setLayoutManager(manager);
        FollowersAdapter adapter=new FollowersAdapter(context,mylist);
        binding.recyclerViewFollowers.setAdapter(adapter);
    }

    private void addDataToList() {
        for(int a=0;a<followers_image.length;a++){
            FollowersModel model=new FollowersModel();
            model.setFollowers_image(followers_image[a]);
            model.setFollowers_name(followers_name[a]);
            mylist.add(model);
        }
    }
}
