package com.splooze.fragment;


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.splooze.R;
import com.splooze.activity.HomeActivity;
import com.splooze.adapter.FollowersAdapter;
import com.splooze.databinding.FragmentFollowersBinding;
import com.splooze.model.FollowersModel;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FollowersFragment extends Fragment {
    private FragmentFollowersBinding binding;
    private Context context;
    private ArrayList<FollowersModel> MyList=new ArrayList();
    private Integer[] followersImage=new Integer[]{R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,
            R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3,R.mipmap.img1,R.mipmap.img2,R.mipmap.img3};
    private String[] followersName={"Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy",
            "Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy","Naddia Osminy",};
    
    public FollowersFragment() {
        // Required empty public constructor
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding= DataBindingUtil.inflate(inflater,R.layout.fragment_followers,container,false);
        setOnClickListeners();
        setRecyclerView();
        return binding.getRoot();
    }
    private void setOnClickListeners() {
        binding.toolbar.ivToolbarLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity)context).onBackPressed();
            }
        });
        binding.toolbar.tvToolbarTitle.setText(getString(R.string.follower));
    }
    //**************set the recycler view with the orientation of layout grid manager for grid view**************
    private void setRecyclerView() {
        addDataToList();
        GridLayoutManager manager=new GridLayoutManager(getContext(),3,LinearLayoutManager.VERTICAL,false);
        binding.rvFollowers.setLayoutManager(manager);
        //create a custom adapter for adding mutiple items.
        FollowersAdapter adapter=new FollowersAdapter(context,MyList);
        binding.rvFollowers.setAdapter(adapter);
    }
    //Add or set the item from model class
    private void addDataToList() {
        for(int a=0;a<followersImage.length;a++){
            FollowersModel model=new FollowersModel();
            model.setFollowers_image(followersImage[a]);
            model.setFollowers_name(followersName[a]);
            MyList.add(model); 
        }
    }
}
