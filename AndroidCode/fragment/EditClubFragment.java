package com.splooze.fragment;


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.splooze.R;
import com.splooze.databinding.FragmentEditClubBinding;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditClubFragment extends Fragment implements View.OnClickListener {
    private FragmentEditClubBinding binding;
    private Context context;


    public EditClubFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_edit_club, container, false);
        setOnClickListeners();
        return binding.getRoot();
    }

    private void  setOnClickListeners() {

        binding.etName.addTextChangedListener(new MyWatcher(binding.etName));
        binding.etCity.addTextChangedListener(new MyWatcher(binding.etCity));
        binding.etStreetAddress.addTextChangedListener(new MyWatcher(binding.etStreetAddress));
        binding.tvChooseLocation.setOnClickListener(this);
        binding.ivBack.setOnClickListener(this);
        binding.tvSubmit.setOnClickListener(this);
    }


    //******************  Use Method TO Check Validation At Client Size ********************
    private boolean checkValidation() {
        if (binding.etName.getText().toString().trim().isEmpty()) {
            binding.tilName.setErrorEnabled(true);
            binding.tilName.setError(getString(R.string.err_name_empty));
            binding.etName.requestFocus();
            return false;
        }
        else if(binding.etName.getText().toString().trim().length()<3){
            binding.tilName.setErrorEnabled(true);
            binding.tilName.setError(getString(R.string.err_name_valid));
            binding.etName.requestFocus();
            return false;
        }
        /*else if (binding.tvChooseLocation.getText().toString().equalsIgnoreCase("Choose Location*")){
            binding.tilChooseLocation.setErrorEnabled(true);
            binding.tilChooseLocation.setError(getString(R.string.errChooseLocation));
            Toast.makeText(context,"*Please choose location." , Toast.LENGTH_SHORT).show();
            binding.tvChooseLocation.requestFocus();
            return false;
        }*/
        else if(binding.etCity.getText().toString().isEmpty()){
            binding.tilCity.setErrorEnabled(true);
            binding.tilCity.setError(getString(R.string.errCity));
            binding.etCity.requestFocus();
            return false;
        }
        else if(binding.etCity.getText().toString().length()<3){
            binding.tilCity.setErrorEnabled(true);
            binding.tilCity.setError(getString(R.string.errCityValid));
            binding.etCity.requestFocus();
            return false;
        }
        else if(binding.etStreetAddress.getText().toString().isEmpty()){
            binding.tilStreetAddress.setErrorEnabled(true);
            binding.tilStreetAddress.setError(getString(R.string.errStreetAdress));
            binding.etStreetAddress.requestFocus();
            return false;
        }
        else if(binding.etStreetAddress.getText().toString().length()<3){
            binding.tilStreetAddress.setErrorEnabled(true);
            binding.tilStreetAddress.setError(getString(R.string.errSreetAddressValid));
            binding.etStreetAddress.requestFocus();
            return false;
        }

        return true;
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ivBack:
                Toast.makeText(context, "Under Working", Toast.LENGTH_SHORT).show();
           
            case R.id.tvChooseLocation:
                if(!binding.tvChooseLocation.getText().toString().isEmpty()){
                    manageValidation();
                }
                break;
            case R.id.tvSubmit:
                if(checkValidation()){
                    Toast.makeText(context, "Thank you", Toast.LENGTH_SHORT).show();
                }
                break;

        }

    }
    //***************  Manage All Error Validation ********************
    private void manageValidation() {
        binding.tilName.setErrorEnabled(false);
        //binding.tilChooseLocation.setErrorEnabled(false);
        binding.tilCity.setErrorEnabled(false);
        binding.tilStreetAddress.setErrorEnabled(false);
    }

    //*****************  This Is Text Watcher Class Perform Operation At RunTime On EditTextField *********************
    private class MyWatcher implements TextWatcher {

        private View view;

        private MyWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            switch (view.getId()) {

                case R.id.etName:
                    if (!binding.etName.getText().toString().isEmpty()) {
                        manageValidation();
                    }
                    break;
                case R.id.etCity:
                    if (!binding.etCity.getText().toString().isEmpty()) {
                        manageValidation();
                    }
                    break;
                case R.id.etStreetAddress:
                    if (!binding.etStreetAddress.getText().toString().isEmpty()) {
                        manageValidation();
                    }
                    break;
            }

        }
    }
}
