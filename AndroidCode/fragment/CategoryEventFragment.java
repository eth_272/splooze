package com.splooze.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.splooze.R;
import com.splooze.adapter.CategoryEventAdapter;
import com.splooze.adapter.CitySelectionAdapter;
import com.splooze.databinding.FragmentAllEventsBinding;
import com.splooze.interfaces.RecyclerItemListener;
import com.splooze.model.CitySelectionModel;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */

@SuppressLint("ValidFragment")
public class CategoryEventFragment extends Fragment  implements  RecyclerItemListener, View.OnClickListener {
    private Context context;
    private FragmentAllEventsBinding binding;
    private GridLayoutManager gridLayoutManager;
    private ArrayList<CitySelectionModel> mylist1 = new ArrayList<>();
    private String[] categoryName = {
            "Category 1 ","Category 2 ","Category 3 ","Category 4 ","Category 5 ","Category 6 ","Category 7 ","Category 8 ","Category 9 "};
    private Integer[] categoryImages = new Integer[]{
            R.drawable.cat_1,R.drawable.cat_2,R.drawable.cat_3,R.drawable.cat_4,R.drawable.cat_1,R.drawable.cat_2,R.drawable.cat_3,R.drawable.cat_4,R.drawable.cat_1};

    public CategoryEventFragment(){

    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }
    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_all_events, container, false);
        setOnClickListeners();
        setRecyclerForCityFilter();
        return binding.getRoot();
    }

    private void setOnClickListeners() {
        binding.llCityEvent.setVisibility(View.GONE);
        binding.rvCategoryFilter.setVisibility(View.VISIBLE);
        binding.tvChangeCategory.setOnClickListener(this);
    }
    private void setRecyclerForCityFilter() {
        addDataToList();
        gridLayoutManager = new GridLayoutManager(getContext(),3, LinearLayoutManager.VERTICAL,false);
        binding.rvCategoryFilter.setLayoutManager(gridLayoutManager);
        CitySelectionAdapter customAdapter = new CitySelectionAdapter(mylist1,context,this);
        binding.rvCategoryFilter.setAdapter(customAdapter);
        binding.llCityEvent.setVisibility(View.GONE);
        binding.rvCategoryFilter.setVisibility(View.VISIBLE);
    }


    private void addDataToList() {
        mylist1.clear();
        for(int i = 0; i<categoryName.length; i++){
            CitySelectionModel citySelectionModel=new CitySelectionModel();
            citySelectionModel.setImage_product(categoryImages[i]);
            citySelectionModel.setName_city(categoryName[i]);
            mylist1.add(citySelectionModel);
        }
    }


    private void setSliderWithRecyclerView() {
        binding.dsAllEvent.setSlideOnFling(true);
        CategoryEventAdapter adapter=new CategoryEventAdapter(context,this);
        binding.dsAllEvent.setAdapter(adapter);
        binding.dsAllEvent.scrollToPosition(0);
        binding.dsAllEvent.setItemTransformer(new ScaleTransformer.Builder()
                .setMinScale(0.85f)
                .build());
    }

    @Override
    public void onClickOfOk(int position) {
        binding.llCityEvent.setVisibility(View.VISIBLE);
        binding.rvCategoryFilter.setVisibility(View.GONE);
        setSliderWithRecyclerView();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tvChangeCategory:
                binding.llCityEvent.setVisibility(View.GONE);
                binding.rvCategoryFilter.setVisibility(View.VISIBLE);
                break;
        }
    }
}
