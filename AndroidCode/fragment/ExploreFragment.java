package com.splooze.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.splooze.R;
import com.splooze.activity.HomeActivity;
import com.splooze.adapter.CustomViewPager;
import com.splooze.adapter.ViewPagerAdapter;
import com.splooze.databinding.FragmentExploreBinding;

/**
 * A simple {@link Fragment} subclass.
 */

@SuppressLint("ValidFragment")
public class ExploreFragment extends Fragment implements  CustomViewPager.OnPageChangeListener, View.OnClickListener {
    private Context context;
    private FragmentExploreBinding binding;
    private ViewPagerAdapter viewPagerAdapter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }
    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_explore, container, false);
        setOnClickListeners();
        setViewPager();
        return binding.getRoot();
    }


    private void setOnClickListeners() {
        binding.toolbarExplore.llYourEvents.setOnClickListener(this);
        binding.toolbarExplore.llAllEvents.setOnClickListener(this);
        HomeActivity.binding.bottomTab.llRoot.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
         switch (v.getId()){
             case R.id.llYourEvents:
                 manageTabBar(0);
                 break;

             case R.id.llAllEvents:
                 manageTabBar(1);
                 break;
         }
    }

    private void manageTabBar(int position) {
        if(position==0){
            binding.toolbarExplore.llYourSelectedEvents.setVisibility(View.VISIBLE);
            binding.toolbarExplore.llSelectedAllEvents.setVisibility(View.INVISIBLE);
        }
        if(position==1){
            binding.toolbarExplore.llYourSelectedEvents.setVisibility(View.INVISIBLE);
            binding.toolbarExplore.llSelectedAllEvents.setVisibility(View.VISIBLE);
        }
    }

    private void setViewPager() {
        ExploreYourEventsFragment yourEventFragment=new ExploreYourEventsFragment();
        ExploreAllEventsFragment allEventFragment =new ExploreAllEventsFragment();

        viewPagerAdapter=new ViewPagerAdapter(getChildFragmentManager());
        viewPagerAdapter.addFrag(yourEventFragment,"your");
        viewPagerAdapter.addFrag(allEventFragment,"all");

        binding.viewPager.setOffscreenPageLimit(1);
        binding.viewPager.setAdapter(viewPagerAdapter);
        binding.viewPager.setOnPageChangeListener(this);
    }


    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        manageTabBar(position);
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


}
