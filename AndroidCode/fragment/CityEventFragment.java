package com.splooze.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.splooze.R;
import com.splooze.adapter.CityEventAdapter;
import com.splooze.adapter.CitySelectionAdapter;
import com.splooze.databinding.FragmentCityEventBinding;
import com.splooze.interfaces.RecyclerItemListener;
import com.splooze.model.CitySelectionModel;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */

@SuppressLint("ValidFragment")
public class CityEventFragment extends Fragment  implements  RecyclerItemListener, View.OnClickListener {
    private Context context;
    private FragmentCityEventBinding binding;
    private GridLayoutManager gridLayoutManager;
    private ArrayList<CitySelectionModel> mylist1 = new ArrayList<>();
    private String[] cityName = {
            "City 1 ","City 2","City 3","City 1 ","City 1 ","City 2","City 3","City 1 ","City 1 ","City 2","City 3","City 1 ",
            "City 1 ","City 2","City 3","City 1 ","City 1 ","City 2","City 3","City 1 ","City 1 ","City 2","City 3","City 1 "};
    private Integer[] cityImages = new Integer[]{
            R.drawable.pic_1,R.drawable.pic_2,R.drawable.pic_3,R.drawable.pic_4,R.drawable.pic_2,R.drawable.pic_4,R.drawable.pic_3,R.drawable.pic_1,R.drawable.pic_2,R.drawable.pic_3,R.drawable.pic_1,R.drawable.pic_3,
            R.drawable.pic_1,R.drawable.pic_2,R.drawable.pic_3,R.drawable.pic_4,R.drawable.pic_2,R.drawable.pic_4,R.drawable.pic_3,R.drawable.pic_1,R.drawable.pic_2,R.drawable.pic_3,R.drawable.pic_1,R.drawable.pic_3};

    public CityEventFragment(){
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }


    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_city_event, container, false);
        setOnClickListeners();
        setRecyclerForCityFilter();
        return binding.getRoot();
    }

    private void setOnClickListeners() {
        binding.llCityEvent.setVisibility(View.GONE);
        binding.rvCityFilter.setVisibility(View.VISIBLE);
        binding.tvChangeCity.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tvChangeCity:
                binding.llCityEvent.setVisibility(View.GONE);
                binding.rvCityFilter.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void setRecyclerForCityFilter() {
        addDataToList();
        gridLayoutManager = new GridLayoutManager(getContext(),3, LinearLayoutManager.VERTICAL,false);
        binding.rvCityFilter.setLayoutManager(gridLayoutManager);
        CitySelectionAdapter customAdapter = new CitySelectionAdapter(mylist1,context,this);
        binding.rvCityFilter.setAdapter(customAdapter);
        binding.llCityEvent.setVisibility(View.GONE);
        binding.rvCityFilter.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClickOfOk(int position) {
        binding.llCityEvent.setVisibility(View.VISIBLE);
        binding.rvCityFilter.setVisibility(View.GONE);
        setSliderWithRecyclerView();
    }


    private void addDataToList() {
        for(int i = 0; i<cityName.length; i++){
            CitySelectionModel citySelectionModel=new CitySelectionModel();
            citySelectionModel.setImage_product(cityImages[i]);
            citySelectionModel.setName_city(cityName[i]);
            mylist1.add(citySelectionModel);
        }
    }


    private void setSliderWithRecyclerView() {
        binding.dsAllEvent.setSlideOnFling(true);
        CityEventAdapter adapter=new CityEventAdapter(context,this);
        binding.dsAllEvent.setAdapter(adapter);
        binding.dsAllEvent.scrollToPosition(0);
        binding.dsAllEvent.setItemTransformer(new ScaleTransformer.Builder().setMinScale(0.85f).build());
    }



}
