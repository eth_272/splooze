package com.splooze.fragment;


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.splooze.R;
import com.splooze.adapter.CitySelectionAdapter;
import com.splooze.databinding.FragmentCitySelectionBinding;
import com.splooze.model.CitySelectionModel;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class CitySelectionFragment extends Fragment {
    View View;
    private Context context;
    private FragmentCitySelectionBinding citySelectionBinding;
    private ArrayList<CitySelectionModel> mylist1 = new ArrayList<>();
    private String[] cityName = {"City 1 ","City 2","City 3","City 1 ","City 1 ","City 2","City 3","City 1 ","City 1 ","City 2","City 3","City 1 ",
            "City 1 ","City 2","City 3","City 1 ","City 1 ","City 2","City 3","City 1 ","City 1 ","City 2","City 3","City 1 "};
    private Integer[] cityImages = new Integer[]{R.drawable.pic_1,R.drawable.pic_2,R.drawable.pic_3,R.drawable.pic_4,R.drawable.pic_2,R.drawable.pic_4,R.drawable.pic_3,R.drawable.pic_1,R.drawable.pic_2,R.drawable.pic_3,R.drawable.pic_1,R.drawable.pic_3,
            R.drawable.pic_1,R.drawable.pic_2,R.drawable.pic_3,R.drawable.pic_4,R.drawable.pic_2,R.drawable.pic_4,R.drawable.pic_3,R.drawable.pic_1,R.drawable.pic_2,R.drawable.pic_3,R.drawable.pic_1,R.drawable.pic_3};
    public CitySelectionFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
      /*  citySelectionBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_city_selection, container, false);
        context=getActivity();
        // set a GridLayoutManager with 3 number of columns ,vertical gravity and false value for reverseLayout to show the items from start to end
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(),3, LinearLayoutManager.VERTICAL,false);
        citySelectionBinding.recyclerView.setLayoutManager(gridLayoutManager); // set LayoutManager to RecyclerView
        //  call the constructor of CustomAdapter to send the reference and data to Adapter
        CitySelectionAdapter customAdapter = new CitySelectionAdapter(mylist1,context);
        citySelectionBinding.recyclerView.setAdapter(customAdapter);// set the Adapter to RecyclerView
        addDatatoList();
        ///Add to not stop im middle the images
        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(citySelectionBinding.recyclerView);
*/
        return citySelectionBinding.getRoot();

    }

    private void addDatatoList() {
        for(int i = 0; i<cityName.length; i++){
            CitySelectionModel citySelectionModel=new CitySelectionModel();
            citySelectionModel.setImage_product(cityImages[i]);
            citySelectionModel.setName_city(cityName[i]);
            mylist1.add(citySelectionModel);
        }
    }

}
