package com.splooze.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.splooze.R;
import com.splooze.adapter.NotificationAdapter;
import com.splooze.adapter.ParticipantsAdapter;
import com.splooze.databinding.FragmentExploreBinding;
import com.splooze.databinding.FragmentNotificationBinding;
import com.splooze.model.NotificationModel;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */

@SuppressLint("ValidFragment")
public class NotificationFragment extends Fragment {
    private Context context;
    private FragmentNotificationBinding binding;
    private ArrayList<NotificationModel> MyList=new ArrayList<>();
    private Integer[] Image={R.mipmap.clubimage,R.mipmap.img1,R.mipmap.clubimage,R.mipmap.profilepic,R.mipmap.clubimage};
    private String[] Name={"Music Club","Dance Club","Roaming Ring","Celebration Event","Roaming Ring"};
    private String[] Address={"A 45,Sector 63,Noida,Uttar Pradesh 201307","A 45,Sector 63,Noida,Uttar Pradesh 201307","A 45,Sector 63,Noida,Uttar Pradesh 201307","A 45,Sector 63,Noida,Uttar Pradesh 201307","A 45,Sector 63,Noida,Uttar Pradesh 201307"};
    private String[] DateTime={"Monday,22 June 2019,10:30PM","Tuesday,22 April 2019,10:30PM","Wednesday,22 May 1998 12:30PM","Thursday,21 Dec 1996 12:30PM","Wednesday,22 May 1998 12:30PM"};
    private String[] Type={"Created Event","Follow","Follower","Attendance","Follower"};
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }
    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_notification, container, false);
        setRecyclerView();
        return binding.getRoot();
    }

    //set the recycler view with vertical orientation and add multiple view item through adapter class
    private void setRecyclerView(){
       addDatatoMyList();
        LinearLayoutManager manager=new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false);
        binding.rvNotification.setLayoutManager(manager);
        
        NotificationAdapter adapter=new NotificationAdapter(context,MyList);
        binding.rvNotification.setAdapter(adapter);
    }
    //add  items in list
    private void addDatatoMyList() {
        for (int a=0;a<Type.length;a++){
            NotificationModel model=new NotificationModel();
            model.setAddress(Address[a]);
            model.setDateTime(DateTime[a]);
            model.setImage(Image[a]);
            model.setName(Name[a]);
            model.setType(Type[a]);
            MyList.add(model);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

}
