package com.splooze.location;

import android.location.Location;

public  abstract class LocationResult {
	public abstract void gotLocation(Location location);
}