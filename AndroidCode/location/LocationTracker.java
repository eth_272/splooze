package com.splooze.location;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.splooze.R;


/**
 * This class is used for Getting location form GPS
 */
public class LocationTracker implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ResultCallback<LocationSettingsResult> {
    private LocationRequest mLocationRequest;
    private GoogleApiClient mLocationClient;
    private Context context;
    private LocationResult locationResult;
    public static int REQUEST_CHECK_SETTINGS = 100;
    public static final int LOCATION_ENABLE = 102;

    public LocationTracker(Context context, LocationResult locationResult) {
        super();
        this.context = context;
        this.locationResult = locationResult;
    }

    public boolean isLocationServiceEnabled() {
        LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return statusOfGPS;
    }


    public void showSettingsAlert(final Activity context) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setMessage(context.getString(R.string.location_service_is_disabled));

        alertDialog.setPositiveButton(context.getString(R.string.settings), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                context.startActivityForResult(intent, LOCATION_ENABLE);
            }
        });

        alertDialog.setNegativeButton(context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @SuppressLint("NewApi")
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                context.finishAffinity();
            }
        });

        alertDialog.show();
    }


    public void onUpdateLocation() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        mLocationClient = new GoogleApiClient.Builder(context, LocationTracker.this, LocationTracker.this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        if (mLocationClient.isConnected()) {
            Log.e("Location Tracker", "Location client is connected...");
            startUpdates();
        } else {
            mLocationClient.connect();
            Log.e("Location Tracker", "Location client is going to connected...");
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.e("Location Tracker", "on connection Failed...");
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Log.e("Location Tracker", "on connected...");
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mLocationClient,
                        builder.build()
                );

        result.setResultCallback(this);
        startUpdates();
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.e("Location Tracker", "on location changed...");
        if (location != null) {

            if (location.getLatitude() != 0 && location.getLongitude() != 0) {

                locationResult.gotLocation(location);

                if (mLocationClient != null && mLocationClient.isConnected()) {
                    stopPeriodicUpdates();

                }

            }
        }

    }

    /**
     * In response to a request to start updates, send a request
     * to Location Services
     */
    @SuppressLint("MissingPermission")
    private void startPeriodicUpdates() {
        Log.e("Location Tracker", "start periodic updates...");
        if (checkPermission())
            LocationServices.FusedLocationApi.requestLocationUpdates(mLocationClient, mLocationRequest, LocationTracker.this);

    }

    /**
     * In response to a request to stop updates, send a request to
     * Location Services
     */
    private void stopPeriodicUpdates() {
        if (checkPermission())
            LocationServices.FusedLocationApi.removeLocationUpdates(mLocationClient, LocationTracker.this);
        Log.e("Location Tracker", "Stop periodic updates...");

    }


    private void startUpdates() {
        Log.e("Location Tracker", "Start Update...");
        startPeriodicUpdates();

    }

    @Override
    public void onConnectionSuspended(int arg0) {
        // TODO Auto-generated method stub
        Log.e("Location Tracker", "Connection Suspended");
    }


    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
        return result == PackageManager.PERMISSION_GRANTED;
    }


    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:

                // NO need to show the dialog;

                break;

            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                //  Location settings are not satisfied. Show the user a dialog

                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().

                    status.startResolutionForResult((Activity) context, REQUEST_CHECK_SETTINGS);

                } catch (IntentSender.SendIntentException e) {

                    //failed to show
                }
                break;

            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                // Location settings are unavailable so not possible to show any dialog now
                break;
        }
    }
}