package com.splooze.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.splooze.R;
import com.splooze.databinding.RowBeverageItemBinding;
import com.splooze.interfaces.BeveragesItemListener;
import com.splooze.model.BeveragesModel;
import java.util.ArrayList;

/**
 * Created by ajay on 4/9/2018.
 */
public class AddBeverageAdapter extends RecyclerView.Adapter<AddBeverageAdapter.MyViewHolder> {
    private Context context;
    private BeveragesItemListener itemListener;
    private ArrayList<BeveragesModel> myList;
    public AddBeverageAdapter(Context context, BeveragesItemListener itemListener, ArrayList<BeveragesModel> myList) {
        this.context = context;
        this.itemListener = itemListener;
        this.myList = myList;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        RowBeverageItemBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.row_beverage_item, parent, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        if(myList.get(position).isSelected){
            holder.binding.etBeverageName.setEnabled(true);
            holder.binding.etBeveragePrice.setEnabled(true);
            holder.binding.ivEditEvent.setImageResource(R.drawable.save_bev);
        }else {
            holder.binding.etBeverageName.setEnabled(false);
            holder.binding.etBeveragePrice.setEnabled(false);
            holder.binding.ivEditEvent.setImageResource(R.mipmap.editevent);
        }

        if(myList.get(position).beverageImage!=null && !myList.get(position).beverageImage.equalsIgnoreCase("")){
            holder.binding.ivBeverages.setImageURI(Uri.parse(myList.get(position).beverageImage));
        }

        holder.binding.etBeverageName.setText(myList.get(position).beverageName);
        holder.binding.etBeveragePrice.setText(myList.get(position).beveragePrice);

        holder.binding.ivEditEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(myList.get(position).isSelected){
                    BeveragesModel model=new BeveragesModel();
                    model.isSelected=false;
                    model.beverageName=holder.binding.etBeverageName.getText().toString().trim();
                    model.beveragePrice=holder.binding.etBeveragePrice.getText().toString().trim();
                    model.beverageImage=myList.get(position).beverageImage;
                    myList.set(position,model);
                    notifyItemChanged(position);
                }
                else {
                    for (int i = 0; i <myList.size() ; i++) {
                        BeveragesModel model=new BeveragesModel();
                        if(i==position){
                            model.isSelected=true;
                        }else {
                            model.isSelected=false;
                        }
                        model.beverageName=myList.get(i).beverageName;
                        model.beveragePrice=myList.get(i).beveragePrice;
                        model.beverageImage=myList.get(i).beverageImage;
                        myList.set(i,model);
                    }
                    notifyDataSetChanged();
                }
            }
        });

        holder.binding.ivBeverages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(myList.get(position).isSelected){
                    itemListener.onClickOfOk(position,"edit");
                }
            }
        });

        holder.binding.ivDeleteEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new android.app.AlertDialog.Builder(context).setTitle("Delete")
                        .setMessage("Do you want to delete  this Item?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                myList.remove(position);
                                notifyDataSetChanged();
                            }
                        }).setNegativeButton("No", null).show();

            }
        });

    }

    @Override
    public int getItemCount() {
        return myList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final RowBeverageItemBinding binding;
        public MyViewHolder(RowBeverageItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
