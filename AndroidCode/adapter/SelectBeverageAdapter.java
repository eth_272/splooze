package com.splooze.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.splooze.R;
import com.splooze.databinding.RowBeverageItem1Binding;
import com.splooze.databinding.RowBeverageItemBinding;
import com.splooze.interfaces.BeveragesItemListener;
import com.splooze.model.BeveragesModel;

import java.util.ArrayList;

/**
 * Created by ajay on 4/9/2018.
 */
public class SelectBeverageAdapter extends RecyclerView.Adapter<SelectBeverageAdapter.MyViewHolder> {
    private Context context;
    private BeveragesItemListener itemListener;
    private ArrayList<BeveragesModel> myList;

    public SelectBeverageAdapter(Context context, BeveragesItemListener itemListener, ArrayList<BeveragesModel> myList) {
        this.context = context;
        this.itemListener = itemListener;
        this.myList = myList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        RowBeverageItem1Binding binding = DataBindingUtil.inflate(layoutInflater, R.layout.row_beverage_item1, parent, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        if(myList.get(position).isSelected){
            holder.binding.ivItemCheck.setImageResource(R.drawable.item_select);
        }else {
            holder.binding.ivItemCheck.setImageResource(R.drawable.item_unselect);
        }

        if(myList.get(position).beverageImage!=null && !myList.get(position).beverageImage.equalsIgnoreCase("")){
            holder.binding.ivBeverages.setImageURI(Uri.parse(myList.get(position).beverageImage));
        }
        holder.binding.etBeverageName.setText(myList.get(position).beverageName);
        holder.binding.etBeveragePrice.setText(myList.get(position).beveragePrice);


        holder.binding.ivItemCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BeveragesModel model=new BeveragesModel();
                if(myList.get(position).isSelected){
                    model.isSelected=false;
                }else {
                    model.isSelected=true;
                }
                model.beverageImage=myList.get(position).beverageImage;
                model.beverageName=myList.get(position).beverageName;
                model.beveragePrice=myList.get(position).beveragePrice;
                myList.set(position,model);
                notifyItemChanged(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return myList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final RowBeverageItem1Binding binding;
        public MyViewHolder(RowBeverageItem1Binding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
