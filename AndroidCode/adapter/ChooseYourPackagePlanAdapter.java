package com.splooze.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.splooze.R;
import com.splooze.databinding.RowChooseYourPackagePlanBinding;
import com.splooze.model.ChooseYourPackagePlanModel;

import java.util.ArrayList;

public class ChooseYourPackagePlanAdapter extends RecyclerView.Adapter<ChooseYourPackagePlanAdapter.MyViewHolder> {
    private ArrayList<ChooseYourPackagePlanModel> MyList;
    private Context context;
    
    public ChooseYourPackagePlanAdapter(Context context, ArrayList<ChooseYourPackagePlanModel> myList) {
        MyList = myList;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        RowChooseYourPackagePlanBinding binding= DataBindingUtil.inflate(inflater, R.layout.row_choose_your_package_plan,parent,false);
        return new ChooseYourPackagePlanAdapter.MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.binding.tvPlanType.setText(MyList.get(position).getPlanType());
        holder.binding.tvPlanDetail.setText(MyList.get(position).getPlanDetail());
        holder.binding.tvPlanPrice.setText(MyList.get(position).getPlanMoney());
        holder.binding.ivPlanDone.setImageResource(MyList.get(position).getPlanDone());
    }

    @Override public int getItemCount() {
        return MyList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private RowChooseYourPackagePlanBinding binding;
        public MyViewHolder(@NonNull RowChooseYourPackagePlanBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
        }
    }
}
