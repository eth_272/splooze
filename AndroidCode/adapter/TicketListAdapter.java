package com.splooze.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.splooze.R;
import com.splooze.activity.TicketDetailActivity;
import com.splooze.databinding.RowTicketListBinding;
import com.splooze.model.TicketListModel;

import java.util.ArrayList;


public class TicketListAdapter extends RecyclerView.Adapter<TicketListAdapter.MyViewHolder> {

    private ArrayList<TicketListModel> MyItem;
    private Context context;
    public TicketListAdapter(Context context,ArrayList <TicketListModel> myitem) {
        this.MyItem=myitem;
        this.context=context;
    }
    public TicketListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        RowTicketListBinding binding=DataBindingUtil.inflate(layoutInflater,R.layout.row_ticket_list,viewGroup,false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull TicketListAdapter.MyViewHolder myViewHolder, int position) {
        myViewHolder.binding.ivEventImge.setImageResource(MyItem.get(position).getEvent_image());
        myViewHolder.binding.tvEventName.setText(MyItem.get(position).getEvent_name());
        myViewHolder.binding.tvEventAddress.setText(MyItem.get(position).getEvent_address());
        myViewHolder.binding.tvEventDate.setText(MyItem.get(position).event_date);
        if(MyItem.get(position).getEvent_price().equalsIgnoreCase("free")){
            myViewHolder.binding.tvEventPriceTitle.setVisibility(View.GONE);
        }else {
            myViewHolder.binding.tvEventPriceTitle.setVisibility(View.VISIBLE);
        }
        myViewHolder.binding.tvEventPriceTitle.setText(MyItem.get(position).getEvent_price_title());
        myViewHolder.binding.tvEventPrice.setText(MyItem.get(position).getEvent_price());

        myViewHolder.binding.llRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, TicketDetailActivity.class);
                context.startActivity(intent);
            }
        });
    }

    @Override public int getItemCount() {
        return MyItem.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private RowTicketListBinding binding;

        public MyViewHolder(RowTicketListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }
    }
}






