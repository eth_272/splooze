package com.splooze.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.splooze.R;
import com.splooze.databinding.RowHistoryEventBinding;
import com.splooze.fragment.EventDetailsFragment;
import com.splooze.model.HistoryEventModel;
import com.splooze.utils.Commonutils;

import java.util.ArrayList;

public class HistoryEventAdapter extends RecyclerView.Adapter<HistoryEventAdapter.MyViewHolder> {
    private  ArrayList<HistoryEventModel> MyList;
    private Context context;



    public HistoryEventAdapter(Context context,ArrayList<HistoryEventModel> MyList) {
        this.MyList=MyList;
        this.context=context;
    }

    @NonNull
    @Override
    public HistoryEventAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int item) {
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        RowHistoryEventBinding binding= DataBindingUtil.inflate(inflater, R.layout.row_history_event,parent,false);
        return new HistoryEventAdapter.MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {
        myViewHolder.binding.ivEventImage.setImageResource(MyList.get(position).getEvent_image());
        myViewHolder.binding.tvHistoryEventLocattion.setText(MyList.get(position).getEvent_address());
        myViewHolder.binding.tvEventHistoryName.setText(MyList.get(position).getEvent_name());

        myViewHolder.binding.ivEventImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventDetailsFragment fragment = new EventDetailsFragment();
                Commonutils.setFragment(fragment, false, (FragmentActivity) context, R.id.flHome);
            }
        });
    }
    @Override
    public int getItemCount() {
        return MyList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private RowHistoryEventBinding binding;
        public MyViewHolder(@NonNull RowHistoryEventBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
        }
    }
}
