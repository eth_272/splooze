package com.splooze.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.splooze.R;
import com.splooze.databinding.RowHomeFilterBinding;
import com.splooze.interfaces.RecyclerItemListener;
import com.splooze.model.FilterModel;

import java.util.ArrayList;

/**
 * Created by ajay on 4/9/2018.
 */
public class HomeFilterAdapter extends RecyclerView.Adapter<HomeFilterAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<FilterModel> filterList;
    private RecyclerItemListener itemListener;
    public HomeFilterAdapter(Context context, ArrayList<FilterModel> filterList, RecyclerItemListener itemListener) {
        this.context = context;
        this.filterList = filterList;
        this.itemListener = itemListener;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        RowHomeFilterBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.row_home_filter, parent, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        if(filterList.get(position).isSelected){
            holder.binding.viewFilter.setVisibility(View.VISIBLE);
        }else {
            holder.binding.viewFilter.setVisibility(View.INVISIBLE);
        }

        holder.binding.tvFilterName.setText(filterList.get(position).filterName);

        holder.binding.llFilterItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0; i <filterList.size() ; i++) {
                    FilterModel model=new FilterModel();
                    if(i==position){
                        model.isSelected=true;
                    }else {
                        model.isSelected=false;
                    }
                    model.filterName=filterList.get(i).filterName;
                    filterList.set(i,model);
                }
                notifyDataSetChanged();
                itemListener.onClickOfOk(position);
            }
        });
    }


    @Override
    public int getItemCount() {
        return filterList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final RowHomeFilterBinding binding;
        public MyViewHolder(RowHomeFilterBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
