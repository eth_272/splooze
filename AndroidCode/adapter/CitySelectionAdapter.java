package com.splooze.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.splooze.R;
import com.splooze.databinding.RowCitySelectionBinding;
import com.splooze.fragment.HomeFragment;
import com.splooze.interfaces.RecyclerItemListener;
import com.splooze.model.CitySelectionModel;
import com.splooze.utils.AppConstants;
import com.splooze.utils.Commonutils;

import java.util.ArrayList;

public class CitySelectionAdapter  extends RecyclerView.Adapter<CitySelectionAdapter.MyViewHolder> {
    private ArrayList<CitySelectionModel> myList1;
    private Context context;
    private RecyclerItemListener listener;
    public CitySelectionAdapter(ArrayList <CitySelectionModel> myList1, Context context, RecyclerItemListener  listener) {
        this.myList1=myList1;
        this.context=context;
        this.listener=listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        RowCitySelectionBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.row_city_selection, viewGroup, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int position) {

        myViewHolder.binding.ivCityImage.setImageResource(myList1.get(position).getImage_product());
        myViewHolder.binding.tvCityName.setText(myList1.get(position).getName_city());

        myViewHolder.binding.ivCityImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClickOfOk(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return myList1.size();
    }

    public  class MyViewHolder extends RecyclerView.ViewHolder{
        private final RowCitySelectionBinding binding;

        public MyViewHolder(RowCitySelectionBinding binding) {
            super(binding.getRoot());
            this.binding = binding;


        }
    }
}


