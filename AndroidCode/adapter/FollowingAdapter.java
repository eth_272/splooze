package com.splooze.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.splooze.R;
import com.splooze.databinding.RowFollowersBinding;
import com.splooze.databinding.RowFollowingBinding;
import com.splooze.fragment.ProfileFragment;
import com.splooze.model.FollowersModel;
import com.splooze.model.FollowingModel;
import com.splooze.utils.AppConstants;
import com.splooze.utils.Commonutils;

import java.util.ArrayList;

public class FollowingAdapter extends RecyclerView.Adapter<FollowingAdapter.MyViewHolder> {
    private ArrayList<FollowingModel> MyList;
    private Context context;

    public FollowingAdapter(Context context,ArrayList<FollowingModel> MyList) {
        this.MyList=MyList;
        this.context=context;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int item) {
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        RowFollowingBinding binding= DataBindingUtil.inflate(inflater, R.layout.row_following,parent,false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {
        myViewHolder.binding.ivFollowingImage.setImageResource(MyList.get(position).getFollowing_image());
        myViewHolder.binding.tvFollowingName.setText(MyList.get(position).getFollowing_name());

        myViewHolder.binding.ivFollowingImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putString(AppConstants.NAVIGATE_FROM,"Following");
                ProfileFragment fragment = new ProfileFragment();
                fragment.setArguments(bundle);
                Commonutils.setFragment(fragment, false, (FragmentActivity) context, R.id.flHome);
            }
        });

    }

    @Override
    public int getItemCount() {
        return MyList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private RowFollowingBinding binding;

        public MyViewHolder(RowFollowingBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }
    }
}
