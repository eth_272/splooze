package com.splooze.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.splooze.R;
import com.splooze.databinding.RowSheetItemBinding;
import com.splooze.interfaces.RecyclerItemListener;
import com.splooze.model.SheetCountModel;

import java.util.ArrayList;

/**
 * Created by ajay on 4/9/2018.
 */
public class SheetCountAdapter extends RecyclerView.Adapter<SheetCountAdapter.MyViewHolder> {
    private Context context;
    private RecyclerItemListener itemListener;
    private ArrayList<SheetCountModel> sheetCountList;
    public SheetCountAdapter(Context context, RecyclerItemListener itemListener, ArrayList<SheetCountModel> sheetCountList) {
        this.context = context;
        this.itemListener = itemListener;
        this.sheetCountList = sheetCountList;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        RowSheetItemBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.row_sheet_item, parent, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        if(sheetCountList.get(position).isSelected){
            holder.binding.llRoot.setBackgroundColor(ContextCompat.getColor(context,R.color.colorRed));
            holder.binding.tvNoCount.setTextColor(ContextCompat.getColor(context,R.color.colorWhite));
        }else {
            holder.binding.llRoot.setBackgroundColor(Color.TRANSPARENT);
            holder.binding.tvNoCount.setTextColor(ContextCompat.getColor(context,R.color.colorBlack));
        }

        holder.binding.tvNoCount.setText(""+sheetCountList.get(position).countNo);

        holder.binding.llRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0; i <sheetCountList.size() ; i++) {
                    SheetCountModel model=new SheetCountModel();
                    if(position==i){
                        model.isSelected=true;
                    }else {
                        model.isSelected=false;
                    }
                    model.countNo=sheetCountList.get(i).countNo;
                    sheetCountList.set(i,model);
                }
                notifyDataSetChanged();
            }
        });

    }


    @Override
    public int getItemCount() {
        return sheetCountList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final RowSheetItemBinding binding;
        public MyViewHolder(RowSheetItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
