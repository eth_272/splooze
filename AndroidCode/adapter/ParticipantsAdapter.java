package com.splooze.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.splooze.R;
import com.splooze.databinding.RowParticipantsBinding;
import com.splooze.fragment.ProfileFragment;
import com.splooze.model.ParticipantsModel;
import com.splooze.utils.AppConstants;
import com.splooze.utils.Commonutils;

import java.util.ArrayList;

public class ParticipantsAdapter extends RecyclerView.Adapter <ParticipantsAdapter.MyViewHolder>{

    private ArrayList<ParticipantsModel> MyList;
    private Context context;
    

    public ParticipantsAdapter(Context context, ArrayList<ParticipantsModel> myList) {
        this.MyList=myList;
        this.context=context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(viewGroup.getContext());
        RowParticipantsBinding binding= DataBindingUtil.inflate(inflater, R.layout.row_participants,viewGroup,false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {
        myViewHolder.binding.ivParticipantsImage.setImageResource(MyList.get(position).getParticipantsImage());
        myViewHolder.binding.tvParticipantsName.setText(MyList.get(position).getParticipantsName());

        myViewHolder.binding.ivParticipantsImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putString(AppConstants.NAVIGATE_FROM,"Participants");
                ProfileFragment fragment = new ProfileFragment();
                fragment.setArguments(bundle);
                Commonutils.setFragment(fragment, false, (FragmentActivity) context, R.id.flHome);
            }
        });


    }
    

    @Override
    public int getItemCount() {
        return MyList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private RowParticipantsBinding binding;
        
        public MyViewHolder(RowParticipantsBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
        }
    }
}
