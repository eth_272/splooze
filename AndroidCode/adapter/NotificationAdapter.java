package com.splooze.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.splooze.R;
import com.splooze.databinding.RowNotificationBinding;
import com.splooze.model.NotificationModel;

import java.util.ArrayList;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {
    private ArrayList<NotificationModel> MyList;
    private Context context;
   
    public NotificationAdapter(Context context,ArrayList<NotificationModel> MyList) {
        this.context=context;
        this.MyList=MyList;
    }
    @NonNull
    @Override 
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int item) {
        LayoutInflater inflater=LayoutInflater.from(viewGroup.getContext());
        RowNotificationBinding binding= DataBindingUtil.inflate(inflater, R.layout.row_notification,viewGroup,false);
        return new NotificationAdapter.MyViewHolder(binding);
    }
    @SuppressLint("ResourceAsColor")
    @Override 
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {
        myViewHolder.binding.civClubImage.setImageResource(MyList.get(position).getImage());
        myViewHolder.binding.tvName.setText(MyList.get(position).getName());
        myViewHolder.binding.tvType.setText(MyList.get(position).getType());
        myViewHolder.binding.tvAddress.setText(MyList.get(position).getAddress());
        myViewHolder.binding.tvDateTime.setText(MyList.get(position).getDateTime());
        if(MyList.get(position).getType().equalsIgnoreCase("Created Event"))
        {
            myViewHolder.binding.tvAction.setVisibility(View.GONE);
        }
        else if(MyList.get(position).getType().equalsIgnoreCase("Attendance"))
        {
            myViewHolder.binding.tvAction.setVisibility(View.GONE);
        }
        
        else if(MyList.get(position).getType().equalsIgnoreCase("Follow"))
        {
            myViewHolder.binding.tvAction.setText("Following");
        }
        
        else if(MyList.get(position).getType().equalsIgnoreCase("Following")) {
            myViewHolder.binding.tvAction.setText("Follow");
        }
        /*else if (MyList.get(position).getType().equalsIgnoreCase("Celebration Event")||MyList.get(position).getType().equalsIgnoreCase("A 45,Sector 63,Noida,Uttar Pradesh 201307")
        ||MyList.get(position).getType().equalsIgnoreCase("Wednesday,22 May 1998 12:30PM")) {
                myViewHolder.binding.tvType.setVisibility(View.GONE);
                myViewHolder.binding.tvType.setText("Ticket Expiry");
                myViewHolder.binding.tvType.setBackgroundColor(R.color.colorRed); 
        }*/
        else {
            myViewHolder.binding.tvAction.setVisibility(View.VISIBLE);
        }
    }

    @Override 
    public int getItemCount() {
        return MyList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private RowNotificationBinding binding;
        public MyViewHolder(@NonNull RowNotificationBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
        }
    }
}
