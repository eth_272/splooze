package com.splooze.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.splooze.R;
import com.splooze.databinding.RowBeveragesBinding;
import com.splooze.interfaces.RecyclerItemListener;

/**
 * Created by ajay on 4/9/2018.
 */
public class BeverageAdapter extends RecyclerView.Adapter<BeverageAdapter.MyViewHolder> {
    private Context context;
    private RecyclerItemListener itemListener;

    public BeverageAdapter(Context context, RecyclerItemListener itemListener) {
        this.context = context;
        this.itemListener = itemListener;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        RowBeveragesBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.row_beverages, parent, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        if(position==0){
            holder.binding.ivBeverage.setImageResource(R.drawable.pic_5);
            holder.binding.tvItemName.setText("5 packs pop-con");
        }

        if(position==1){
            holder.binding.ivBeverage.setImageResource(R.drawable.pic_6);
            holder.binding.tvItemName.setText("2 Soft Drink");
        }

        if(position==2){
            holder.binding.ivBeverage.setImageResource(R.drawable.pic_7);
            holder.binding.tvItemName.setText("1 Pizza ");
        }

        if(position==3){
            holder.binding.ivBeverage.setImageResource(R.drawable.pic_8);
            holder.binding.tvItemName.setText("1 Burger");
        }

        holder.binding.llRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final RowBeveragesBinding binding;
        public MyViewHolder(RowBeveragesBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
