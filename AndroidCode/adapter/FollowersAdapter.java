package com.splooze.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.splooze.R;
import com.splooze.databinding.RowFollowersBinding;
import com.splooze.fragment.ProfileFragment;
import com.splooze.model.FollowersModel;
import com.splooze.utils.AppConstants;
import com.splooze.utils.Commonutils;

import java.util.ArrayList;

public class FollowersAdapter extends RecyclerView.Adapter<FollowersAdapter.MyViewHolder> {
    private ArrayList<FollowersModel> MyList;
    private Context context;
    public FollowersAdapter(Context context,ArrayList <FollowersModel> mylist) {
        this.MyList=mylist;
        this.context=context;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int item) {
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        RowFollowersBinding binding= DataBindingUtil.inflate(inflater, R.layout.row_followers,parent,false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {
        myViewHolder.binding.ivFollowersImage.setImageResource(MyList.get(position).getFollowers_image());
        myViewHolder.binding.tvFollowersName.setText(MyList.get(position).getFollowers_name());

        myViewHolder.binding.ivFollowersImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putString(AppConstants.NAVIGATE_FROM,"Follower");
                ProfileFragment fragment = new ProfileFragment();
                fragment.setArguments(bundle);
                Commonutils.setFragment(fragment, false, (FragmentActivity) context, R.id.flHome);
            }
        });
    }

    @Override
    public int getItemCount() {
        return MyList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private RowFollowersBinding binding;

        public MyViewHolder(RowFollowersBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }
    }
}
