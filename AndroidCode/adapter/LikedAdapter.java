package com.splooze.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.splooze.R;
import com.splooze.databinding.RowLikedBinding;
import com.splooze.fragment.ProfileFragment;
import com.splooze.model.LikedModel;
import com.splooze.utils.AppConstants;
import com.splooze.utils.Commonutils;

import java.util.ArrayList;

public class LikedAdapter extends RecyclerView.Adapter<LikedAdapter.MyViewHolder>{
    private ArrayList<LikedModel> MyList;
    private Context context;

    public LikedAdapter(Context context, ArrayList<LikedModel> myList) {
        this.MyList=myList;
        this.context=context; 
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int item) {
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        RowLikedBinding binding= DataBindingUtil.inflate(inflater, R.layout.row_liked,parent,false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {
        myViewHolder.binding.ivLikedImage.setImageResource(MyList.get(position).getLikedImage());
        myViewHolder.binding.tvLikedName.setText(MyList.get(position).getLikedName());

        myViewHolder.binding.ivLikedImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putString(AppConstants.NAVIGATE_FROM,"Liked");
                ProfileFragment fragment = new ProfileFragment();
                fragment.setArguments(bundle);
                Commonutils.setFragment(fragment, false, (FragmentActivity) context, R.id.flHome);
            }
        });

    }

    @Override
    public int getItemCount() {
        return MyList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private RowLikedBinding binding;
        public MyViewHolder(@NonNull RowLikedBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
        }
    }
}
