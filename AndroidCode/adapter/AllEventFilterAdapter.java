package com.splooze.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.LinearLayout;

import com.splooze.R;
import com.splooze.activity.EventDetailsActivity;
import com.splooze.databinding.RowEventItemBinding;
import com.splooze.fragment.EventDetailsFragment;
import com.splooze.fragment.ParticipantsFragment;
import com.splooze.interfaces.RecyclerItemListener;
import com.splooze.model.FilterModel;
import com.splooze.utils.AnimationItem;
import com.splooze.utils.Commonutils;

import java.util.ArrayList;

import spencerstudios.com.bungeelib.Bungee;

/**
 * Created by ajay on 4/9/2018.
 */
public class AllEventFilterAdapter extends RecyclerView.Adapter<AllEventFilterAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<FilterModel> filterList;
    private RecyclerItemListener itemListener;
    public AllEventFilterAdapter(Context context, RecyclerItemListener itemListener) {
        this.context = context;
        this.filterList = filterList;
        this.itemListener = itemListener;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        RowEventItemBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.row_event_item, parent, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.binding.llRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventDetailsFragment fragment = new EventDetailsFragment();
                Commonutils.setFragment(fragment, false, (FragmentActivity) context, R.id.flHome);
            }
        });

        holder.binding.flParticipant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParticipantsFragment fragment = new ParticipantsFragment();
                Commonutils.setFragment(fragment, false, (FragmentActivity) context, R.id.flHome);
            }
        });

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    private void runLayoutAnimation(String callFrom, final LinearLayout layout, final AnimationItem item) {
        final Context context = layout.getContext();
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, item.getResourceId());
        // layout.setLayoutAnimation(controller);
        //  layout.scheduleLayoutAnimation();

    }

    private void runFadeInAnimation(LinearLayout layout) {
        Animation a = AnimationUtils.loadAnimation(context, R.anim.animate_slide_up_enter);
        a.reset();
        layout.clearAnimation();
        layout.startAnimation(a);

        Handler  handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent=new Intent(context, EventDetailsActivity.class);
                context.startActivity(intent);
                Bungee.shrink(context);
            }
        },400);

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final RowEventItemBinding binding;
        public MyViewHolder(RowEventItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

}
