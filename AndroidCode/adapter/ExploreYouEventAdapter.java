package com.splooze.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.splooze.R;
import com.splooze.databinding.RowExploreEventBinding;
import com.splooze.fragment.EventDetailsFragment;
import com.splooze.model.FilterModel;
import com.splooze.utils.Commonutils;

import java.util.ArrayList;

/**
 * Created by ajay on 4/9/2018.
 */
public class ExploreYouEventAdapter extends RecyclerView.Adapter<ExploreYouEventAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<FilterModel> filterList;

    public  ExploreYouEventAdapter(Context context) {
        this.context = context;
        this.filterList = filterList;

    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        RowExploreEventBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.row_explore_event, parent, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        if (position==0){
            holder.binding.ivEventImage.setImageResource(R.drawable.i_3);
            holder.binding.tvEventName.setText("Music Event");
        }

        if (position==1){
            holder.binding.ivEventImage.setImageResource(R.drawable.i_4);
            holder.binding.tvEventName.setText("Celebration Event");
        }

        if (position==2){
            holder.binding.ivEventImage.setImageResource(R.drawable.i_2);
            holder.binding.tvEventName.setText("Friends Party Event");
        }

        holder.binding.ivEditEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, R.string.work_in_progress, Toast.LENGTH_SHORT).show();
            }
        });

        holder.binding.ivDeleteEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, R.string.work_in_progress, Toast.LENGTH_SHORT).show();
            }
        });
        holder.binding.ivShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, R.string.work_in_progress, Toast.LENGTH_SHORT).show();
            }
        });

        holder.binding.ivEventImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EventDetailsFragment fragment = new EventDetailsFragment();
                Commonutils.setFragment(fragment, false, (FragmentActivity) context, R.id.flHome);

            }
        });

    }


    @Override
    public int getItemCount() {
        return 10;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final RowExploreEventBinding binding;
        public MyViewHolder(RowExploreEventBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
