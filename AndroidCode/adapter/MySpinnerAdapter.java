package com.splooze.adapter;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.splooze.R;

import java.util.List;

public class MySpinnerAdapter extends ArrayAdapter<String> {
    private List<String> items;
    private Context context;

    public MySpinnerAdapter(Context context, int resource, List<String> items) {
        super(context, resource, items);
        this.context = context;
        this.items = items;
    }
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        TextView view = (TextView) super.getView(position, convertView, parent);
        view.setText(items.get(position));
        //view.setBackground(ContextCompat.getDrawable(context, R.drawable.bg_rectangle_white_green));
        if(position == 0){
            view.setTextColor(ContextCompat.getColor(context, R.color.trans));
        }
        else {
            view.setTextColor(ContextCompat.getColor(context,R.color.colorGrey));
        }
        return view;
    }

    // Affects opened state of the spinner
    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        TextView view = (TextView) super.getDropDownView(position, convertView, parent);
        view.setBackgroundColor(ContextCompat.getColor(context,R.color.colorWhite));
        if(position == 0){
            // Set the hint text color gray
            view.setVisibility(View.GONE);
            view.setTextColor(ContextCompat.getColor(context,R.color.colorGrey));
        }
        else {
            view.setVisibility(View.VISIBLE);
            view.setTextColor(ContextCompat.getColor(context,R.color.colorGrey));
        }
       // view.setGravity(Gravity.CENTER);
        return view;
    }
}
