package com.splooze.model;

import java.io.Serializable;

public class FilterModel implements Serializable {
    public String filterName;
    public boolean isSelected;
}
