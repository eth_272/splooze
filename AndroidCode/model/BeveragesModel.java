package com.splooze.model;

import java.io.Serializable;

public class BeveragesModel implements Serializable {
    public String beverageName;
    public boolean isSelected;
    public String beveragePrice;
    public String beverageImage;

}
