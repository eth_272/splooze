package com.splooze.model;

public class ParticipantsModel {
    private Integer participantsImage;
    private String participantsName;

    public Integer getParticipantsImage() {
        return participantsImage;
    }

    public void setParticipantsImage(Integer participantsImage) {
        this.participantsImage = participantsImage;
    }

    public String getParticipantsName() {
        return participantsName;
    }

    public void setParticipantsName(String participantsName) {
        this.participantsName = participantsName;
    }
}
