package com.splooze.model;

public class CitySelectionModel {

    public Integer image_product;
    public  String name_city;

    public String getName_city() {
        return name_city;
    }

    public void setName_city(String name_city) {
        this.name_city = name_city;
    }


    public Integer getImage_product() {
        return image_product;
    }

    public void setImage_product(Integer image_product) {
        this.image_product = image_product;
    }



}
