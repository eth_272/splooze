package com.splooze.model;

public class TicketListModel {
    public Integer event_ticket_back_view;
    public Integer event_image;
    public  String event_name;
    public String event_address;
    public String event_date;
    public String event_price_title;
    public String event_price;


    public Integer getEvent_ticket_back_view() {
        return event_ticket_back_view;
    }

    public void setEvent_ticket_back_view(Integer event_ticket_back_view) {
        this.event_ticket_back_view = event_ticket_back_view;
    }

    public Integer getEvent_image() {
        return event_image;
    }

    public void setEvent_image(Integer event_image) {
        this.event_image = event_image;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getEvent_address() {
        return event_address;
    }

    public void setEvent_address(String event_address) {
        this.event_address = event_address;
    }

    public String getEvent_date() {
        return event_date;
    }

    public void setEvent_date(String event_date) {
        this.event_date = event_date;
    }

    public String getEvent_price_title() {
        return event_price_title;
    }

    public void setEvent_price_title(String event_price_title) {
        this.event_price_title = event_price_title;
    }

    public String getEvent_price() {
        return event_price;
    }

    public void setEvent_price(String event_price) {
        this.event_price = event_price;
    }



}
