package com.splooze.model;

public class ChooseYourPackagePlanModel {
    private  String PlanMoney;
    private String PlanType;
    private String PlanDetail;

    public Integer getPlanDone() {
        return PlanDone;
    }

    public void setPlanDone(Integer planDone) {
        PlanDone = planDone;
    }

    private Integer PlanDone;

    public String getPlanMoney() {
        return PlanMoney;
    }

    public void setPlanMoney(String planMoney) {
        PlanMoney = planMoney;
    }

    public String getPlanType() {
        return PlanType;
    }

    public void setPlanType(String planType) {
        PlanType = planType;
    }

    public String getPlanDetail() {
        return PlanDetail;
    }

    public void setPlanDetail(String planDetail) {
        PlanDetail = planDetail;
    }

    
}
