package com.splooze.model;

public class FollowingModel {
    
    private Integer following_image;
    private  String following_name;
    public Integer getFollowing_image() {
        return following_image;
    }

    public void setFollowing_image(Integer following_image) {
        this.following_image = following_image;
    }

    public String getFollowing_name() {
        return following_name;
    }

    public void setFollowing_name(String following_name) {
        this.following_name = following_name;
    }
}
