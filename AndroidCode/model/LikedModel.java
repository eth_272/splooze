package com.splooze.model;

public class LikedModel {
    private Integer likedImage;
    private String likedName;

    public Integer getLikedImage() {
        return likedImage;
    }

    public void setLikedImage(Integer likedImage) {
        this.likedImage = likedImage;
    }

    public String getLikedName() {
        return likedName;
    }

    public void setLikedName(String likedName) {
        this.likedName = likedName;
    }
}
