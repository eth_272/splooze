package com.splooze.model;

public class FollowersModel {
    private Integer followers_image;
    private  String followers_name;

    public Integer getFollowers_image() {
        return followers_image;
    }

    public void setFollowers_image(Integer followers_image) {
        this.followers_image = followers_image;
    }

    public String getFollowers_name() {
        return followers_name;
    }

    public void setFollowers_name(String followers_name) {
        this.followers_name = followers_name;
    }
}
