package com.splooze.model;

public class HistoryEventModel {
    private  Integer event_image;
    private String event_name;
    private  String event_address;
    public Integer getEvent_image() {
        return event_image;
    }

    public void setEvent_image(Integer event_image) {
        this.event_image = event_image;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }
    public String getEvent_address() {
        return event_address;
    }

    public void setEvent_address(String event_address) {
        this.event_address = event_address;
    }


}
