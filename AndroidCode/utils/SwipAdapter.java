package com.splooze.utils;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

@SuppressWarnings("ALL")
public abstract class SwipAdapter extends BaseAdapter {
	private final Context mContext;

	protected SwipAdapter(Context context) {
		this.mContext = context;
	}

	protected abstract View generateLeftView(final int position,
                                             View convertView);

	protected abstract View generateRightView(final int position,
                                              View convertView);

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LinearLayout layout = new LinearLayout(mContext);
		convertView = layout;

		layout.addView(generateLeftView(position, convertView));
		layout.addView(generateRightView(position, convertView));
		return convertView;
	}

	/*
	 * @Override public View getView(int position, View convertView, ViewGroup
	 * parent) { LinearLayout swipe_layout = new LinearLayout(mContext); convertView =
	 * swipe_layout;
	 * 
	 * swipe_layout.addView(generateLeftView(position, convertView));
	 * swipe_layout.addView(generateRightView(position, convertView)); return
	 * convertView; }
	 */
}
