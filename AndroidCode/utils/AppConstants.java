package com.splooze.utils;

/**
 * Created by Ajay Sharma on 22-11-2016.
 */

@SuppressWarnings("ALL")
public class AppConstants {

    public static final String EMAIL = "email";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME= "last_name";
    public static final String PHONE_NO= "phone_no";
    public static final String COUNTRY_CODE= "country_code";
    public static final String USER_ID= "user_id";
    public static final String OTP= "otp";
    public static final String PASSWORD = "password";
    public static final String YEAR = "yyyy";
    public static final String MONTH = "mm";
    public static final String DATE = "dd";
    public static final String IMAGE_URL = "image_url";
    public static final String SELECT_DATE="select_date";
    public static final String SELECT_BIT_MAP="select_bit_map";
    public static final String SHA = "SHA";
    public static final String TWITTER_KEY = "Z4ebMWwrKQbuz77iOt276Bq03";
    public static final String TWITTER_SECRET = "dpy3EsaAullA3YsbSpzHHOGla1wWc0BlDlG8ijYhn1brY3txwY";
    public static final String WEEK_LIST = "week_list";
    public static final String CURRENT_LAT = "CURRENT_LAT";
    public static final String CURRENT_LONG = "CURRENT_LONG";
    public static final int PERMISSION_REQUEST_CODE = 1;
    public static final String FROM_GALLERY = "fromGallery";
    public static final String FROM_CAMERA = "fromCamera";
    public static final String TEMP = "temp";
    public static final String USER_IMAGE_LOCAL = "imageLocal";
    public static final String JPG_EXT = ".jpg";
    public static final String DEVICE_TOKEN = "device_token";
    public static final String NAVIGATE_FROM = "navigate_from";
    public static final String LAT = "lat";
    public static final String LNG = "lng";
    public static final String INFO = "info";
    public static final String DEVICETOKEN = "DEVICETOKEN";
    public static final String NAVIGATION_FROM = "NAVIGATION_FROM";
    public static final String NAVIGATION_FOR = "NAVIGATION_FOR";
    public static final String NEWS_POST = "NEWS_POST";
    public static final String NEWS_POST_ID = "NEWS_POST_ID";
    public static final String SELECT_POSITION = "select_position";
    public static final String CONTACT = "contact";
    public static final String LOGIN_TYPE= "login_type";
    public static final String LOGIN_BYE= "login_bye";
    public static final String DOB= "DOB";
    public static final String GENDER= "gender";

}
